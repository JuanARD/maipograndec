﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaipoGrandeDatos
{
    public class CD_Pedido
    {
        //Crud aplicado Juan Román y Cristián Esquivel
        private ConexionBD conexion = new ConexionBD();

        OracleDataReader leer;
        DataTable tabla = new DataTable();
        OracleCommand comando = new OracleCommand();

        public DataTable Mostrar()
        {
            tabla = new DataTable();
            comando.Connection = conexion.AbrirConexion();
            comando = new OracleCommand("listarPedidos", conexion.GetConexion());
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.Add("reg", OracleType.Cursor).Direction = ParameterDirection.Output;

            OracleDataAdapter adapter = new OracleDataAdapter();
            adapter.SelectCommand = comando;
            adapter.Fill(tabla);

            conexion.CerrarConexion();

            return tabla;
        }

        public void Insertar(int id_pedido, int id_representante, string ruta_contrato, DateTime fecha_inicio_pedido, DateTime fecha_entrega_estipulada,
            DateTime fecha_entrega_real, string observacion_inicial, string estado_pedido, string calidadEntrega, string observacion_entrega, char devuelto, char vigente, string direccion, string fono)
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "Insert into t_pedido values(:id_pedido,:id_representante,:ruta_contrato,:fecha_inicio_pedido,:fecha_entrega_estipulada," +
                ":fecha_entrega_real,:observacion_inicial,:estado_pedido,:calidadEntrega,:observacion_entrega,:devuelto,:vigente,:direccion,:fono)";
            comando.Parameters.AddWithValue("id_pedido", id_pedido);
            comando.Parameters.AddWithValue("id_representante", id_representante);
            comando.Parameters.AddWithValue("ruta_contrato", ruta_contrato);
            comando.Parameters.AddWithValue("fecha_inicio_pedido", fecha_inicio_pedido);
            comando.Parameters.AddWithValue("fecha_entrega_estipulada", fecha_entrega_estipulada);
            comando.Parameters.AddWithValue("fecha_entrega_real", fecha_entrega_real);
            comando.Parameters.AddWithValue("observacion_inicial", observacion_inicial);
            comando.Parameters.AddWithValue("estado_pedido", estado_pedido);
            comando.Parameters.AddWithValue("calidadEntrega", calidadEntrega);
            comando.Parameters.AddWithValue("observacion_entrega", observacion_entrega);
            comando.Parameters.AddWithValue("devuelto", devuelto);
            comando.Parameters.AddWithValue("vigente", vigente);
            comando.Parameters.AddWithValue("direccion", direccion);
            comando.Parameters.AddWithValue("fono", fono);
            comando.CommandType = CommandType.Text;
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
        }

        public void Modificar(int id_pedido, int id_representante, string ruta_contrato, DateTime fecha_inicio_pedido, DateTime fecha_entrega_estipulada,
            string fecha_entrega_real, string observacion_inicial, string estado_pedido, string calidadEntrega, string observacion_entrega, char devuelto, char vigente, string direccion, string fono)
        {
            comando.Connection = conexion.AbrirConexion();
            comando = new OracleCommand("ACTUALIZAR_PEDIDO", conexion.GetConexion());
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.Add("nuevo_idPedido", OracleType.Number).Value = id_pedido;
            comando.Parameters.Add("nuevo_idRepresentante", OracleType.Number).Value = id_representante;
            comando.Parameters.Add("nuevo_rutaContrato", OracleType.VarChar).Value = ruta_contrato;
            comando.Parameters.Add("nuevo_fechaInicioPedido", OracleType.DateTime).Value = fecha_inicio_pedido;
            comando.Parameters.Add("nuevo_fechaEntregaEstipulada", OracleType.DateTime).Value = fecha_entrega_estipulada;
            if (fecha_entrega_real.Trim()=="")
            {
                comando.Parameters.Add("nuevo_fechaEntregaReal", OracleType.DateTime).Value = DBNull.Value;
            }else
            {
                comando.Parameters.Add("nuevo_fechaEntregaReal", OracleType.DateTime).Value = fecha_entrega_real;
            }
            
            comando.Parameters.Add("nuevo_observacionInicial", OracleType.Clob).Value = observacion_inicial;
            comando.Parameters.Add("nuevo_estadoPedido", OracleType.VarChar).Value = estado_pedido;
            comando.Parameters.Add("nuevo_calidadEntrega", OracleType.VarChar).Value = calidadEntrega;
            comando.Parameters.Add("nuevo_observacionEntrega", OracleType.Clob).Value = observacion_entrega;
            comando.Parameters.Add("nuevo_devuelto", OracleType.Char).Value = devuelto;
            comando.Parameters.Add("nuevo_vigente", OracleType.Char).Value = vigente;
            comando.Parameters.Add("nuevo_direccion", OracleType.VarChar).Value = direccion;
            comando.Parameters.Add("nuevo_fono", OracleType.VarChar).Value = fono;
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
        }

        public void Eliminar(int id_pedido)
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "Delete from t_pedido where id_pedido=:id_pedido";
            comando.Parameters.AddWithValue("id_pedido", id_pedido);
            comando.CommandType = CommandType.Text;
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
        }

        public DataTable ListarIdPedidos()
        {
            tabla = new DataTable();
            comando.Connection = conexion.AbrirConexion();
            comando = new OracleCommand("listarPedidos", conexion.GetConexion());
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.Add("reg", OracleType.Cursor).Direction = ParameterDirection.Output;

            OracleDataAdapter adapter = new OracleDataAdapter();
            adapter.SelectCommand = comando;
            adapter.Fill(tabla);

            conexion.CerrarConexion();

            return tabla;
        }

        public DataTable MostrarPedidoById(int id) // Maximiliano González
        {
            tabla = new DataTable();
            comando = new OracleCommand();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "select * from t_pedido where idpedido=:id_ped";
            comando.Parameters.AddWithValue("id_ped", id);
            comando.CommandType = CommandType.Text;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }

        public DataTable MostrarPedidoReqNegociacion() // Maximiliano González
        {
            tabla = new DataTable();
            comando = new OracleCommand();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "SELECT  a.idpedido, b.nombre, b.appaterno,a.fechainiciopedido, a.fechaentregaestipulada,  a.estadopedido,a.calidadentrega,a.direccion,a.fono FROM  t_pedido a JOIN t_representante b on a.idrepresentante=b.idrepresentante WHERE a.vigente='1' and a.estadopedido='Requiere Negociación' order by a.idpedido asc";
            comando.CommandType = CommandType.Text;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }

    }
}

