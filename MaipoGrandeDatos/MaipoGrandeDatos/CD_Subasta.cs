﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaipoGrandeDatos
{
    public class CD_Subasta
    {
        //Crud aplicado Juan Román
        private ConexionBD conexion = new ConexionBD();

        OracleDataReader leer;
        DataTable tabla = new DataTable();
        OracleCommand comando = new OracleCommand();

        public DataTable Mostrar()
        {
            tabla = new DataTable();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "Select * from t_subasta";
            comando.CommandType = CommandType.Text;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }
        public DataTable MostrarAofertar() //Maximiliano González
        {
            tabla = new DataTable();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "select a.idsubasta, a.nombresubasta, a.idpedido, a.fechapublicacion, a.fechatermino, a.idrepresentante from t_subasta a " +
                "left join t_pedido b on b.idpedido = a.idpedido where b.estadopedido = 'Subastado'";
            comando.CommandType = CommandType.Text;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }

        public void Insertar(int id_subasta, string nombre_subasta, int id_pedido, DateTime fecha_publicacion, DateTime fecha_termino, int id_representante)
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "Insert into t_subasta values(:id_subasta,:nombre_subasta,:id_pedido,:fecha_publicacion,:fecha_termino,:id_representante)";
            comando.Parameters.AddWithValue("id_subasta", id_subasta);
            comando.Parameters.AddWithValue("nombre_subasta", nombre_subasta);
            comando.Parameters.AddWithValue("id_pedido", id_pedido);
            comando.Parameters.AddWithValue("fecha_publicacion", fecha_publicacion);
            comando.Parameters.AddWithValue("fecha_termino", fecha_termino);
            comando.Parameters.AddWithValue("id_representante", id_representante);
            comando.CommandType = CommandType.Text;
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
        }

        public void Modificar(int id_subasta, string nombre_subasta, int id_pedido, DateTime fecha_publicacion, DateTime fecha_termino, int id_representante)
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "Update t_subasta set id_subasta=:id_subasta,nombre_subasta=:nombre_subasta,id_pedido=:id_pedido,fecha_publicacion=:fecha_publicacion,fecha_termino=:fecha_termino,id_representante=:id_representante where id_subasta= :id_subasta";
            comando.Parameters.AddWithValue("id_subasta", id_subasta);
            comando.Parameters.AddWithValue("nombre_subasta", nombre_subasta);
            comando.Parameters.AddWithValue("id_pedido", id_pedido);
            comando.Parameters.AddWithValue("fecha_publicacion", fecha_publicacion);
            comando.Parameters.AddWithValue("fecha_termino", fecha_termino);
            comando.Parameters.AddWithValue("id_representante", id_representante);
            comando.CommandType = CommandType.Text;
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
        }

        public void Eliminar(int id_subasta)
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "Delete from t_subasta where id_subasta=:id_subasta";
            comando.Parameters.AddWithValue("id_subasta", id_subasta);
            comando.CommandType = CommandType.Text;
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
        }
    }
}
