﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaipoGrandeDatos
{
    public class CD_Representante
    {
        //Crud aplicado Juan Román y Cristián Esquivel
        private ConexionBD conexion = new ConexionBD();

        OracleDataReader leer;
        DataTable tabla = new DataTable();
        OracleCommand comando = new OracleCommand();
        OracleCommand comando2 = new OracleCommand();

        public DataTable Mostrar()
        {
            tabla = new DataTable();
            comando.Connection = conexion.AbrirConexion();
            comando = new OracleCommand("ListarRepresentante", conexion.GetConexion());
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.Add("reg", OracleType.Cursor).Direction = ParameterDirection.Output;

            OracleDataAdapter adapter = new OracleDataAdapter();
            adapter.SelectCommand = comando;
            adapter.Fill(tabla);

            conexion.CerrarConexion();
            return tabla;
        }

        public DataTable MostrarPorID(int id)
        {
            tabla = new DataTable();
            comando = new OracleCommand();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "select * from t_representante where idrepresentante=:id_rep";
            comando.Parameters.AddWithValue("id_rep", id);
            comando.CommandType = CommandType.Text;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }

        public DataTable MostrarRepresentanteEmpresaID(int id) // Maximiliano González
        {
            tabla = new DataTable();
            comando = new OracleCommand();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "select a.nombre, a.appaterno, a.apmaterno, a.rutrepresentante, a.fono1, b.nombrecomercial, b.email from t_representante a left join t_empresa b on a.idempresa=b.idempresa where idrepresentante=:id_rep";
            comando.Parameters.AddWithValue("id_rep", id);
            comando.CommandType = CommandType.Text;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }

        public void Insertar(int id_representante, int id_empresa, string rut_representante, string rol_representante, string nombre, string ap_paterno, string ap_materno, char sexo, DateTime fecha_nacimiento, string fono1, string fono2, int rut_empresa, char vigente)
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "Insert into t_representante values(:id_representante,:id_empresa,:rut_representante,:rol_representante,:nombre,:ap_paterno,:ap_materno,:sexo,:fecha_nacimiento,:fono1,:fono2,:vigente)";
            comando.Parameters.AddWithValue("id_representante", id_representante);
            comando.Parameters.AddWithValue("id_empresa", id_empresa);
            comando.Parameters.AddWithValue("rut_representante", rut_representante);
            comando.Parameters.AddWithValue("rol_representante", rol_representante);
            comando.Parameters.AddWithValue("nombre", nombre);
            comando.Parameters.AddWithValue("ap_paterno", ap_paterno);
            comando.Parameters.AddWithValue("ap_materno", ap_materno);
            comando.Parameters.AddWithValue("sexo", sexo);
            comando.Parameters.AddWithValue("fecha_nacimiento", fecha_nacimiento);
            comando.Parameters.AddWithValue("fono1", fono1);
            comando.Parameters.AddWithValue("fono2", fono2);
            comando.Parameters.AddWithValue("vigente", vigente);
            comando.CommandType = CommandType.Text;
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
        }

        public void Modificar(int id_representante, int id_empresa, string rut_representante, string rol_representante, string nombre, string ap_paterno, string ap_materno, char sexo, DateTime fecha_nacimiento, string fono1, string fono2, int rut_empresa, char vigente)
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "Update t_representante set id_representante=:id_representante,id_empresa=:id_empresa,rut_representante=:rut_representante,rol_representante=:rol_representante,nombre=:nombre,ap_paterno=:ap_paterno,ap_materno=:ap_materno,sexo=:sexo,fecha_nacimiento=:fecha_nacimiento,fono1=:fono1,fono2=:fono2,vigente=:vigente where id_representante= :id_representante";
            comando.Parameters.AddWithValue("id_representante", id_representante);
            comando.Parameters.AddWithValue("id_empresa", id_empresa);
            comando.Parameters.AddWithValue("rut_representante", rut_representante);
            comando.Parameters.AddWithValue("rol_representante", rol_representante);
            comando.Parameters.AddWithValue("nombre", nombre);
            comando.Parameters.AddWithValue("ap_paterno", ap_paterno);
            comando.Parameters.AddWithValue("ap_materno", ap_materno);
            comando.Parameters.AddWithValue("sexo", sexo);
            comando.Parameters.AddWithValue("fecha_nacimiento", fecha_nacimiento);
            comando.Parameters.AddWithValue("fono1", fono1);
            comando.Parameters.AddWithValue("fono2", fono2);
            comando.Parameters.AddWithValue("vigente", vigente);
            comando.CommandType = CommandType.Text;
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
        }

        public bool Eliminar(int id_representante)
        {
            comando.Connection = conexion.AbrirConexion();
            comando = new OracleCommand("VERDEPENDENCIASREPRESENTANTE", conexion.GetConexion());
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.Add("ID_Representante", OracleType.Number).Value = id_representante;
            comando.Parameters.Add("reg", OracleType.Cursor).Direction = ParameterDirection.Output;
            OracleDataAdapter adapter = new OracleDataAdapter();
            adapter.SelectCommand = comando;
            DataTable t = new DataTable();
            adapter.Fill(t);
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
            if (int.Parse(t.Rows[0][0].ToString()) > 0)
            {
                return false;
            }



            comando.Connection = conexion.AbrirConexion();
            comando2.Connection = conexion.AbrirConexion();
            comando2 = new OracleCommand("ELIMINAR_USUARIO", conexion.GetConexion());
            comando2.CommandType = CommandType.StoredProcedure;
            comando2.Parameters.Add("ID_REPRESENTANTE", OracleType.Number).Value = id_representante;
            comando = new OracleCommand("ELIMINAR_REPRESENTANTE", conexion.GetConexion());
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.Add("ID_REPRESENTANTE", OracleType.Number).Value = id_representante;
            comando2.ExecuteNonQuery();
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
            return true;
        }

        public DataTable ListarIdRepresentante()
        {
            tabla = new DataTable();
            comando.Connection = conexion.AbrirConexion();
            comando = new OracleCommand("ListarRepresentante", conexion.GetConexion());
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.Add("reg", OracleType.Cursor).Direction = ParameterDirection.Output;

            OracleDataAdapter adapter = new OracleDataAdapter();
            adapter.SelectCommand = comando;
            adapter.Fill(tabla);

            conexion.CerrarConexion();

            return tabla;
        }


    }
}
