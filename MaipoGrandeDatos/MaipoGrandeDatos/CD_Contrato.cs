﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaipoGrandeDatos
{
    public class CD_Contrato
    {
        //Crud aplicado Juan Román
        private ConexionBD conexion = new ConexionBD();

        OracleDataReader leer;
        DataTable tabla = new DataTable();
        OracleCommand comando = new OracleCommand();

        public DataTable Mostrar()
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "Select * from t_contrato";
            comando.CommandType = CommandType.Text;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }

        public void Insertar(int id_contrato, int id_empresa, DateTime fecha_inico, DateTime fecha_termino, string ruta_documento, char vigente)
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "Insert into t_contrato values(:id_contrato,:id_empresa,:fecha_inico,:fecha_termino,:ruta_documento,:vigente)";
            comando.Parameters.AddWithValue("id_contrato", id_contrato);
            comando.Parameters.AddWithValue("id_empresa", id_empresa);
            comando.Parameters.AddWithValue("fecha_inico", fecha_inico);
            comando.Parameters.AddWithValue("fecha_termino", fecha_termino);
            comando.Parameters.AddWithValue("ruta_documento", ruta_documento);
            comando.Parameters.AddWithValue("vigente", vigente);
            comando.CommandType = CommandType.Text;
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
        }

        public void Modificar(int id_contrato, int id_empresa, DateTime fecha_inico, DateTime fecha_termino, string ruta_documento, char vigente)
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "Update t_contrato set id_contrato=:id_contrato,id_empresa=:id_empresa,fecha_inico=:fecha_inico,fecha_termino=:fecha_termino,ruta_documento=:ruta_documento,vigente=:vigente where id_contrato= :id_contrato";
            comando.Parameters.AddWithValue("id_contrato", id_contrato);
            comando.Parameters.AddWithValue("id_empresa", id_empresa);
            comando.Parameters.AddWithValue("fecha_inico", fecha_inico);
            comando.Parameters.AddWithValue("fecha_termino", fecha_termino);
            comando.Parameters.AddWithValue("ruta_documento", ruta_documento);
            comando.Parameters.AddWithValue("vigente", vigente);
            comando.CommandType = CommandType.Text;
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
        }

        public void Eliminar(int id_contrato)
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "Delete from t_contrato where id_contrato=:id_contrato";
            comando.Parameters.AddWithValue("id_contrato", id_contrato);
            comando.CommandType = CommandType.Text;
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
        }


    }
}


