﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaipoGrandeDatos
{
    public class CD_Producto_Pedido
    {
        //Crud aplicado Juan Román y Cristián Esquivel
        private ConexionBD conexion = new ConexionBD();

        OracleDataReader leer;
        DataTable tabla = new DataTable();
        OracleCommand comando = new OracleCommand();

        public DataTable Mostrar()
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "Select * from t_producto_pedido";
            comando.CommandType = CommandType.Text;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }

        public DataTable MostrarProductosEnPedido(int id)
        {
            tabla = new DataTable();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "select t_producto.nombre,t_producto.tipo,t_unidad_medicion.descripcion,t_producto_pedido.cantidad from t_producto_pedido " +
                "inner join t_producto on t_producto.idproducto = t_producto_pedido.idproducto " +
                "inner join t_unidad_medicion on t_unidad_medicion.idunidad = t_producto_pedido.idunidad " +
                "where t_producto_pedido.idpedido = :id_ped";
            comando.Parameters.AddWithValue("id_ped", id);
            comando.CommandType = CommandType.Text;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }

        public void Insertar(int id_producto_pedido, int id_pedido, int cantidad, int id_unidad, int id_producto)
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "Insert into t_producto_pedido values(:id_producto_pedido,:id_pedido,:cantidad,:id_unidad,:id_producto)";
            comando.Parameters.AddWithValue("id_producto_pedido", id_producto_pedido);
            comando.Parameters.AddWithValue("id_pedido", id_pedido);
            comando.Parameters.AddWithValue("cantidad", cantidad);
            comando.Parameters.AddWithValue("id_unidad", id_unidad);
            comando.Parameters.AddWithValue("id_producto", id_producto);
            comando.CommandType = CommandType.Text;
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
        }

        public void Modificar(int id_producto_pedido, int id_pedido, int cantidad, int id_unidad, int id_producto)
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "Update t_producto_pedido set id_producto_pedido=:id_producto_pedido,id_pedido=:id_pedido,cantidad=:cantidad,id_unidad=:id_unidad,id_producto=:id_producto where id_producto_pedido= :id_producto_pedido";
            comando.Parameters.AddWithValue("id_producto_pedido", id_producto_pedido);
            comando.Parameters.AddWithValue("id_pedido", id_pedido);
            comando.Parameters.AddWithValue("cantidad", cantidad);
            comando.Parameters.AddWithValue("id_unidad", id_unidad);
            comando.Parameters.AddWithValue("id_producto", id_producto);
            comando.CommandType = CommandType.Text;
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
        }

        public void Eliminar(int id_producto_pedido)
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "Delete from t_producto_pedido where id_producto_pedido=:id_producto_pedido";
            comando.Parameters.AddWithValue("id_producto_pedido", id_producto_pedido);
            comando.CommandType = CommandType.Text;
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
        }
    }
}
