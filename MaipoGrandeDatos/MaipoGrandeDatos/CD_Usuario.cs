﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaipoGrandeDatos
{
    public class CD_Usuario
    {
        //Crud aplicado Juan Román
        private ConexionBD conexion = new ConexionBD();

        OracleDataReader leer;
        DataTable tabla = new DataTable();
        OracleCommand comando = new OracleCommand();

        public DataTable Mostrar()
        {
            tabla = new DataTable();
            comando.Connection = conexion.AbrirConexion();
            comando = new OracleCommand("ListarUsuRep", conexion.GetConexion());
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.Add("reg", OracleType.Cursor).Direction = ParameterDirection.Output;

            OracleDataAdapter adapter = new OracleDataAdapter();
            adapter.SelectCommand = comando;
            adapter.Fill(tabla);

            conexion.CerrarConexion();

            return tabla;
        }

        public void Insertar(int id_usuario, string email, string contrasena, int id_representante, char vigente)
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "Insert into t_unidad_medicion values(:id_usuario,:email,:contrasena,:id_representante,:vigente)";
            comando.Parameters.AddWithValue("id_usuario", id_usuario);
            comando.Parameters.AddWithValue("email", email);
            comando.Parameters.AddWithValue("contrasena", contrasena);
            comando.Parameters.AddWithValue("id_representante", id_representante);
            comando.Parameters.AddWithValue("vigente", vigente);
            comando.CommandType = CommandType.Text;
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
        }

        public void Modificar(int id_usuario, string email, string contrasena, int id_representante, char vigente)
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "Update t_unidad_medicion set id_usuario=:id_usuario,email=:email,contrasena=:contrasena,id_representante=:id_representante,vigente=:vigente where id_usuario= :id_usuario";
            comando.Parameters.AddWithValue("id_usuario", id_usuario);
            comando.Parameters.AddWithValue("email", email);
            comando.Parameters.AddWithValue("contrasena", contrasena);
            comando.Parameters.AddWithValue("id_representante", id_representante);
            comando.Parameters.AddWithValue("vigente", vigente);
            comando.CommandType = CommandType.Text;
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
        }

        public void Eliminar(int id_usuario)
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "Delete from t_unidad_medicion where id_usuario=:id_usuario";
            comando.Parameters.AddWithValue("id_usuario", id_usuario);
            comando.CommandType = CommandType.Text;
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
        }

        /* LOGIN REVISAR
        public DataTable IniciarSesion(string rut_usuario, string clave)
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "select u.rut_usuario,u.clave,t.descripcion from usuario u right outer join tipousuario t on t.id_tipousuario = u.id_tipousuario where u.rut_usuario = :rut_usuario and u.clave = :clave";
            comando.Parameters.AddWithValue("rut_usuario", rut_usuario); // Parametros, importante para que no te tire error el comando
            comando.Parameters.AddWithValue("clave", clave);
            comando.CommandType = CommandType.Text;
            tabla.Load(comando.ExecuteReader());
            bool hasRows = comando.ExecuteReader().Read(); //Si se devolvieron valores
            conexion.CerrarConexion();
            if (hasRows)
            {
                return tabla;
            }
            else
            {
                return null;
            }

        }
        */
    }
}
