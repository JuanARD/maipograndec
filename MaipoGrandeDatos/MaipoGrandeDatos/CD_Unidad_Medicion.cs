﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaipoGrandeDatos
{
    public class CD_Unidad_Medicion
    {
        //Crud aplicado Juan Román y Cristián Esquivel
        private ConexionBD conexion = new ConexionBD();

        OracleDataReader leer;
        DataTable tabla = new DataTable();
        OracleCommand comando = new OracleCommand();

        public DataTable Mostrar()
        {
            tabla = new DataTable();
            comando.Connection = conexion.AbrirConexion();
            comando = new OracleCommand("listarUnidadMedicion", conexion.GetConexion());
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.Add("reg", OracleType.Cursor).Direction = ParameterDirection.Output;

            OracleDataAdapter adapter = new OracleDataAdapter();
            adapter.SelectCommand = comando;
            adapter.Fill(tabla);
            return tabla;
        }

        public void Insertar(int id_unidad, string descripcion)
        {
            comando.Connection = conexion.AbrirConexion();
            comando = new OracleCommand("CREAR_UNIDAD_MEDICION", conexion.GetConexion());
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.Add("nuevo_idUnidad", OracleType.Number).Value = 0;
            comando.Parameters.Add("nuevo_descripcion", OracleType.VarChar).Value = descripcion;
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
        }

        public void Modificar(int id_unidad, string descripcion)
        {
            comando.Connection = conexion.AbrirConexion();
            comando = new OracleCommand("ACTUALIZAR_UNIDAD_MEDICION", conexion.GetConexion());
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.Add("nuevo_idUnidad", OracleType.Number).Value = id_unidad;
            comando.Parameters.Add("nuevo_descripcion", OracleType.VarChar).Value = descripcion;
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
        }

        public bool Eliminar(int id_unidad)
        {
            comando.Connection = conexion.AbrirConexion();
            comando = new OracleCommand("VerificarDependenciasUnidad", conexion.GetConexion());
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.Add("ID_UNIDAD", OracleType.Number).Value = id_unidad;
            comando.Parameters.Add("reg", OracleType.Cursor).Direction = ParameterDirection.Output;
            OracleDataAdapter adapter = new OracleDataAdapter();
            adapter.SelectCommand = comando;
            DataTable t = new DataTable();
            adapter.Fill(t);
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
            if (int.Parse(t.Rows[0][0].ToString()) > 0)
            {
                return false;
            }

            comando.Connection = conexion.AbrirConexion();
            comando = new OracleCommand("ELIMINAR_UNIDAD_MEDICION", conexion.GetConexion());
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.Add("ID_UNIDAD", OracleType.Number).Value = id_unidad;
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
            return true;
        }
    }
}
