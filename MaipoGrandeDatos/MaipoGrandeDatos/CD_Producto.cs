﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaipoGrandeDatos
{
    public class CD_Producto
    {
        //Crud aplicado Juan Román
        private ConexionBD conexion = new ConexionBD();

        OracleDataReader leer;
        DataTable tabla = new DataTable();
        OracleCommand comando = new OracleCommand();

        public DataTable Mostrar()
        {
            tabla = new DataTable();
            comando.Connection = conexion.AbrirConexion();
            comando = new OracleCommand("LISTARPRODUCTO", conexion.GetConexion());
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.Add("reg", OracleType.Cursor).Direction = ParameterDirection.Output;

            OracleDataAdapter adapter = new OracleDataAdapter();
            adapter.SelectCommand = comando;
            adapter.Fill(tabla);

            conexion.CerrarConexion();

            return tabla;
        }

        public DataTable MostrarProductoByID(int id) // Maximiliano González
        {
            tabla = new DataTable();
            comando = new OracleCommand();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "select * from t_producto where idproducto=:id_prod";
            comando.Parameters.AddWithValue("id_prod", id);
            comando.CommandType = CommandType.Text;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }

        public void Insertar(int id_producto, string nombre, string tipo, string foto)
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "Insert into t_producto values(:id_producto,:nombre,:tipo,:foto)";
            comando.Parameters.AddWithValue("id_producto", id_producto);
            comando.Parameters.AddWithValue("nombre", nombre);
            comando.Parameters.AddWithValue("tipo", tipo);
            comando.Parameters.AddWithValue("foto", foto);
            comando.CommandType = CommandType.Text;
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
        }

        public void Modificar(int id_producto, string nombre, string tipo, string foto)
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "Update t_producto set id_producto=:id_producto,nombre=:nombre,tipo=:tipo,foto=:foto where id_producto= :id_producto";
            comando.Parameters.AddWithValue("id_producto", id_producto);
            comando.Parameters.AddWithValue("nombre", nombre);
            comando.Parameters.AddWithValue("tipo", tipo);
            comando.Parameters.AddWithValue("foto", foto);
            comando.CommandType = CommandType.Text;
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
        }

        public bool Eliminar(int id_producto)
        {
            comando.Connection = conexion.AbrirConexion();
            comando = new OracleCommand("VerificarDependenciasProducto", conexion.GetConexion());
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.Add("ID_Producto", OracleType.Number).Value = id_producto;
            comando.Parameters.Add("reg", OracleType.Cursor).Direction = ParameterDirection.Output;
            OracleDataAdapter adapter = new OracleDataAdapter();
            adapter.SelectCommand = comando;
            DataTable t = new DataTable();
            adapter.Fill(t);
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
            if (int.Parse(t.Rows[0][0].ToString()) > 0)
            {
                return false;
            }

            comando.Connection = conexion.AbrirConexion();
            comando = new OracleCommand("ELIMINAR_PRODUCTO", conexion.GetConexion());
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.Add("id_Producto", OracleType.Number).Value = id_producto;
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
            return true;
        }
    }
}
