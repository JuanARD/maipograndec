﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaipoGrandeDatos
{
    public class CD_Reportes
    {   //Consultas de Reportes por parámetros por Maximiliano González
        private ConexionBD conexion = new ConexionBD();


        //   OracleCommand comando = new OracleCommand();
        OracleDataReader leer;

        public DataTable MostrarLotesByCalidad(String cal)
        {
            OracleCommand comando = new OracleCommand();
            DataTable tabla = new DataTable();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "SELECT a.idloteproducto, b.nombre, b.tipo, a.cantidadproductos, concat( '$', a.preciounidad) as preciounidad, to_date(a.fechavencimiento,'DD-MM-YYYY') as fechavencimiento, c.nombrecomercial, c.fono1 FROM t_lote_producto a join t_producto b on a.idproducto = b.idproducto  join t_empresa c on a.idempresa=c.idempresa where a.calidad= :l_calidad order by a.idloteproducto desc";
            comando.Parameters.AddWithValue("l_calidad", cal);
            comando.CommandType = CommandType.Text;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }


        public DataTable MostrarPedidoByEstado(String estado)
        {
            
            OracleCommand comando = new OracleCommand();
            
            DataTable tabla = new DataTable();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "SELECT a.idpedido, to_date(a.fechainiciopedido,'DD-MM-YYYY') as fechainiciopedido, to_date(a.fechaentregaestipulada,'DD-MM-YYYY') as fechaentregaestipulada,a.estadopedido,a.calidadentrega,a.direccion,a.fono,b.nombre,b.appaterno,b.rutrepresentante,b.fono1 FROM t_pedido a join t_representante b on a.idrepresentante=b.idrepresentante where a.estadopedido= :e_estado order by a.idpedido desc";
            comando.Parameters.AddWithValue("e_estado", estado);
            comando.CommandType = CommandType.Text;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }

        public DataTable MostrarOfertaTransporte()
        {
            OracleCommand comando = new OracleCommand();
            
            DataTable tabla = new DataTable();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "SELECT aa.idofertatransporte, concat('$', aa.costotransporte) as costotransporte, bb.rutrepresentante, bb.nombre, bb.appaterno, bb.fono1, cc.patente, cc.clase, concat(cc.capacidadkg,' Kg '), cc.cmancho, cc.cmalto, cc.cmlargo,cc.frigorifico FROM t_oferta_transporte aa join t_subasta x on aa.idofertatransporte=x.idsubasta join t_vehiculo cc on aa.idvehiculo=cc.idvehiculo join t_representante bb on x.idrepresentante= bb.idrepresentante order by aa.idofertatransporte desc";
            comando.CommandType = CommandType.Text;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }


        public DataTable MostrarSubastaByEstado(String est)
        {
            OracleCommand comando = new OracleCommand();

            DataTable tabla = new DataTable();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "SELECT g.idsubasta, g.nombresubasta, to_date(g.fechapublicacion) as fechapublicacion, p.estadopedido,f.nombre,f.rutrepresentante,f.appaterno, f.apmaterno, h.email,f.fono1 FROM t_subasta g join t_pedido p on g.idpedido=p.idpedido join t_representante f on g.idrepresentante=f.idrepresentante join t_usuario h on h.idrepresentante=f.idrepresentante where p.estadopedido= :s_estado order by g.idsubasta desc";
            comando.Parameters.AddWithValue("s_estado", est);
            comando.CommandType = CommandType.Text;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }
    }
}
