﻿using System;
using System.Collections.Generic;
using System.Data.OracleClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaipoGrandeDatos
{
    public class ConexionBD
    {
        public static string conex = "DATA SOURCE = XE; PASSWORD=1234;USER ID = BDMAIPOGRANDE;";

        private OracleConnection Conexion = new OracleConnection(conex); //El data source por defecto en oracle es XE

        public OracleConnection AbrirConexion()
        {
            if (Conexion.State == System.Data.ConnectionState.Closed)
            {
                Conexion.Open();
            }
            return Conexion;
        }

        public OracleConnection CerrarConexion()
        {
            if (Conexion.State == System.Data.ConnectionState.Open)
            {
                Conexion.Close();
            }
            return Conexion;
        }

        public OracleConnection GetConexion()
        {
            return Conexion;
        }

    }
}
