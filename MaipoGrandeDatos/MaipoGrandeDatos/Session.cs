﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaipoGrandeDatos
{
    public static class Session
    {
        public static string TipoUsuario { get; set; }
        public static string NombreUsuario { get; set; }
    }
}
