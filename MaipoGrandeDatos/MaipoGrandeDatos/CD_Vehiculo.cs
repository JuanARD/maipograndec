﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaipoGrandeDatos
{
    public class CD_Vehiculo
    {
        //Crud aplicado Juan Román
        private ConexionBD conexion = new ConexionBD();

        OracleDataReader leer;
        DataTable tabla = new DataTable();
        OracleCommand comando = new OracleCommand();

        public DataTable Mostrar()
        {
            tabla = new DataTable();
            comando.Connection = conexion.AbrirConexion();
            comando = new OracleCommand("LISTARVEHI", conexion.GetConexion());
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.Add("reg", OracleType.Cursor).Direction = ParameterDirection.Output;

            OracleDataAdapter adapter = new OracleDataAdapter();
            adapter.SelectCommand = comando;
            adapter.Fill(tabla);

            conexion.CerrarConexion();

            return tabla;
        }

        public DataTable MostrarVehiculoById(int id) // Maximiliano González
        {
            tabla = new DataTable();
            comando = new OracleCommand();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "select * from t_vehiculo where idvehiculo=:id_veh";
            comando.Parameters.AddWithValue("id_veh", id);
            comando.CommandType = CommandType.Text;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }

        public void Insertar(int id_vehiculo, int id_empresa, string patente, string clase, int capacidad_KG, int cm_ancho, int cm_largo, char frigorifico)
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "Insert into t_vehiculo values(:id_vehiculo,:id_empresa,:patente,:clase,:capacidad_KG,:cm_ancho,:cm_largo,:frigorifico)";
            comando.Parameters.AddWithValue("id_vehiculo", id_vehiculo);
            comando.Parameters.AddWithValue("id_empresa", id_empresa);
            comando.Parameters.AddWithValue("patente", patente);
            comando.Parameters.AddWithValue("clase", clase);
            comando.Parameters.AddWithValue("capacidad_KG", capacidad_KG);
            comando.Parameters.AddWithValue("cm_ancho", cm_ancho);
            comando.Parameters.AddWithValue("cm_largo", cm_largo);
            comando.Parameters.AddWithValue("frigorifico", frigorifico);
            comando.CommandType = CommandType.Text;
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
        }

        public void Modificar(int id_vehiculo, int id_empresa, string patente, string clase, int capacidad_KG, int cm_ancho, int cm_largo, char frigorifico)
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "Update t_vehiculo set id_vehiculo=:id_vehiculo, id_empresa=:id_empresa, patente=:patente, clase=:clase, capacidad_KG=:capacidad_KG, cm_ancho=:cm_ancho, cm_largo=:cm_largo, frigorifico=:frigorifico where id_vehiculo= :id_vehiculo";
            comando.Parameters.AddWithValue("id_vehiculo", id_vehiculo);
            comando.Parameters.AddWithValue("id_empresa", id_empresa);
            comando.Parameters.AddWithValue("patente", patente);
            comando.Parameters.AddWithValue("clase", clase);
            comando.Parameters.AddWithValue("capacidad_KG", capacidad_KG);
            comando.Parameters.AddWithValue("cm_ancho", cm_ancho);
            comando.Parameters.AddWithValue("cm_largo", cm_largo);
            comando.Parameters.AddWithValue("frigorifico", frigorifico);
            comando.CommandType = CommandType.Text;
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
        }

        public bool Eliminar(int id_vehiculo)
        {
            comando.Connection = conexion.AbrirConexion();
            comando = new OracleCommand("VerificarDependenciasVehiculo", conexion.GetConexion());
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.Add("id_vehiculo", OracleType.Number).Value = id_vehiculo;
            comando.Parameters.Add("reg", OracleType.Cursor).Direction = ParameterDirection.Output;
            OracleDataAdapter adapter = new OracleDataAdapter();
            adapter.SelectCommand = comando;
            DataTable t = new DataTable();
            adapter.Fill(t);
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
            if (int.Parse(t.Rows[0][0].ToString()) > 0)
            {
                return false;
            }

            comando.Connection = conexion.AbrirConexion();
            comando = new OracleCommand("ELIMINAR_VEHICULO", conexion.GetConexion());
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.Add("ID_VEHICULO", OracleType.Number).Value = id_vehiculo;
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
            return true;
        }
    }
}
