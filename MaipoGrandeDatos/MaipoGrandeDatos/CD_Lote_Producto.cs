﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaipoGrandeDatos
{
    public class CD_Lote_Producto
    {
        //Crud aplicado Juan Román
        private ConexionBD conexion = new ConexionBD();

        OracleDataReader leer;
        DataTable tabla = new DataTable();
        OracleCommand comando = new OracleCommand();

        public DataTable Mostrar()
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "Select * from t_lote_producto";
            comando.CommandType = CommandType.Text;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }

        public DataTable MostrarLoteProductoById(int id) // Maximiliano González
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "SELECT idloteproducto,  idproducto, idempresa, idunidad, cantidadproductos, preciounidad, calidad, to_char (fechavencimiento,'DD-MM-YYYY') as fechav,  condicionesalmacenaje,  descripcion, vigente, ventaalextranjero FROM t_lote_producto where idproducto=:id_prod";
            comando.Parameters.AddWithValue("id_prod", id);
            comando.CommandType = CommandType.Text;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }

        public void Insertar(int id_lote_producto, int id_producto, int id_empresa, int id_unidad, int cantidad_productos, int precioUnidad, int calidad, DateTime fecha_vencimiento, string condiciones_almacenaje, string descripcion, char vigente, char ventaAlExtranjero)
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "Insert into t_lote_producto values(:id_lote_producto,:id_producto,:id_empresa,:id_unidad,:cantidad_productos,:precioUnidad,:calidad,:fecha_vencimiento,:condiciones_almacenaje,:descripcion,:vigente,:ventaAlExtranjero)";
            comando.Parameters.AddWithValue("id_lote_producto", id_lote_producto);
            comando.Parameters.AddWithValue("id_producto", id_producto);
            comando.Parameters.AddWithValue("id_empresa", id_empresa);
            comando.Parameters.AddWithValue("id_unidad", id_unidad);
            comando.Parameters.AddWithValue("cantidad_productos", cantidad_productos);
            comando.Parameters.AddWithValue("precioUnidad", precioUnidad);
            comando.Parameters.AddWithValue("calidad", calidad);
            comando.Parameters.AddWithValue("fecha_vencimiento", fecha_vencimiento);
            comando.Parameters.AddWithValue("condiciones_almacenaje", condiciones_almacenaje);
            comando.Parameters.AddWithValue("descripcion", descripcion);
            comando.Parameters.AddWithValue("vigente", vigente);
            comando.Parameters.AddWithValue("ventaAlExtranjero", ventaAlExtranjero);
            comando.CommandType = CommandType.Text;
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
        }

        public void Modificar(int id_lote_producto, int id_producto, int id_empresa, int id_unidad, int cantidad_productos, int precioUnidad, int calidad, DateTime fecha_vencimiento, string condiciones_almacenaje, string descripcion, char vigente, char ventaAlExtranjero)
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "Insert into t_lote_producto values(:id_lote_producto,:id_producto,:id_empresa,:id_unidad,:cantidad_productos,:precioUnidad,:calidad,:fecha_vencimiento,:condiciones_almacenaje,:descripcion,:vigente,:ventaAlExtranjero)";
            comando.Parameters.AddWithValue("id_lote_producto", id_lote_producto);
            comando.Parameters.AddWithValue("id_producto", id_producto);
            comando.Parameters.AddWithValue("id_empresa", id_empresa);
            comando.Parameters.AddWithValue("id_unidad", id_unidad);
            comando.Parameters.AddWithValue("cantidad_productos", cantidad_productos);
            comando.Parameters.AddWithValue("precioUnidad", precioUnidad);
            comando.Parameters.AddWithValue("calidad", calidad);
            comando.Parameters.AddWithValue("fecha_vencimiento", fecha_vencimiento);
            comando.Parameters.AddWithValue("condiciones_almacenaje", condiciones_almacenaje);
            comando.Parameters.AddWithValue("descripcion", descripcion);
            comando.Parameters.AddWithValue("vigente", vigente);
            comando.Parameters.AddWithValue("ventaAlExtranjero", ventaAlExtranjero);
            comando.CommandType = CommandType.Text;
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
        }

        public void Eliminar(int id_lote_producto)
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "Delete from t_lote_producto where id_lote_producto=:id_lote_producto";
            comando.Parameters.AddWithValue("id_lote_producto", id_lote_producto);
            comando.CommandType = CommandType.Text;
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
        }

    }
}
