﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaipoGrandeDatos
{
    public class CD_Empresa
    {
        //Crud aplicado por Cristián Esquivel
        private ConexionBD conexion = new ConexionBD();

        OracleDataReader leer;
        DataTable tabla = new DataTable();
        OracleCommand comando = new OracleCommand();

        public DataTable Mostrar()
        {
            tabla = new DataTable();
            comando.Connection = conexion.AbrirConexion();
            comando = new OracleCommand("listarEmpresas", conexion.GetConexion());
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.Add("reg", OracleType.Cursor).Direction = ParameterDirection.Output;

            OracleDataAdapter adapter = new OracleDataAdapter();
            adapter.SelectCommand = comando;
            adapter.Fill(tabla);

            conexion.CerrarConexion();
            return tabla;
        }

        public DataTable MostrarPorID(int id)
        {
            tabla = new DataTable();
            comando = new OracleCommand();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "select * from t_empresa where idempresa=:id_rep";
            comando.Parameters.AddWithValue("id_rep", id);
            comando.CommandType = CommandType.Text;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }

        public void Insertar(int id_Empresa, string rut_empresa, string tipo_empresa, string nombre_comercial, string razon_social, string logo, string direccion, string ciudad, string pais, string fono1, string fono2, string email, string web)
        {
            comando.Connection = conexion.AbrirConexion();
            comando = new OracleCommand("CREAR_EMPRESA", conexion.GetConexion());
            comando.CommandType = CommandType.StoredProcedure;
            Byte[] f = new Byte[1];
            /*Representante - comando*/
            comando.Parameters.Add("nuevo_idempresa", OracleType.Number).Value = 0;
            comando.Parameters.Add("nuevo_rutempresa", OracleType.VarChar).Value = rut_empresa;
            comando.Parameters.Add("nuevo_tipoempresa", OracleType.VarChar).Value = tipo_empresa;
            comando.Parameters.Add("nuevo_nombrecomercial", OracleType.VarChar).Value = nombre_comercial;
            comando.Parameters.Add("nuevo_razonsocial", OracleType.VarChar).Value = razon_social;
            comando.Parameters.Add("nuevo_logo", OracleType.Blob).Value = DBNull.Value;
            comando.Parameters.Add("nuevo_direccion", OracleType.VarChar).Value = direccion;
            comando.Parameters.Add("nuevo_ciudad", OracleType.VarChar).Value = ciudad;
            comando.Parameters.Add("nuevo_pais", OracleType.VarChar).Value = pais;
            comando.Parameters.Add("nuevo_fono1", OracleType.VarChar).Value = fono1;
            comando.Parameters.Add("nuevo_fono2", OracleType.VarChar).Value = fono2;
            comando.Parameters.Add("nuevo_email", OracleType.VarChar).Value = email;
            comando.Parameters.Add("nuevo_web", OracleType.VarChar).Value = web;
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
        }

        public void Modificar(int id_Empresa, string rut_empresa, string tipo_empresa, string nombre_comercial, string razon_social, string logo, string direccion, string ciudad, string pais, string fono1, string fono2, string email, string web)
        {
            comando.Connection = conexion.AbrirConexion();
            comando = new OracleCommand("ACTUALIZAR_EMPRESA", conexion.GetConexion());
            comando.CommandType = CommandType.StoredProcedure;
            Byte[] f = new Byte[1];
            /*Representante - comando*/
            comando.Parameters.Add("nuevo_idempresa", OracleType.Number).Value = id_Empresa;
            comando.Parameters.Add("nuevo_rutempresa", OracleType.VarChar).Value = rut_empresa;
            comando.Parameters.Add("nuevo_tipoempresa", OracleType.VarChar).Value = tipo_empresa;
            comando.Parameters.Add("nuevo_nombrecomercial", OracleType.VarChar).Value = nombre_comercial;
            comando.Parameters.Add("nuevo_razonsocial", OracleType.VarChar).Value = razon_social;
            comando.Parameters.Add("nuevo_logo", OracleType.Blob).Value = DBNull.Value;
            comando.Parameters.Add("nuevo_direccion", OracleType.VarChar).Value = direccion;
            comando.Parameters.Add("nuevo_ciudad", OracleType.VarChar).Value = ciudad;
            comando.Parameters.Add("nuevo_pais", OracleType.VarChar).Value = pais;
            comando.Parameters.Add("nuevo_fono1", OracleType.VarChar).Value = fono1;
            comando.Parameters.Add("nuevo_fono2", OracleType.VarChar).Value = fono2;
            comando.Parameters.Add("nuevo_email", OracleType.VarChar).Value = email;
            comando.Parameters.Add("nuevo_web", OracleType.VarChar).Value = web;
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
        }

        public bool Eliminar(int id_Empresa)
        {
            comando.Connection = conexion.AbrirConexion();
            comando = new OracleCommand("VerificarDependenciasEmpresa", conexion.GetConexion());
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.Add("ID_EMPRESA", OracleType.Number).Value = id_Empresa;
            comando.Parameters.Add("reg", OracleType.Cursor).Direction = ParameterDirection.Output;
            OracleDataAdapter adapter = new OracleDataAdapter();
            adapter.SelectCommand = comando;
            DataTable t = new DataTable();
            adapter.Fill(t);
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
            if (int.Parse(t.Rows[0][0].ToString()) > 0)
            {
                return false;
            }

            comando.Connection = conexion.AbrirConexion();
            comando = new OracleCommand("ELIMINAR_EMPRESA", conexion.GetConexion());
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.Add("ID_EMPRESA", OracleType.Number).Value = id_Empresa;
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
            return true;
        }


    }
}

