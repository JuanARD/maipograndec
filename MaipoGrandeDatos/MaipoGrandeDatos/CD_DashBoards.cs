﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaipoGrandeDatos
{
    public class CD_DashBoards
    {
        private ConexionBD conexion = new ConexionBD();

        OracleDataReader leer;
        DataTable tabla = new DataTable();
        OracleCommand comando = new OracleCommand();


        OracleDataAdapter DA;
        DataTable DT;
        DataRow DR;
        string consulta;
        string CadenaConexion = ConexionBD.conex;


        #region Top Empresas


        public DataTable Top5Clientes()
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "Select * from V_TOP_5_CLIENTES_MAS_PEDIDOS";
            comando.CommandType = CommandType.Text;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }

        public DataTable Top5Productores()
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "Select * from V_TOP_5_PRODUCTORES_MAS_LOTES";
            comando.CommandType = CommandType.Text;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }

        public DataTable Top5Transportistas()
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "Select * from V_TOP_5_TRANSP_MAS_V_DESTINO";
            comando.CommandType = CommandType.Text;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }
        #endregion

        #region frutas y variedades

        public List<string[]> ContarVariedadesFrutas()
        {
            string[] variedad;
            List<string[]> frutas = new List<string[]>();
            DataRow fila;

            tabla = new DataTable();
            comando = new OracleCommand();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "select * from v_variedades_x_fruta";
            comando.CommandType = CommandType.Text;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();

            //foreach (DataRow fila in tabla.Rows.Count)
            //{
            //    variedad[0] = fila.ItemArray[0].ToString();
            //    variedad[1] = 34.ToString();

            //    frutas.Add(variedad);
            //}
            for (int i = 0; i < tabla.Rows.Count; i++)
            {
                variedad = new string[2];
                variedad[0] = tabla.Rows[i][0].ToString();
                variedad[1] = tabla.Rows[i][1].ToString();

                frutas.Add(variedad);
            }

            return frutas;
        }

        public List<string[]> ContarVariedadesFrutasXProductor()
        {
            string[] variedad;
            List<string[]> frutas = new List<string[]>();
            DataRow fila;

            tabla = new DataTable();
            comando = new OracleCommand();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "select * from V_CONTAR_VARIEDADES_X_PROD";
            comando.CommandType = CommandType.Text;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();

            //foreach (DataRow fila in tabla.Rows.Count)
            //{
            //    variedad[0] = fila.ItemArray[0].ToString();
            //    variedad[1] = 34.ToString();

            //    frutas.Add(variedad);
            //}
            for (int i = 0; i < tabla.Rows.Count; i++)
            {
                variedad = new string[2];
                variedad[0] = tabla.Rows[i][0].ToString();
                variedad[1] = tabla.Rows[i][1].ToString();

                frutas.Add(variedad);
            }

            return frutas;
        }
        #endregion

        #region PedidosTotales

        public double TotalPedidos()
        {
            double valorEsperado = 0.0;
            consulta = "Select count(*) as resultado from t_pedido";



            DA = new OracleDataAdapter(consulta, CadenaConexion);
            DT = new DataTable();
            DA.Fill(DT);

            DR = DT.Rows[0];


            valorEsperado = Convert.ToDouble(DR.ItemArray[0].ToString());


            return valorEsperado;
        }

        /// <summary>
        /// Devuelve la cuenta del estado de pedido con valor ingresado por código
        /// 0='Requiere_Validación', 1='Requiere_Negociación', 2='Subastado' 3='A_Despachar' 4='En_Recorrido' 5='Entregado' 6='Completado' 7='Anulado'
        /// </summary>
        /// <param name="indice"></param>
        /// <returns></returns>
        public double TotalPedidos(int indice)
        {
            double valorEsperado = 0.0;
            consulta = "Select * from v_contar_estados_pedidos";

            DA = new OracleDataAdapter(consulta, CadenaConexion);
            DT = new DataTable();
            DA.Fill(DT);
            DR = DT.Rows[0];
            valorEsperado = Convert.ToDouble(DR.ItemArray[indice].ToString());

            return valorEsperado;
        }

        public double TotalPedidosEnProceso()
        {
            double valorEsperado = TotalPedidos(0) + TotalPedidos(1) + TotalPedidos(2) + TotalPedidos(3) + TotalPedidos(4) + TotalPedidos(5);
            return valorEsperado;
        }

        public double TotalEntregados()
        {
            double valorEsperado = 0.0;
            consulta = "select count(*) from t_pedido where estadopedido like 'Entregado'";


            DA = new OracleDataAdapter(consulta, CadenaConexion);
            DT = new DataTable();
            DA.Fill(DT);

            DR = DT.Rows[0];


            valorEsperado = Convert.ToDouble(DR.ItemArray[0].ToString());


            return valorEsperado;
        }


        public double TotalAnulados()
        {
            double valorEsperado = 0.0;
            consulta = "select count(*) from t_pedido where estadopedido like 'Anulado'";


            DA = new OracleDataAdapter(consulta, CadenaConexion);
            DT = new DataTable();
            DA.Fill(DT);

            DR = DT.Rows[0];


            valorEsperado = Convert.ToDouble(DR.ItemArray[0].ToString());


            return valorEsperado;
        }


        public double TotalCompletados()
        {
            double valorEsperado = 0.0;
            consulta = "select count(*) from t_pedido where estadopedido like 'Completado'";


            DA = new OracleDataAdapter(consulta, CadenaConexion);
            DT = new DataTable();
            DA.Fill(DT);

            DR = DT.Rows[0];


            valorEsperado = Convert.ToDouble(DR.ItemArray[0].ToString());


            return valorEsperado;
        }
        #endregion

        #region Cantidad Representantes

        /// <summary>
        /// Devuelve la cantidad de representantes con el valor ingresado por código
        /// 0='Administrador', 1='Consultor', 2='Cliente_Externo' 3='Cliente_Interno' 4='Productor' 5='Transportista'
        /// </summary>
        /// <param name="indice"></param>
        /// <returns></returns>
        public int TotalRepresentantes(int indice)
        {
            int valorEsperado = 0;
            consulta = "Select * from V_CONTAR_ROLES_REPRESENTANTES";

            DA = new OracleDataAdapter(consulta, CadenaConexion);
            DT = new DataTable();
            DA.Fill(DT);
            DR = DT.Rows[0];
            valorEsperado = Convert.ToInt32(DR.ItemArray[indice].ToString());

            return valorEsperado;
        }
        #endregion
    }

}
