﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaipoGrandeDatos
{
    public class CD_Oferta_Transporte
    {
        //Crud aplicado Juan Román
        private ConexionBD conexion = new ConexionBD();

        OracleDataReader leer;
        DataTable tabla = new DataTable();
        OracleCommand comando = new OracleCommand();

        public DataTable Mostrar()
        {
            tabla = new DataTable();
            comando.Connection = conexion.AbrirConexion();
            comando = new OracleCommand("LISTAROFERTATRA", conexion.GetConexion());
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.Add("reg", OracleType.Cursor).Direction = ParameterDirection.Output;

            OracleDataAdapter adapter = new OracleDataAdapter();
            adapter.SelectCommand = comando;
            adapter.Fill(tabla);

            conexion.CerrarConexion();

            return tabla;
        }

        public DataTable MostrarSubastaEnOferta(int id)//Maximiliano González
        {
            // falta filtrar solo las ofertas sin comprobacion de estado t_Pedido
            comando = new OracleCommand();
            tabla = new DataTable();

            comando.Connection = conexion.AbrirConexion();

            comando.CommandText = "select t_oferta_transporte.idofertatransporte,t_oferta_transporte.idsubasta,t_oferta_transporte.idvehiculo,t_oferta_transporte.costotransporte,t_oferta_transporte.vigente from t_oferta_transporte " +
                "inner join t_subasta on t_oferta_transporte.idsubasta = t_subasta.idsubasta where t_oferta_transporte.idsubasta=:id_sub";


            comando.Parameters.AddWithValue("id_sub", id);
            comando.CommandType = CommandType.Text;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();

            return tabla;
        }

        public void Insertar(int id_oferta_transporte, int id_subasta, int id_vehiculo, int id_representante, float costo_transporte, char vigente)
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "Insert into t_oferta_transporte values(:id_oferta_transporte,:id_subasta,:id_vehiculo,:id_representante,:costo_transporte,:vigente)";
            comando.Parameters.AddWithValue("id_oferta_transporte", id_oferta_transporte);
            comando.Parameters.AddWithValue("id_subasta", id_subasta);
            comando.Parameters.AddWithValue("id_vehiculo", id_vehiculo);
            comando.Parameters.AddWithValue("id_representante", id_representante);
            comando.Parameters.AddWithValue("costo_transporte", costo_transporte);
            comando.Parameters.AddWithValue("vigente", vigente);
            comando.CommandType = CommandType.Text;
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
        }

        public void Modificar(int id_oferta_transporte, int id_subasta, int id_vehiculo, int id_representante, float costo_transporte, char vigente)
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "Update t_oferta_transporte set id_oferta_transporte=:id_oferta_transporte,id_subasta=:id_subasta,id_vehiculo=:id_vehiculo,id_representante=:id_representante,costo_transporte=:costo_transporte,vigente=:vigente where id_oferta_transporte= :id_oferta_transporte";
            comando.Parameters.AddWithValue("id_oferta_transporte", id_oferta_transporte);
            comando.Parameters.AddWithValue("costo_transporte", costo_transporte);
            comando.Parameters.AddWithValue("vigente", vigente);
            comando.CommandType = CommandType.Text;
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
        }

        public void ModificarEstadoPedidoSubastaOferta(int id_oferta) // Maximiliano González
        {
            comando.Connection = conexion.AbrirConexion();
            comando = new OracleCommand("ACTUALIZAR_OFERTAPEDIDO", conexion.GetConexion());
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.Add("nuevo_idoferta", OracleType.Number).Value = id_oferta;
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
        }

        public void Eliminar(int id_oferta_transporte)
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "Delete from t_oferta_transporte where id_oferta_transporte=:id_oferta_transporte";
            comando.Parameters.AddWithValue("id_oferta_transporte", id_oferta_transporte);
            comando.CommandType = CommandType.Text;
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
        }

    }
}
