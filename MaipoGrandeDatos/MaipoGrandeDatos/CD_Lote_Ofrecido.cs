﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaipoGrandeDatos
{
    public class CD_Lote_Ofrecido
    {
        //Crud aplicado Juan Román
        private ConexionBD conexion = new ConexionBD();

        OracleDataReader leer;
        DataTable tabla = new DataTable();
        OracleCommand comando = new OracleCommand();

        public DataTable Mostrar()
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "Select * from t_lote_ofrecido";
            comando.CommandType = CommandType.Text;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }

        public DataTable MostrarLotesByIdPedido(int id) // Maximiliano González
        {
            tabla = new DataTable();
            comando = new OracleCommand();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "SELECT b.idloteofrecido, b.idproductopedido, a.idproducto,  b.enbodega,   a.cantidad,  b.cantidadsaldo, b.vigente, b.idrepresentante FROM  t_lote_ofrecido b Join t_producto_pedido a on b.idproductopedido=a.idproductopedido where a.idpedido=:id_pedido ORDER BY b.idloteofrecido asc";
            comando.Parameters.AddWithValue("id_pedido", id);
            comando.CommandType = CommandType.Text;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }

        public void ValidarLoteProductoPedido(int id) // Maximiliano González
        {
            comando.Connection = conexion.AbrirConexion();
            comando = new OracleCommand("ACTUALIZAR_LOTEOFRECIDO_PEDIDO", conexion.GetConexion());
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.Add("nuevo_idproductopedido", OracleType.Number).Value = id;
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
        }

        public void Insertar(int id_lote_ofrecido, int id_producto_pedido, int id_lote_producto, string comentario, char en_bodega, int cantidad_saldo, int id_representantem, char vigente)
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "Insert into t_lote_ofrecido values(:id_lote_ofrecido,:id_producto_pedido,:id_lote_producto,:comentario,:en_bodega,:cantidad_saldo,:id_representantem,:vigente)";
            comando.Parameters.AddWithValue("id_lote_ofrecido", id_lote_ofrecido);
            comando.Parameters.AddWithValue("id_producto_pedido", id_producto_pedido);
            comando.Parameters.AddWithValue("id_lote_producto", id_lote_producto);
            comando.Parameters.AddWithValue("comentario_estado", comentario);
            comando.Parameters.AddWithValue("en_bodega", en_bodega);
            comando.Parameters.AddWithValue("cantidad_saldo", cantidad_saldo);
            comando.Parameters.AddWithValue("representante", id_representantem);
            comando.Parameters.AddWithValue("vigente", vigente);
            comando.CommandType = CommandType.Text;
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
        }

        public void Modificar(int id_lote_ofrecido, int id_producto_pedido, int id_lote_producto, string comentario, char en_bodega, int cantidad_saldo, int id_representantem, char vigente)
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "Insert into t_lote_ofrecido values(:id_lote_ofrecido,:id_producto_pedido,:id_lote_producto,:comentario,:en_bodega,:cantidad_saldo,:id_representantem,:vigente)";
            comando.Parameters.AddWithValue("id_lote_ofrecido", id_lote_ofrecido);
            comando.Parameters.AddWithValue("id_producto_pedido", id_producto_pedido);
            comando.Parameters.AddWithValue("id_lote_producto", id_lote_producto);
            comando.Parameters.AddWithValue("comentario_estado", comentario);
            comando.Parameters.AddWithValue("en_bodega", en_bodega);
            comando.Parameters.AddWithValue("cantidad_saldo", cantidad_saldo);
            comando.Parameters.AddWithValue("representante", id_representantem);
            comando.Parameters.AddWithValue("vigente", vigente);
            comando.CommandType = CommandType.Text;
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
        }

        public void Eliminar(int id_lote_ofrecido)
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "Delete from t_lote_ofrecido where id_lote_ofrecido=:id_lote_ofrecido";
            comando.Parameters.AddWithValue("id_lote_ofrecido", id_lote_ofrecido);
            comando.CommandType = CommandType.Text;
            comando.ExecuteNonQuery();
            conexion.CerrarConexion();
        }
    }
}
