﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MaipoGrandeNegocio;
namespace MaipoGrandeVista
{
    /// Modulo que musestra las ofertas de una subasta sin oferta de transporte || Maximiliano González
    /// Lógica de interacción para VerOfertas.xaml || Maximiliano González

    public partial class VerOfertas : Window
    {
        MainWindow mainOferta;
        DataRowView datosSubasta;
        DataRowView datosPedido;
        DataRowView datosVehiculo;
        DataRowView datosRepresentante;

        public VerOfertas(MainWindow main, DataRowView subasta)
        {
            InitializeComponent();
            mainOferta = main;
            datosSubasta = subasta;
            gridOfertasSubasta.ItemsSource = CN_Oferta_Transporte.Instance.MostrarSubastaEnOferta(int.Parse(datosSubasta[0].ToString())).DefaultView;
        }

        private void btnVolver_Click(object sender, RoutedEventArgs e)
        {

            mainOferta.Show();
            Close();
        }

        private void BtnValidarOferta_Click(object sender, RoutedEventArgs e)
        {
            DataRowView d = gridOfertasSubasta.SelectedItem as DataRowView;
            MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Desea Validar la Oferta de transporte n° : " + d[0].ToString() + " seleccionada, ¿Continuar?", "¿Confirmar Cambios?", System.Windows.MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (messageBoxResult == MessageBoxResult.No)
            {
                return;
            }
            try
            {
                CN_Oferta_Transporte.Instance.ValidarOfertaPedido(int.Parse(d[0].ToString()));

                MessageBox.Show("Oferta n°: " + d[0].ToString() + " de Transporte validada con éxito");
                btnValidarOferta.IsEnabled = false;
                lblmsj.Visibility = Visibility.Visible;
                gridOfertasSubasta.ItemsSource = CN_Oferta_Transporte.Instance.MostrarSubastaEnOferta(int.Parse(datosSubasta[0].ToString())).DefaultView;
                gridOfertasSubasta.Items.Refresh();
            }
            catch (Exception f)
            {
                MessageBox.Show(f.Message);
                //XX error por corregir falla refrescar necesario para mostrar el cambio de la subasta elegida vigente de 0 a 1
                //XX || causa; CN_Oferta_Transporte.Instance.
            }

        }

        private void GridOfertasSubasta_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            datosPedido = CN_Pedido.Instance.MostrarPedidoById(int.Parse(datosSubasta[2].ToString())).DefaultView[0];

            lblFechaInicial.Content = datosPedido[3].ToString(); //fechainiciopedido
            lblEstado.Content = datosPedido[7].ToString(); // estadopedido
            lblCalidad.Content = datosPedido[8].ToString();// calidadentrega
            lblDestino.Content = datosPedido[12].ToString(); // direccion
            lblContacto.Content = datosPedido[13].ToString(); // fono

            // Extraer datos vehiculo

            DataRowView d = gridOfertasSubasta.SelectedItem as DataRowView;
            if (d!=null)
            {

            
            datosVehiculo = CN_Vehiculo.Instance.MostrarVehiculoById(int.Parse(d[2].ToString())).DefaultView[0];

            lblPatente.Content = datosVehiculo[2].ToString(); //PATENTE
            lblClase.Content = datosVehiculo[3].ToString(); //CLASE
            lblCapacidad.Content = datosVehiculo[4].ToString(); //CAPACIDADKG
            lblAncho.Content = datosVehiculo[5].ToString(); //ANCHO
            lblAlto.Content = datosVehiculo[6].ToString(); //ALTO
            lblLargo.Content = datosVehiculo[7].ToString(); //CMLARGO
            //FRIGORIFICO SI O NO
            if (datosVehiculo[8].ToString() == "1") { lblFrigorifico.Content = "Sí"; }
            if (datosVehiculo[8].ToString() == "0") { lblFrigorifico.Content = "No"; }

            //Extraer Datos Representante
            datosRepresentante = CN_Representante.Instance.MostrarRepresentanteEmpresaID(int.Parse(datosSubasta[5].ToString())).DefaultView[0];

            lblNombres.Content = datosRepresentante[0].ToString() + " " + datosRepresentante[1].ToString() + " " + datosRepresentante[2].ToString();
            lblRut.Content = datosRepresentante[3].ToString();
            lblTelefono.Content = datosRepresentante[4].ToString();
            lblEmpresa.Content = datosRepresentante[5].ToString();
            lblCorreoEmpresa.Content = datosRepresentante[2].ToString();
            }
        }

    }
}
