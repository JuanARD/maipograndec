﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MaipoGrandeNegocio;

namespace MaipoGrandeVista
{

    // Lógica de interacción para VerLotesProductoPedido.xaml
    // Maximiliano González
    public partial class VerLotesProductoPedido : Window
    {
        MainWindow mainLotes;
        DataRowView datosPedido;
        DataRowView datosRepresentante;
        DataRowView datosLoteProducto;
        DataRowView datosProducto;

        CN_Lote_Ofrecido lot = new CN_Lote_Ofrecido();
        CN_Lote_Producto lotp = new CN_Lote_Producto();

        public VerLotesProductoPedido(MainWindow main, DataRowView pedido)
        {
            InitializeComponent();
            mainLotes = main;
            datosPedido = pedido;

            gridLoteOfrecido.ItemsSource = lot.MostrarLotesByIdPedido(int.Parse(datosPedido[0].ToString())).DefaultView;
        }

        private void btnVolver_Click(object sender, RoutedEventArgs e)
        {
            mainLotes.Show();
            Close();
        }

        private void BtnValidarOferta_Click(object sender, RoutedEventArgs e)
        {
            DataRowView d = gridLoteOfrecido.SelectedItem as DataRowView;
            MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Desea validar el Lote de Productos n° : " + d[0].ToString() + " seleccionado, ¿Continuar?", "¿Confirmar Cambios?", System.Windows.MessageBoxButton.YesNo, MessageBoxImage.Warning);

            if (messageBoxResult == MessageBoxResult.No) { return; }
            try
            {
                lot.ValidarLoteProductoPedido(int.Parse(d[0].ToString()));
                MessageBox.Show("Lote n°: " + d[0].ToString() + " de Productos validado con éxito");

                btnValidarLoteOfrecido.IsEnabled = false;
                lblmsjlote.Visibility = Visibility.Visible;
                gridLoteOfrecido.ItemsSource = lot.MostrarLotesByIdPedido(int.Parse(datosPedido[0].ToString())).DefaultView;
                gridLoteOfrecido.Items.Refresh();
            }
            catch (Exception f)
            {
                MessageBox.Show(f.Message);
            }
        }

        private void GridLoteOfrecido_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataRowView d = gridLoteOfrecido.SelectedItem as DataRowView;

            if (d != null)
            {
                //Extraer Datos Representante
                datosRepresentante = CN_Representante.Instance.MostrarRepresentanteEmpresaID(int.Parse(d[7].ToString())).DefaultView[0];

                lblNombres.Content = datosRepresentante[0].ToString() + " " + datosRepresentante[1].ToString() + " " + datosRepresentante[2].ToString();
                lblRut.Content = datosRepresentante[3].ToString();
                lblTelefono.Content = datosRepresentante[4].ToString();
                lblEmpresa.Content = datosRepresentante[5].ToString();
                lblCorreoEmpresa.Content = datosRepresentante[6].ToString();

                // Extraer Datos Lotes Producto
                datosLoteProducto = lotp.MostrarLoteProductoById(int.Parse(d[2].ToString())).DefaultView[0];

                lblCantidad.Content = datosLoteProducto[4].ToString();
                lblPrecio.Content = "$" + datosLoteProducto[5].ToString();
                lblCalidad.Content = datosLoteProducto[6].ToString();
                lblEmbalaje.Content = datosLoteProducto[8].ToString();
                lblFechaInicio.Content = datosLoteProducto[7].ToString();

                if (datosLoteProducto[6].ToString() == "1") { lblCalidad.Content = "Mínima"; }
                if (datosLoteProducto[6].ToString() == "2") { lblCalidad.Content = "Baja"; }
                if (datosLoteProducto[6].ToString() == "3") { lblCalidad.Content = "Media"; }
                if (datosLoteProducto[6].ToString() == "4") { lblCalidad.Content = "Alta"; }
                if (datosLoteProducto[6].ToString() == "5") { lblCalidad.Content = "Máxima"; }

                //Extraer Datos Producto
                datosProducto = CN_Producto.Instance.MostrarProductoById(int.Parse(d[2].ToString())).DefaultView[0];

                lblNombreProducto.Content = datosProducto[1].ToString();
                lblTipo.Content = datosProducto[2].ToString();
            }

        }
    }
}
