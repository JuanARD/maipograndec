﻿using MaipoGrandeDatos;
using MaipoGrandeNegocio;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MaipoGrandeVista
{
    // Módulo que Muestra el detalle del pedido, permitiendole también cambiar el estado del pedido al administrador
    // Módulo Creado por Cristián Esquivel
    public partial class VerDetallePedido : Window
    {
        MainWindow main;
        DataRowView datosPedido;
        DataRowView datosRepresentante;
        // Constructor, inicializa la pantalla para mostrar los datos
        public VerDetallePedido(MainWindow m, DataRowView pedido)
        {
            InitializeComponent();
            main = m;
            //Extraer los datos del pedido seleccionado en la pantalla anterior
            datosPedido = pedido;
            ExtraerPedido();
            //Extraer los datos del representante asignado al pedido
            datosRepresentante = CN_Representante.Instance.MostrarRepresentantePorID(int.Parse(datosPedido[1].ToString())).DefaultView[0];
            ExtraerRepresentante();
            gridProductosEnPedido.ItemsSource = CN_Producto_Pedido.Instance.MostrarProductosEnPedido(int.Parse(datosPedido[0].ToString())).DefaultView;

            CheckValidar();
            CheckAnular();
        }

        private void btnVolver_Click(object sender, RoutedEventArgs e)
        {
            main.Show();
            Close();
        }

        // Método para cambiar el estado del pedido
        private void btnCambiarEstado_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Va a Cambiar el Estado del Pedido, esto podría causar cambios graves en caso de equivocarse, ¿Continuar?", "¿Confirmar Cambios?", System.Windows.MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (messageBoxResult == MessageBoxResult.No)
            {
                return;
            }
            try
            {
                CN_Pedido.Instance.ModificarPedido(int.Parse(datosPedido[0].ToString()),//IDPEDIDO
                int.Parse(datosPedido[1].ToString()),//IDREPRESENTANTE
                datosPedido[2].ToString(),//RUTACONTRATO
                DateTime.Parse(datosPedido[3].ToString()),//FECHAINICIOPEDIDO
                DateTime.Parse(datosPedido[4].ToString()),//FECHAENTREGAESTIPULADA
                datosPedido[5].ToString(),//FECHAENTREGAREAL
                datosPedido[6].ToString(),//OBSERVACIONINICIAL
                cboEstados.Text,//ESTADOPEDIDO
                datosPedido[8].ToString(),//CALIDADENTREGA
                datosPedido[9].ToString(),//OBSERVACIONENTREGA
                char.Parse(datosPedido[10].ToString()),//DEVUELTO
                char.Parse(datosPedido[11].ToString()),//VIGENTE
                datosPedido[12].ToString(),//DIRECCION
                datosPedido[13].ToString()//FONO
                );
                lblEstado.Content = cboEstados.Text;
                CheckValidar();
                CheckAnular();
                MessageBox.Show("Estado Modificado");
            }
            catch (Exception f)
            {
                MessageBox.Show(f.Message);
            }

        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            main.Show();
        }

        // Método para validar el pedido y publicarlo, solo para pedidos que están en estado "Requiere Validación"
        private void btnValidarPedido_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Va a Validar y Publicar el Pedido Actual, ¿Continuar?", "¿Confirmar Cambios?", System.Windows.MessageBoxButton.YesNo, MessageBoxImage.Information);
                if (messageBoxResult == MessageBoxResult.No)
                {
                    return;
                }
                CN_Pedido.Instance.ModificarPedido(int.Parse(datosPedido[0].ToString()),//IDPEDIDO
                int.Parse(datosPedido[1].ToString()),//IDREPRESENTANTE
                datosPedido[2].ToString(),//RUTACONTRATO
                DateTime.Parse(datosPedido[3].ToString()),//FECHAINICIOPEDIDO
                DateTime.Parse(datosPedido[4].ToString()),//FECHAENTREGAESTIPULADA
                datosPedido[5].ToString(),//FECHAENTREGAREAL
                datosPedido[6].ToString(),//OBSERVACIONINICIAL
                "Requiere Negociación",//ESTADOPEDIDO
                datosPedido[8].ToString(),//CALIDADENTREGA
                datosPedido[9].ToString(),//OBSERVACIONENTREGA
                char.Parse(datosPedido[10].ToString()),//DEVUELTO
                char.Parse(datosPedido[11].ToString()),//VIGENTE
                datosPedido[12].ToString(),//DIRECCION
                datosPedido[13].ToString()//FONO
                );
                lblEstado.Content = "Requiere Negociación";
                CheckValidar();
                CheckAnular();
                MessageBox.Show("Pedido Publicado");
            }
            catch (Exception f)
            {
                MessageBox.Show(f.Message);
            }
        }

        // Método que extrae el pedido de la pantalla anterior para mostrarlos en la pantalla actual
        void ExtraerPedido()
        {
            lblIDPedido.Content = datosPedido[0].ToString();//IDPEDIDO
            lblDireccion.Content = datosPedido[12].ToString();//DIRECCION
            lblFono.Content = datosPedido[13].ToString();//FONO
            lblFechaInicioPedido.Content = datosPedido[3].ToString();//FECHAINICIOPEDIDO
            lblFechaEntregaEstipulada.Content = datosPedido[4].ToString();//FECHAENTREGAESTIPULADA
            lblFechaEntregaReal.Content = datosPedido[5].ToString();//FECHAENTREGAREAL
            lblEstado.Content = datosPedido[7].ToString();//ESTADOPEDIDO
            if (datosPedido[11].ToString() == "1")//VIGENTE
            {
                lblVigente.Content = "Aún Vigente";
            }
            else
            {
                lblVigente.Content = "No Vigente";
            }
        }
        // Método que extrae los datos del representante asociado al pedido
        void ExtraerRepresentante()
        {
            lblRutRep.Content = datosRepresentante[2].ToString();//RUTREPRESENTANTE
            lblNombreRep.Content = datosRepresentante[4].ToString() + " " + datosRepresentante[5].ToString() + " " + datosRepresentante[6].ToString();//NOMBRE REPRESENTANTE
            lblFechaNacRep.Content = datosRepresentante[8].ToString();//FECHANACIMIENTO
            lblFonoPrincipalRep.Content = datosRepresentante[9].ToString();//FONO1
            lblDirecciónRep.Content = datosRepresentante[11].ToString();//DIRECCION 
        }

        // Método que valida si es correcto mostrar el botón para validar y publicar el pedido
        void CheckValidar()
        {
            if (!lblEstado.Content.Equals("Requiere Validación"))
            {
                btnValidarPedido.Visibility = Visibility.Hidden;
            }
            else if (lblEstado.Content.Equals("Requiere Validación"))
            {
                btnValidarPedido.Visibility = Visibility.Visible;
            }
        }

        // Método que valida si es correcto mostrar el botón para anular el pedido
        void CheckAnular()
        {
            if (lblEstado.Content.Equals("Anulado"))
            {
                btnAnularPedido.Visibility = Visibility.Hidden;
            }
            else
            {
                btnAnularPedido.Visibility = Visibility.Visible;
            }
        }

        // Método que cambia el estado del pedido a "Anulado", cambiando también su vigencia
        private void btnAnularPedido_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Está a punto de anular un pedido, eso causará que el pedido pierda su validez, ¿Continuar?", "ATENCION!!", System.Windows.MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (messageBoxResult == MessageBoxResult.No)
                {
                    return;
                }
                CN_Pedido.Instance.ModificarPedido(int.Parse(datosPedido[0].ToString()),//IDPEDIDO
                int.Parse(datosPedido[1].ToString()),//IDREPRESENTANTE
                datosPedido[2].ToString(),//RUTACONTRATO
                DateTime.Parse(datosPedido[3].ToString()),//FECHAINICIOPEDIDO
                DateTime.Parse(datosPedido[4].ToString()),//FECHAENTREGAESTIPULADA
                datosPedido[5].ToString(),//FECHAENTREGAREAL
                datosPedido[6].ToString(),//OBSERVACIONINICIAL
                "Anulado",//ESTADOPEDIDO
                datosPedido[8].ToString(),//CALIDADENTREGA
                datosPedido[9].ToString(),//OBSERVACIONENTREGA
                char.Parse(datosPedido[10].ToString()),//DEVUELTO
                char.Parse("0"),//VIGENTE
                datosPedido[12].ToString(),//DIRECCION
                datosPedido[13].ToString()//FONO
                );
                lblEstado.Content = "Anulado";
                lblVigente.Content = "No Vigente";
                CheckValidar();
                CheckAnular();
                MessageBox.Show("Pedido Anulado");
            }
            catch (Exception f)
            {
                MessageBox.Show(f.Message);
            }
        }
    }
}
