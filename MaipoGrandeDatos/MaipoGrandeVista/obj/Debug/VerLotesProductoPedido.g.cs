﻿#pragma checksum "..\..\VerLotesProductoPedido.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "395C1783D8156AA8898852CBA779BE28229DD1E0FB85A20776825B9615D5CD8C"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using MaipoGrandeVista;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace MaipoGrandeVista {
    
    
    /// <summary>
    /// VerLotesProductoPedido
    /// </summary>
    public partial class VerLotesProductoPedido : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 28 "..\..\VerLotesProductoPedido.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.GroupBox groupBoxLotes;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\VerLotesProductoPedido.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid gridLoteOfrecido;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\VerLotesProductoPedido.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.GroupBox groupBoxCamion;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\VerLotesProductoPedido.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnVolverPedidos;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\VerLotesProductoPedido.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnValidarLoteOfrecido;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\VerLotesProductoPedido.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.GroupBox groupBoxRepresentante_Copy;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\VerLotesProductoPedido.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblNombres;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\VerLotesProductoPedido.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblRut;
        
        #line default
        #line hidden
        
        
        #line 43 "..\..\VerLotesProductoPedido.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblTelefono;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\VerLotesProductoPedido.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblEmpresa;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\VerLotesProductoPedido.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblCorreoEmpresa;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\VerLotesProductoPedido.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblCantidad;
        
        #line default
        #line hidden
        
        
        #line 57 "..\..\VerLotesProductoPedido.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblPrecio;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\VerLotesProductoPedido.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblCalidad;
        
        #line default
        #line hidden
        
        
        #line 59 "..\..\VerLotesProductoPedido.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblEmbalaje;
        
        #line default
        #line hidden
        
        
        #line 60 "..\..\VerLotesProductoPedido.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblFechaInicio;
        
        #line default
        #line hidden
        
        
        #line 61 "..\..\VerLotesProductoPedido.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblNombreProducto;
        
        #line default
        #line hidden
        
        
        #line 62 "..\..\VerLotesProductoPedido.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblTipo;
        
        #line default
        #line hidden
        
        
        #line 63 "..\..\VerLotesProductoPedido.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblmsjlote;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/MaipoGrandeVista;component/verlotesproductopedido.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\VerLotesProductoPedido.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.groupBoxLotes = ((System.Windows.Controls.GroupBox)(target));
            return;
            case 2:
            this.gridLoteOfrecido = ((System.Windows.Controls.DataGrid)(target));
            
            #line 29 "..\..\VerLotesProductoPedido.xaml"
            this.gridLoteOfrecido.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.GridLoteOfrecido_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 3:
            this.groupBoxCamion = ((System.Windows.Controls.GroupBox)(target));
            return;
            case 4:
            this.btnVolverPedidos = ((System.Windows.Controls.Button)(target));
            
            #line 34 "..\..\VerLotesProductoPedido.xaml"
            this.btnVolverPedidos.Click += new System.Windows.RoutedEventHandler(this.btnVolver_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.btnValidarLoteOfrecido = ((System.Windows.Controls.Button)(target));
            
            #line 35 "..\..\VerLotesProductoPedido.xaml"
            this.btnValidarLoteOfrecido.Click += new System.Windows.RoutedEventHandler(this.BtnValidarOferta_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.groupBoxRepresentante_Copy = ((System.Windows.Controls.GroupBox)(target));
            return;
            case 7:
            this.lblNombres = ((System.Windows.Controls.Label)(target));
            return;
            case 8:
            this.lblRut = ((System.Windows.Controls.Label)(target));
            return;
            case 9:
            this.lblTelefono = ((System.Windows.Controls.Label)(target));
            return;
            case 10:
            this.lblEmpresa = ((System.Windows.Controls.Label)(target));
            return;
            case 11:
            this.lblCorreoEmpresa = ((System.Windows.Controls.Label)(target));
            return;
            case 12:
            this.lblCantidad = ((System.Windows.Controls.Label)(target));
            return;
            case 13:
            this.lblPrecio = ((System.Windows.Controls.Label)(target));
            return;
            case 14:
            this.lblCalidad = ((System.Windows.Controls.Label)(target));
            return;
            case 15:
            this.lblEmbalaje = ((System.Windows.Controls.Label)(target));
            return;
            case 16:
            this.lblFechaInicio = ((System.Windows.Controls.Label)(target));
            return;
            case 17:
            this.lblNombreProducto = ((System.Windows.Controls.Label)(target));
            return;
            case 18:
            this.lblTipo = ((System.Windows.Controls.Label)(target));
            return;
            case 19:
            this.lblmsjlote = ((System.Windows.Controls.Label)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

