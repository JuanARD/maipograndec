﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MaipoGrandeNegocio;
using MaipoGrandeDatos;
using System.Data.OracleClient;
using System.Data;
using System.Text.RegularExpressions;
using System.Windows.Threading;
using LiveCharts;
using LiveCharts.Wpf;
using System.Collections;
using Microsoft.Reporting.WinForms;

namespace MaipoGrandeVista
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {


        CN_Subasta sub = new CN_Subasta();
        VerDetallePedido p;
        VerOfertas vo;
        VerLotesProductoPedido plp;
        ReportDataSource ds;
        CN_Reportes rp = new CN_Reportes();
        //Ricardo   
        CN_Contrato con = new CN_Contrato();
        

        OracleConnection Conexion = new OracleConnection(ConexionBD.conex);
        public MainWindow()
        {
            Perdida perdida = new Perdida();
            DataContext = new PerdidaViewModel(perdida);
            //No entra al Mainwindows
            if (Session.TipoUsuario == null)
            {
                Login login = new Login();
                login.Show();
                this.Hide();
               
            }
            else
            {
                InitializeComponent();

                //Entra como consultor
                if (Session.TipoUsuario.Equals("Consultor"))
                {

                    //Oculta pestañas de administrador:
                    tbnv1_inicio_admin.Visibility = Visibility.Collapsed;
                    tbnv1_moduloEmpresas.Visibility = Visibility.Collapsed;
                    tbnv1_moduloUsuarios.Visibility = Visibility.Collapsed;
                    tbnv1_moduloPedidos.Visibility = Visibility.Collapsed;
                    tbnv1_moduloSubastas.Visibility = Visibility.Collapsed;

                    //Muestra el dashboard de consultor por defecto al entrar
                    Dispatcher.BeginInvoke((Action)(() => tbc_general.SelectedIndex = 1));
                }
                //Entra como administrador
                if (Session.TipoUsuario.Equals("Administrador"))
                {
                    //Oculta pestañas de consultor:
                    tbnv1_inicio_consultor.Visibility = Visibility.Collapsed;
                    tbnv1_moduloReportes.Visibility = Visibility.Collapsed;

                }
                btnExpandPerfil.Header = Session.NombreUsuario;

                //Independientemente de cómo entre, cargo todos los datos por ahora
                gridValidarLotePedidos.ItemsSource = CN_Pedido.Instance.MostrarPedidoReqNegociacion().DefaultView;
                gridSubastas.ItemsSource = sub.MostrarSubastaAdespacho().DefaultView;
                gridPedidos.ItemsSource = CN_Pedido.Instance.MostrarPedido().DefaultView;
                gridEmpresa.ItemsSource = CN_Empresa.Instance.MostrarEmpresa().DefaultView;
                gridusuariosUsu.ItemsSource = CN_Usuario.Instance.MostrarUsuario().DefaultView;
                gridUnidades.ItemsSource = CN_Unidad_Medicion.Instance.MostrarUnidadMedicion().DefaultView;
                dataVehi.ItemsSource = CN_Vehiculo.Instance.MostrarVehiculos().DefaultView;
                dataOferta.ItemsSource = CN_Oferta_Transporte.Instance.MostrarOfertatra().DefaultView;
                dataProductos.ItemsSource = CN_Producto.Instance.MostrarProduct().DefaultView;
                gridSubasta.ItemsSource = sub.MostrarSubasta().DefaultView;

                cargarcmbidempresa(cmbIdEmpresaUsu);
                cargarcbIdPedido(cbIdPedido);
                cargarcbIdRepresentante(cbIdRepresentante);
                

                //Módulo contratos
                this.subirDataGrid();
                this.cargarComboBoxYTextBox();

                //DASHBOARD CONSULTOR
                this.consultorPieChart();


                //DashBoard Admin
                IniciarDashAdmin();
            }
        }




        public void cargarcmbidempresa(ComboBox cmbIdEmpresaUsu)
        {
            //autor Juan Román
            /*Método para obtener ID de empresa y cargarla en combobox*/
            cmbIdEmpresaUsu.Items.Clear();
            Conexion.Open();
            OracleCommand comando = new OracleCommand("SELECT * FROM T_EMPRESA", Conexion);
            OracleDataReader Registro = comando.ExecuteReader();

            while (Registro.Read())
            {
                cmbIdEmpresaUsu.Items.Add(Registro[0].ToString());
            }

            Conexion.Close();
            cmbIdEmpresaUsu.Items.Insert(0, "Seleccione Empresa");
            cmbIdEmpresaUsu.SelectedIndex = 0;

        }




        public void cargarcbIdPedido(ComboBox cbIdPedido)
        {
            cbIdPedido.Items.Clear();
            Conexion.Open();
            OracleCommand comando = new OracleCommand("SELECT * FROM T_PEDIDO", Conexion);
            OracleDataReader registro = comando.ExecuteReader();

            while (registro.Read())
            {
                cbIdPedido.Items.Add(registro[0].ToString());

            }

            Conexion.Close();
            cbIdPedido.Items.Insert(0, "Seleccione Pedido");
            cbIdPedido.SelectedIndex = 0;

        }

        public void cargarcbIdRepresentante(ComboBox cbIdRepresentante)
        {
            cbIdRepresentante.Items.Clear();
            Conexion.Open();
            OracleCommand comando = new OracleCommand("SELECT * FROM T_REPRESENTANTE", Conexion);
            OracleDataReader registro = comando.ExecuteReader();

            while (registro.Read())
            {
                cbIdRepresentante.Items.Add(registro[0].ToString());
            }

            Conexion.Close();
            cbIdRepresentante.Items.Insert(0, "Seleccione Representante");
            cbIdRepresentante.SelectedIndex = 0;

        }

        private void BtnSetting_Click(object sender, RoutedEventArgs e)
        {
            Dispatcher.BeginInvoke((Action)(() => tbc_general.SelectedIndex = 2));
            Dispatcher.BeginInvoke((Action)(() => tbc_gestionUsuarios.SelectedIndex = 0));
        }

        private void Cmbsexo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }

        #region  Contrato Ricardo //________________________________________________________________

        private void subirDataGrid()
        {
            Conexion.Open();
            OracleCommand cmd = Conexion.CreateCommand();
            cmd.CommandText = "SELECT IDCONTRATO,IDEMPRESA,FECHAINICIO,FECHATERMINO,RUTADOCUMENTO,VIGENTE FROM T_CONTRATO";
            cmd.CommandType = CommandType.Text;
            OracleDataReader dr = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            midatagrid.ItemsSource = dt.DefaultView;
            Conexion.Close();
        }


        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.subirDataGrid();
            this.cargarComboBoxYTextBox();

        }

        private void cargarComboBoxYTextBox()
        {
            /*string sentenciaComboBoxCargarNombre = "select a.idempresa ,b.nombrecomercial from bdmaipogrande.t_contrato a join bdmaipogrande.t_empresa b on a.idempresa = b.idempresa";*/


            Conexion.Open();
            OracleCommand comando = new OracleCommand("SELECT IDEMPRESA FROM T_EMPRESA", Conexion);
            OracleDataReader registro = comando.ExecuteReader();
            while (registro.Read())
            {

                cmbIdEmpresa.Items.Add(registro["IDEMPRESA"]);
                /* txtNombreEmpresa.Text = (registro["NOMBRECOMERCIAL"].ToString());*/

            }

            Conexion.Close();

        }

        private void BtnAgregar_Click(object sender, RoutedEventArgs e)
        {
            Conexion.Open();

            if (cmbIdEmpresa.SelectedItem == null && dtInicio.SelectedDate == null && dtTermino.SelectedDate == null && txtRutaDoc.Text == "" && cmbVigenteContrato.SelectedItem == null)
            {
                MessageBox.Show("Todos los campos se encuentran vacíos porfavor llenar con la informacion adecuada", "Mensaje", MessageBoxButton.OK, MessageBoxImage.Error);
                this.limpiarTodo();

            }
            else if (cmbIdEmpresa.SelectedItem == null)
            {
                MessageBox.Show("Error, porfavor ingresa id empresa!", "Mensaje", MessageBoxButton.OK, MessageBoxImage.Error);
                this.limpiarTodo();
            }
            else if (dtInicio.SelectedDate == null)
            {
                MessageBox.Show("ERROR, Ingresar fecha inicial!", "Mensaje", MessageBoxButton.OK, MessageBoxImage.Error);
                this.limpiarTodo();

            }
            else if (dtTermino.SelectedDate == null)
            {
                MessageBox.Show("ERROR, Ingresar fecha termino!", "Mensaje", MessageBoxButton.OK, MessageBoxImage.Error);
                this.limpiarTodo();

            }
            else if (txtRutaDoc.Text == "")
            {
                MessageBox.Show("ERROR, Ingresar Ruta Documento", "Mensaje", MessageBoxButton.OK, MessageBoxImage.Error);
                this.limpiarTodo();

            }
            else if (cmbVigenteContrato.SelectedItem == null)
            {
                MessageBox.Show("ERROR, Ingresar Vigencia", "Mensaje", MessageBoxButton.OK, MessageBoxImage.Error);

                this.limpiarTodo();
            }
            else if (dtInicio.SelectedDate < DateTime.Now.Date)
            {
                MessageBox.Show("ERROR, La fecha de inicio no puede ser pasada a la actual", "Mensaje", MessageBoxButton.OK, MessageBoxImage.Error);
                this.limpiarTodo();
            }
            try
            {

                string sql = "INSERT INTO T_CONTRATO(IDEMPRESA,FECHAINICIO,FECHATERMINO,RUTADOCUMENTO,VIGENTE)" +
               "VALUES(:IDEMPRESA,:FECHAINICIO,:FECHATERMINO,:RUTADOCUMENTO,:VIGENTE)";

                OracleCommand cmd = new OracleCommand("CREAR_CONTRATO", Conexion);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("nuevo_idcontrato", OracleType.Number).Value = 0;
                cmd.Parameters.Add("nuevo_idempresa", OracleType.Number).Value = Int32.Parse(cmbIdEmpresa.Text).ToString();
                cmd.Parameters.Add("nuevo_fechainicio", OracleType.DateTime).Value = dtInicio.SelectedDate;
                cmd.Parameters.Add("nuevo_fechatermino", OracleType.DateTime).Value = dtTermino.SelectedDate;
                cmd.Parameters.Add("nuevo_rutadocumento", OracleType.VarChar).Value = txtRutaDoc.Text;
                cmd.Parameters.Add("nuevo_vigente", OracleType.Char).Value = cmbVigenteContrato.SelectedIndex;
                cmd.ExecuteNonQuery();
                MessageBox.Show("Contrato Ingresado", "Mensaje", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                btnAgregar.IsEnabled = false;
                btnElminar.IsEnabled = true;
                btnModificar.IsEnabled = true;
            }
            catch (Exception)
            {


            }

            Conexion.Close();
            this.limpiarTodo();
            this.subirDataGrid();
        }

        private void BtnElminar_Click(object sender, RoutedEventArgs e)
        {
            Conexion.Open();
            OracleCommand cmd = new OracleCommand("eliminarContrato", Conexion);





            MessageBoxResult mg = System.Windows.MessageBox.Show("¿Estas seguro de eliminar el contrato?", "Eliminar", System.Windows.MessageBoxButton.YesNo);
            if (mg == MessageBoxResult.Yes)
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("idcon", OracleType.Number).Value = Int32.Parse(txtIdContrato.Text);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Contrato ha sido borrado", "Mensaje", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                Conexion.Close();
            }
            else
            {
                Conexion.Close();
            }
            this.subirDataGrid();
            this.limpiarTodo();




        }

        private void limpiarTodo()
        {
            cmbIdEmpresa.SelectedIndex = 0;
            dtInicio.SelectedDate = null;
            txtIdContrato.Text = "";
            dtTermino.SelectedDate = null;
            txtRutaDoc.Text = "";
            cmbVigenteContrato.SelectedIndex = 0;
            btnAgregar.IsEnabled = true;
            btnElminar.IsEnabled = false;
            btnModificar.IsEnabled = false;
        }

        private void BtnModificar_Click(object sender, RoutedEventArgs e)
        {
            String sql = "UPDATE T_CONTRATO SET IDEMPRESA =: IDEMPRESA," +
                  "FECHAINICIO =: FECHAINICIO, FECHATERMINO =: FECHATERMINO,RUTADOCUMENTO:=RUTADOCUMENTO,VIGENTE:=VIGENTE" +
                  "WHERE IDCONTRATO:=IDCONTRATO";
            Conexion.Open();
            OracleCommand cmd = new OracleCommand("ACTUALIZAR_CONTRATO", Conexion);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add("nuevo_idcontrato", OracleType.Number).Value = int.Parse(txtIdContrato.Text).ToString();
            cmd.Parameters.Add("nuevo_idempresa", OracleType.Number).Value = Int32.Parse(cmbIdEmpresa.Text).ToString();
            cmd.Parameters.Add("nuevo_fechainicio", OracleType.DateTime).Value = dtInicio.SelectedDate;
            cmd.Parameters.Add("nuevo_fechatermino", OracleType.DateTime).Value = dtTermino.SelectedDate;
            cmd.Parameters.Add("nuevo_rutadocumento", OracleType.VarChar).Value = txtRutaDoc.Text;
            cmd.Parameters.Add("nuevo_vigente", OracleType.Char).Value = cmbVigenteContrato.SelectedIndex;
            cmd.ExecuteNonQuery();
            Conexion.Close();
            MessageBox.Show("Contrato ha sido modificado", "Mensaje", MessageBoxButton.OK, MessageBoxImage.Asterisk);
            this.subirDataGrid();
            this.limpiarTodo();

        }

        private void Midatagrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGrid dg = sender as DataGrid;
            DataRowView dr = dg.SelectedItem as DataRowView;
            if (dr != null)
            {
                txtIdContrato.Text = dr["IDCONTRATO"].ToString();
                cmbIdEmpresa.Text = dr["IDEMPRESA"].ToString();
                dtInicio.SelectedDate = DateTime.Parse(dr["FECHAINICIO"].ToString());
                dtTermino.SelectedDate = DateTime.Parse(dr["FECHATERMINO"].ToString());
                txtRutaDoc.Text = dr["RUTADOCUMENTO"].ToString();
                cmbVigenteContrato.SelectedItem = dr["VIGENTE"];
                btnAgregar.IsEnabled = false;
                btnElminar.IsEnabled = true;
                btnModificar.IsEnabled = true;

            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.limpiarTodo();
        }

        private void BtnCargar_Click(object sender, RoutedEventArgs e)
        {
            Conexion.Open();
            OracleCommand cmd = new OracleCommand("listarContratos", Conexion);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add("reg", OracleType.Cursor).Direction = ParameterDirection.Output;

            OracleDataAdapter adp = new OracleDataAdapter();
            adp.SelectCommand = cmd;
            DataTable tabla = new DataTable();
            adp.Fill(tabla);
            midatagrid.ItemsSource = tabla.DefaultView;
            Conexion.Close();
        }

        private void TxtVigencia_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int ascci = Convert.ToInt32(Convert.ToChar(e.Text));

            if (ascci >= 65 && ascci <= 90 || ascci >= 97 && ascci <= 122)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
                MessageBox.Show("Ingresar solo letras!", "Mensaje", MessageBoxButton.OK, MessageBoxImage.Error);
            }


        }

        //dashboard consultor

        public void consultorPieChart()
        {
            CD_DashBoards conex = new CD_DashBoards();
            double pedidosAnulados = conex.TotalAnulados();
            double pedidosEntregados = conex.TotalEntregados();
            double pedidosCompletados = conex.TotalCompletados();


            PointLabel = chartPoint => string.Format("{0}{1:P}", chartPoint.Y, chartPoint.Participation);

            DA_Consultor_PedidoPerdidos.Series.Add(new PieSeries { Title = "Pedidos Anulados", Fill = Brushes.Blue, StrokeThickness = 0, Values = new ChartValues<double> { pedidosAnulados } });
            DA_Consultor_PedidoPerdidos.Series.Add(new PieSeries { Title = "Pedidos Entregados", Fill = Brushes.Tomato, StrokeThickness = 0, Values = new ChartValues<double> { pedidosEntregados } });
            DA_Consultor_PedidoPerdidos.Series.Add(new PieSeries { Title = "Pedidos Completados", Fill = Brushes.SkyBlue, StrokeThickness = 0, Values = new ChartValues<double> { pedidosCompletados } });

            DataContext = this;
        }


        #endregion //________________________________________________________________

        #region CerrarPrograma //____________________________________________________
        private void BtnCloseSesion_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// Cierra todo el programa.
        /// </summary>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Application.Current.Shutdown();
        }

        #endregion //_________________________________________________________________

        #region Validar Ofertas de transporte - Maximiliano González

        private void BtnValidarOferta_Click(object sender, RoutedEventArgs e)
        {
            DataRowView sub = gridSubastas.SelectedItem as DataRowView;
            if (sub == null)
            {
                MessageBox.Show("No se ha seleccionado la oferta");
                return;
            }
            vo = new VerOfertas(this, sub);
            vo.Show();
            this.Hide();

        }

        private void btnRefrescarOfertas_Click(object sender, RoutedEventArgs e)
        {
            gridSubastas.ItemsSource = sub.MostrarSubastaAdespacho().DefaultView;
            gridSubastas.Items.Refresh();
        }

        #endregion

        #region Validar Lotes Ofrecidos - Maximiliano González

        private void BtnRefrescarPedidosRq_Click(object sender, RoutedEventArgs e)
        {
            gridValidarLotePedidos.ItemsSource = CN_Pedido.Instance.MostrarPedidoReqNegociacion().DefaultView;
            gridValidarLotePedidos.Items.Refresh();
        }

        private void BtnValidarLotePedido_Click(object sender, RoutedEventArgs e)
        {
            DataRowView glp = gridValidarLotePedidos.SelectedItem as DataRowView;
            if (glp == null)
            {
                MessageBox.Show("No se ha seleccionado ningún Pedido");
                return;
            }

            plp = new VerLotesProductoPedido(this, glp);
            plp.Show();
            this.Hide();
        }


        #endregion

        #region CRUD USUARIOS-REPRESENTANTE - Juan Román
        //autor Region Crud USUARIOS- REPRESENTANTE Juan Román
        private void BtnRefrescarusuario_ClickUsu(object sender, RoutedEventArgs e)
        {
            /*Botón refrescar que llama al método que ejecutaría la renovación de los datos*/
            btnRefrescarUsuario();
        }

        void btnRefrescarUsuario()
        {
            /*Cambia el estado de botones acorde al momento, recarga y refresca nuevamente el datagrid y llama el método LimpiarTxtRep()*/
            btnagregarUsu.IsEnabled = true;
            btnmodificarUsu.IsEnabled = false;
            btneliminarUsu.IsEnabled = false;
            gridusuariosUsu.ItemsSource = CN_Usuario.Instance.MostrarUsuario().DefaultView;
            gridusuariosUsu.Items.Refresh();
            LimpiarTxtRep();
        }

        private void Btnagregar_ClickUsu(object sender, RoutedEventArgs e)
        {
            /*Valida los campos ingresados, cambia el estado de botones, ingresa al procedimientos almacenados en la base de datos para ingresar un usuario y representante además de tomar el id del representante y refresca los campos*/
            if (txtcorreousuarioUsu.Text.Trim() == "")
            {
                MessageBox.Show("Ingrese Correo", "Error Dato Faltante", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                txtcorreousuarioUsu.Focus();
                return;
            }
            if (!MaipoGrandeNegocio.Validation.IsValidEmail(txtcorreousuarioUsu.Text))
            {
                MessageBox.Show("Formato de Email Incorrecto", "Error En dato Ingresado", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                txtcorreousuarioUsu.Focus();
                return;
            }

            if (passcontra1Usu.Password.Trim() == "")
            {
                MessageBox.Show("Especifique Contraseña", "Error Dato Faltante", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                passcontra1Usu.Focus();
                return;
            }
            if (passcontra2Usu.Password.Trim() == "")
            {
                MessageBox.Show("Ingrese Contraseña Nuevamente", "Error Dato Faltante", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                passcontra2Usu.Focus();
                return;
            }
            if (passcontra1Usu.Password != passcontra2Usu.Password)
            {
                MessageBox.Show("Contraseña no Coinciden", "Error En dato Ingresado", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                passcontra1Usu.Focus();
                return;
            }
            if (txtrutUsu.Text.Trim() == "")
            {
                MessageBox.Show("Especifique Rut", "Error Dato Faltante", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                txtrutUsu.Focus();
                return;
            }
            if (!MaipoGrandeNegocio.Validation.ValidaRut(txtrutUsu.Text))
            {
                MessageBox.Show("Rut Invalido", "Error En dato Ingresado", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                txtrutUsu.Focus();
                return;
            }
            if (txtnombreUsu.Text.Trim() == "")
            {
                MessageBox.Show("Especifique Nombre", "Error Dato Faltante", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                txtnombreUsu.Focus();
                return;
            }
            if (txtAPaternoUsu.Text.Trim() == "")
            {
                MessageBox.Show("Especifique Apellido Paterno", "Error Dato Faltante", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                txtAPaternoUsu.Focus();
                return;
            }

            if (cmbsexoUsu.SelectedIndex == 0)
            {
                MessageBox.Show("Especifique Sexo", "Error Dato Faltante", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (txtfono1Usu.Text.Trim() == "")
            {
                MessageBox.Show("Especifique Fono 1", "Error Dato Faltante", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                txtfono1Usu.Focus();
                return;
            }

            if (txtDireccionRep.Text.Trim() == "")
            {
                MessageBox.Show("Especifique Dirección", "Error Dato Faltante", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                txtfono1Usu.Focus();
                return;
            }

            if (cmbIdEmpresaUsu.SelectedIndex == 0)
            {
                MessageBox.Show("Especifique Empresa", "Error Dato Faltante", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (cmbRolUsu.SelectedIndex == 0)
            {
                MessageBox.Show("Especifique Rol", "Error Dato Faltante", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (datepFechaNacimientoUsu.SelectedDate.ToString() == "")
            {
                MessageBox.Show("Especifique Fecha de Nacimiento", "Error Dato Faltante", MessageBoxButton.OK, MessageBoxImage.Exclamation);

                return;
            }
            if (datepFechaNacimientoUsu.SelectedDate >= DateTime.Now)
            {
                MessageBox.Show("Fecha de Nacimiento no es Valida", "Error En dato Ingresado", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (datepFechaNacimientoUsu.SelectedDate >= DateTime.Now.AddYears(-18))
            {
                MessageBox.Show("Tiene que ser Mayor de edad para Registrarse", "Error En dato Ingresado", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (cmbvigenteUsu.SelectedIndex == 0)
            {
                MessageBox.Show("Especifique Vigencia", "Error Dato Faltante", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }

            try
            {
                btnagregarUsu.IsEnabled = true;
                btnmodificarUsu.IsEnabled = false;
                btneliminarUsu.IsEnabled = false;
                Conexion.Open();

                OracleCommand comando = new OracleCommand("CREAR_REPRESENTANTE", Conexion);
                comando.CommandType = CommandType.StoredProcedure;

                OracleCommand comando2 = new OracleCommand("CREAR_USUARIO", Conexion);
                comando2.CommandType = CommandType.StoredProcedure;

                /*Representante - comando*/
                comando.Parameters.Add("nuevo_idrepresentante", OracleType.Number).Value = 0;
                comando.Parameters.Add("nuevo_idempresa", OracleType.Number).Value = cmbIdEmpresaUsu.Text;
                comando.Parameters.Add("nuevo_rutrepresentante", OracleType.VarChar).Value = txtrutUsu.Text;
                comando.Parameters.Add("nuevo_rolrepresentante", OracleType.VarChar).Value = cmbRolUsu.Text.Trim();
                comando.Parameters.Add("nuevo_nombre", OracleType.VarChar).Value = txtnombreUsu.Text;
                comando.Parameters.Add("nuevo_appaterno", OracleType.VarChar).Value = txtAPaternoUsu.Text;
                comando.Parameters.Add("nuevo_apmaterno", OracleType.VarChar).Value = txtAMaternoUsu.Text;
                comando.Parameters.Add("nuevo_sexo", OracleType.Char).Value = cmbsexoUsu.SelectedIndex;
                comando.Parameters.Add("nuevo_fechanacimiento", OracleType.DateTime).Value = datepFechaNacimientoUsu.SelectedDate.Value;
                comando.Parameters.Add("nuevo_fono1", OracleType.VarChar).Value = txtfono1Usu.Text;
                comando.Parameters.Add("nuevo_fono2", OracleType.VarChar).Value = txtfono2Usu.Text;
                comando.Parameters.Add("nuevo_direccion", OracleType.VarChar).Value = txtDireccionRep.Text;
                comando.Parameters.Add("nuevo_vigente", OracleType.Char).Value = cmbvigenteUsu.SelectedIndex;/*pasa a numerico*/
                comando.ExecuteNonQuery();


                OracleCommand loCmd = Conexion.CreateCommand();
                loCmd.CommandType = CommandType.Text;
                loCmd.CommandText = "select SEQ_REPRESENTANTE.currval from dual";
                long lnNextVal = Convert.ToInt64(loCmd.ExecuteScalar());

                /*Usuario - comando 2 */

                comando2.Parameters.Add("nuevo_usuario", OracleType.Number).Value = 0;
                comando2.Parameters.Add("nuevo_email", OracleType.VarChar).Value = txtcorreousuarioUsu.Text;
                comando2.Parameters.Add("nueva_contrasena", OracleType.VarChar).Value = passcontra1Usu.Password;
                comando2.Parameters.Add("nuevo_idrepresentante", OracleType.Number).Value = lnNextVal;
                comando2.Parameters.Add("nuevo_vigente", OracleType.Char).Value = cmbvigenteUsu.SelectedIndex;
                comando2.ExecuteNonQuery();

                MessageBox.Show("Usuario Creado", "", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                LimpiarTxtRep();
                btnRefrescarUsuario();



            }
            catch (Exception r)
            {
                MessageBox.Show(r.Message);
            }
            Conexion.Close();

        }

        private void Btneliminar_ClickUsu(object sender, RoutedEventArgs e)
        {
            /*botón que cumpliría la funcionalidad de eliminar usuario y su respectivo representante,  envía un messagebox para confirmar, ingresa al procedimiento para eliminar los datos y refresca los campos*/
            MessageBoxResult messageBoxResult = MessageBox.Show("Usted está por eliminar un Usuario ¿Desea continuar?", "ATENCIÓN!", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (messageBoxResult == MessageBoxResult.No)
            {
                return;
            }
            try
            {
                if (!CN_Representante.Instance.EliminarRepresentante(
                    int.Parse(lbidselecionadarepUsu.Content.ToString())))
                {
                    MessageBox.Show("Aún existen datos que dependen de este Representante, por lo cual no puede eliminarse.", "ERROR", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                else
                {
                    MessageBox.Show("Usuario Eliminado", "", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                }
                btnagregarUsu.IsEnabled = true;
                btnmodificarUsu.IsEnabled = false;
                btneliminarUsu.IsEnabled = false;
                LimpiarTxtRep();
                btnRefrescarUsuario();
            }
            catch (Exception r)
            {
                MessageBox.Show(r.Message);
            }
            Conexion.Close();
        }

        private void Btnmodificar_ClickUsu(object sender, RoutedEventArgs e)
        {
            /*Cumple la función de modificar, realiza sus respectivas validaciones, cambia estados de botones, ingresa al procedimiento para realizar el cambio y envía un messagebox para confirmar*/

            if (txtcorreousuarioUsu.Text.Trim() == "")
            {
                MessageBox.Show("Ingrese Correo", "Error Dato Faltante", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                txtcorreousuarioUsu.Focus();
                return;
            }
            if (!MaipoGrandeNegocio.Validation.IsValidEmail(txtcorreousuarioUsu.Text))
            {
                MessageBox.Show("Formato de Email Incorrecto", "Error En dato Ingresado", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                txtcorreousuarioUsu.Focus();
                return;
            }

            if (passcontra1Usu.Password.Trim() == "")
            {
                MessageBox.Show("Especifique Contraseña", "Error Dato Faltante", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                passcontra1Usu.Focus();
                return;
            }
            if (passcontra2Usu.Password.Trim() == "")
            {
                MessageBox.Show("Ingrese Contraseña Nuevamente", "Error Dato Faltante", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                passcontra2Usu.Focus();
                return;
            }
            if (passcontra1Usu.Password != passcontra2Usu.Password)
            {
                MessageBox.Show("Contraseña no Coinciden", "Error En dato Ingresado", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                passcontra1Usu.Focus();
                return;
            }
            if (txtrutUsu.Text.Trim() == "")
            {
                MessageBox.Show("Especifique Rut", "Error Dato Faltante", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                txtrutUsu.Focus();
                return;
            }
            if (!MaipoGrandeNegocio.Validation.ValidaRut(txtrutUsu.Text))
            {
                MessageBox.Show("Rut Invalido", "Error En dato Ingresado", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                txtrutUsu.Focus();
                return;
            }
            if (txtnombreUsu.Text.Trim() == "")
            {
                MessageBox.Show("Especifique Nombre", "Error Dato Faltante", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                txtnombreUsu.Focus();
                return;
            }
            if (txtAPaternoUsu.Text.Trim() == "")
            {
                MessageBox.Show("Especifique Apellido Paterno", "Error Dato Faltante", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                txtAPaternoUsu.Focus();
                return;
            }

            if (cmbsexoUsu.SelectedIndex == 0)
            {
                MessageBox.Show("Especifique Sexo", "Error Dato Faltante", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (txtfono1Usu.Text.Trim() == "")
            {
                MessageBox.Show("Especifique Fono 1", "Error Dato Faltante", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                txtfono1Usu.Focus();
                return;
            }

            if (txtDireccionRep.Text.Trim() == "")
            {
                MessageBox.Show("Especifique Dirección", "Error Dato Faltante", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                txtfono1Usu.Focus();
                return;
            }

            if (cmbIdEmpresaUsu.SelectedIndex == 0)
            {
                MessageBox.Show("Especifique Empresa", "Error Dato Faltante", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (cmbRolUsu.SelectedIndex == 0)
            {
                MessageBox.Show("Especifique Rol", "Error Dato Faltante", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (datepFechaNacimientoUsu.SelectedDate.ToString() == "")
            {
                MessageBox.Show("Especifique Fecha de Nacimiento", "Error Dato Faltante", MessageBoxButton.OK, MessageBoxImage.Exclamation);

                return;
            }
            if (datepFechaNacimientoUsu.SelectedDate >= DateTime.Now)
            {
                MessageBox.Show("Fecha de Nacimiento no es Valida", "Error En dato Ingresado", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (datepFechaNacimientoUsu.SelectedDate >= DateTime.Now.AddYears(-18))
            {
                MessageBox.Show("Tiene que ser Mayor de edad para Registrarse", "Error En dato Ingresado", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (cmbvigenteUsu.SelectedIndex == 0)
            {
                MessageBox.Show("Especifique Vigencia", "Error Dato Faltante", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            MessageBoxResult messageBoxResult = MessageBox.Show("¿Confirmar Cambios?", "¿Desea confirmar las modificaciones realizadas?", System.Windows.MessageBoxButton.YesNo, MessageBoxImage.Information);
            if (messageBoxResult == MessageBoxResult.No)
            {
                return;
            }
            try
            {
                btnagregarUsu.IsEnabled = true;
                btnmodificarUsu.IsEnabled = false;
                btneliminarUsu.IsEnabled = false;

                Conexion.Open();
                OracleCommand comando = new OracleCommand("ACTUALIZAR_REPRESENTANTE", Conexion);
                comando.CommandType = CommandType.StoredProcedure;

                OracleCommand comando2 = new OracleCommand("ACTUALIZAR_USUARIO", Conexion);
                comando2.CommandType = CommandType.StoredProcedure;

                comando.Parameters.Add("nuevo_idrepresentante", OracleType.Number).Value = int.Parse(lbidselecionadarepUsu.Content.ToString());
                comando.Parameters.Add("nuevo_idempresa", OracleType.Number).Value = cmbIdEmpresaUsu.Text;
                comando.Parameters.Add("nuevo_rutrepresentante", OracleType.VarChar).Value = txtrutUsu.Text;
                comando.Parameters.Add("nuevo_rolrepresentante", OracleType.VarChar).Value = cmbRolUsu.Text.Trim();
                comando.Parameters.Add("nuevo_nombre", OracleType.VarChar).Value = txtnombreUsu.Text;
                comando.Parameters.Add("nuevo_appaterno", OracleType.VarChar).Value = txtAPaternoUsu.Text;
                comando.Parameters.Add("nuevo_apmaterno", OracleType.VarChar).Value = txtAMaternoUsu.Text;
                comando.Parameters.Add("nuevo_sexo", OracleType.Char).Value = cmbsexoUsu.SelectedIndex;
                comando.Parameters.Add("nuevo_fechanacimiento", OracleType.DateTime).Value = datepFechaNacimientoUsu.SelectedDate.Value;
                comando.Parameters.Add("nuevo_fono1", OracleType.VarChar).Value = txtfono1Usu.Text;
                comando.Parameters.Add("nuevo_fono2", OracleType.VarChar).Value = txtfono2Usu.Text;
                comando.Parameters.Add("nuevo_direccion", OracleType.VarChar).Value = txtDireccionRep.Text;
                comando.Parameters.Add("nuevo_vigente", OracleType.Char).Value = cmbvigenteUsu.SelectedIndex;
                comando.ExecuteNonQuery();

                comando2.Parameters.Add("nuevo_idusuario", OracleType.Number).Value = int.Parse(lbidUsu.Content.ToString());
                comando2.Parameters.Add("nuevo_email", OracleType.VarChar).Value = txtcorreousuarioUsu.Text;
                comando2.Parameters.Add("nueva_contrasena", OracleType.VarChar).Value = passcontra1Usu.Password;
                comando2.Parameters.Add("nuevo_idrepresentante", OracleType.Number).Value = int.Parse(lbidselecionadarepUsu.Content.ToString());
                comando2.Parameters.Add("nuevo_vigente", OracleType.Char).Value = cmbvigenteUsu.SelectedIndex;
                comando2.ExecuteNonQuery();
                MessageBox.Show("Usuario Modificado", "", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                LimpiarTxtRep();
                btnRefrescarUsuario();

            }
            catch (Exception r)
            {
                MessageBox.Show(r.Message);
            }
            Conexion.Close();
        }


        public void LimpiarTxtRep()
        {
            /*método que limpiaría todos los datos en textbox, combobox, datepicker y label*/
            lbidselecionadarepUsu.Content = "0";
            lbidUsu.Content = "0";
            txtcorreousuarioUsu.Text = "";
            passcontra1Usu.Password = "";
            passcontra2Usu.Password = "";
            txtrutUsu.Text = "";
            txtnombreUsu.Text = "";
            txtAMaternoUsu.Text = "";
            txtAPaternoUsu.Text = "";
            txtfono1Usu.Text = "";
            txtfono2Usu.Text = "";
            cmbIdEmpresaUsu.SelectedIndex = 0;
            cmbRolUsu.SelectedIndex = 0;
            cmbsexoUsu.SelectedIndex = 0;
            cmbvigenteUsu.SelectedIndex = 0;
            lbidempresa.Content = "0";
            lbempresaUsu.Content = "0";
            txtDireccionEmpresa.Text = "";
            txtDireccionRep.Text = "";
            datepFechaNacimientoUsu.SelectedDate = null;


        }

        private void BtnSeleccionar_ClickUsu(object sender, RoutedEventArgs e)
        {
            /*método que selecciona un dato de datagrid y los carga en textbox, combobox, datepicker y label*/

            DataRowView d = gridusuariosUsu.SelectedItem as DataRowView;
            if (d == null)
            {
                MessageBox.Show("No se seleccionaron lineas");
            }
            else
            {
                btnagregarUsu.IsEnabled = false;
                btnmodificarUsu.IsEnabled = true;
                btneliminarUsu.IsEnabled = true;
                lbidUsu.Content = d[0];
                lbidselecionadarepUsu.Content = d[1];
                txtrutUsu.Text = d[2].ToString();
                txtnombreUsu.Text = d[3].ToString();
                txtAPaternoUsu.Text = d[4].ToString();
                txtAMaternoUsu.Text = d[5].ToString();
                txtDireccionRep.Text = d[6].ToString();
                cmbsexoUsu.SelectedIndex = int.Parse(d[10].ToString());
                datepFechaNacimientoUsu.Text = d[7].ToString();
                txtfono1Usu.Text = d[8].ToString();
                txtfono2Usu.Text = d[9].ToString();
                lbempresaUsu.Content = d[15];
                cmbRolUsu.Text = d[11].ToString();
                txtcorreousuarioUsu.Text = d[12].ToString();
                passcontra1Usu.Password = d[13].ToString();
                passcontra2Usu.Password = d[13].ToString();
                cmbIdEmpresaUsu.Text = d[14].ToString();
                cmbvigenteUsu.SelectedIndex = int.Parse(d[16].ToString());
            }
        }
        #endregion

        #region ListarVehiculo - productos - ofertas - Juan Román
        //autor ListarVehiculo - productos - ofertas Juan Román

        /*----------------Vehículos------------------------------*/
        /*método para seleccionar vehículo  y guardar id en label*/
        private void BtnseleccionarVehiculo_Click(object sender, RoutedEventArgs e)
        {
            DataRowView d = dataVehi.SelectedItem as DataRowView;
            if (d == null)
            {
                MessageBox.Show("No se seleccionaron lineas");
            }
            else
            {
                btnEliminarVehi.IsEnabled = true;
                lblvehi.Content = d[2].ToString();
            }
        }

        /*método para eliminar tomando el id guardado en el label*/
        private void BtnEliminarVehi_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Usted está por eliminar un Vehículo registrado por un usuario ¿Desea continuar?", "ATENCIÓN!", System.Windows.MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (messageBoxResult == MessageBoxResult.No)
            {
                return;
            }
            try
            {
                if (!CN_Vehiculo.Instance.EliminarVehiculo(
                   int.Parse(lblvehi.Content.ToString())))
                {
                    MessageBox.Show("Aún existen datos que dependen de este Vehículo, por lo cual no puede eliminarse.", "ERROR", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                else
                {
                    MessageBox.Show("Vehículo Eliminado", "", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                }
                LimpiarLbvehi();
            }
            catch (Exception r)
            {
                MessageBox.Show(r.Message);
            }
            Conexion.Close();
        }

        /*Refresca el datagrid y label*/
        private void BtnRefrescarVehi_Click(object sender, RoutedEventArgs e)
        {
            btnEliminarVehi.IsEnabled = false;
            dataVehi.ItemsSource = CN_Vehiculo.Instance.MostrarVehiculos().DefaultView;
            dataVehi.Items.Refresh();
            LimpiarLbvehi();
        }
        /*Cambia label a 0*/
        public void LimpiarLbvehi()
        {
            lblvehi.Content = "0";
            btnEliminarVehi.IsEnabled = false;
        }

        /*----------------Ofertas------------------------------*/
        /*método para seleccionar Ofertas y guardar id en label*/
        private void BtnseleccionarOferta_Click(object sender, RoutedEventArgs e)
        {
            DataRowView d = dataOferta.SelectedItem as DataRowView;
            if (d == null)
            {
                MessageBox.Show("No se seleccionaron lineas");
            }
            else
            {
                btnEliminarOferta.IsEnabled = true;
                lblOferta.Content = d[0].ToString();
            }
        }
        /*método para eliminar tomando el id guardado en el label*/
        private void BtnEliminarOferta_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Usted está por eliminar una Oferta de Transporte registrado por un usuario ¿Desea continuar?", "ATENCIÓN!", System.Windows.MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (messageBoxResult == MessageBoxResult.No)
            {
                return;
            }
            try
            {
                Conexion.Open();
                OracleCommand comando = new OracleCommand("ELIMINAR_OFERTA_TRANSPORTE", Conexion);
                comando.CommandType = System.Data.CommandType.StoredProcedure;
                comando.Parameters.Add("ID_OFERTA_TRANSPORTE", OracleType.Number).Value = int.Parse(lblOferta.Content.ToString());
                comando.ExecuteNonQuery();
                MessageBox.Show("Oferta Eliminado", "", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                LimpiarLbOferta();
            }
            catch (Exception r)
            {
                MessageBox.Show(r.Message);

            }
            Conexion.Close();
        }
        /*Refresca el datagrid y label*/
        private void BtnRefrescarOferta_Click(object sender, RoutedEventArgs e)
        {
            btnEliminarOferta.IsEnabled = false;
            dataOferta.ItemsSource = CN_Oferta_Transporte.Instance.MostrarOfertatra().DefaultView;
            dataOferta.Items.Refresh();
            LimpiarLbOferta();
        }

        /*Cambia label a 0*/
        public void LimpiarLbOferta()
        {
            lblOferta.Content = "0";
            btnEliminarOferta.IsEnabled = false;
        }

        /*----------------Productos------------------------------*/
        /*método para seleccionar Productos y guardar id en label*/
        private void BtnseleccionarProductos_Click(object sender, RoutedEventArgs e)
        {
            DataRowView d = dataProductos.SelectedItem as DataRowView;
            if (d == null)
            {
                MessageBox.Show("No se seleccionaron lineas");
            }
            else
            {
                btnEliminarProductos.IsEnabled = true;
                lblProductos.Content = d[0].ToString();
            }
        }
        /*método para eliminar tomando el id guardado en el label*/
        private void BtnEliminarProductos_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Usted está por eliminar un Producto registrado por un usuario ¿Desea continuar?", "ATENCIÓN!", System.Windows.MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (messageBoxResult == MessageBoxResult.No)
            {
                return;
            }
            try
            {
                if (!CN_Producto.Instance.EliminarProducto(
                   int.Parse(lblProductos.Content.ToString())))
                {
                    MessageBox.Show("Aún existen datos que dependen de este Producto, por lo cual no puede eliminarse.", "ERROR", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                else
                {
                    MessageBox.Show("Producto Eliminado", "", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                }

                LimpiarLbProductos();


            }
            catch (Exception r)
            {
                MessageBox.Show(r.Message);

            }
            Conexion.Close();
        }
        /*Refresca el datagrid y label*/
        private void BtnRefrescarProductos_Click(object sender, RoutedEventArgs e)
        {
            btnEliminarProductos.IsEnabled = false;
            dataProductos.ItemsSource = CN_Producto.Instance.MostrarProduct().DefaultView;
            dataProductos.Items.Refresh();
            LimpiarLbProductos();
        }
        /*Cambia label a 0*/
        public void LimpiarLbProductos()
        {
            lblProductos.Content = "0";
            btnEliminarProductos.IsEnabled = false;
        }

        #endregion

        #region CRUD SUBASTAS
        private void BtnIngresarSubasta_Click(object sender, RoutedEventArgs e)
        {
            if (txtNombreSubasta.Text.Trim() == "")
            {
                return;
                MessageBox.Show("Especifique Nombre de Subasta.");
            }

            if (cbIdPedido.SelectedIndex == null)
            {
                MessageBox.Show("Especifique ID Pedido.");
                return;
            }

            if (cbIdRepresentante.SelectedIndex == null)
            {
                MessageBox.Show("Especifique ID Representante.");
                return;
            }

            if (dtFechaPublicacion.SelectedDate == null)
            {
                MessageBox.Show("Especifique Una Fecha de Publicación.");
                return;
            }

            if (dtFechaTermino.SelectedDate == null)
            {
                MessageBox.Show("Especifique Una Fecha de Termino.");
                return;
            }

            if (dtFechaPublicacion.SelectedDate == dtFechaTermino.SelectedDate)

            {
                MessageBox.Show("La fecha de publicación y termino no pueden ser el mismo dia.");
                return;
            }


            if (dtFechaPublicacion.SelectedDate < DateTime.Now.Date)
            {
                MessageBox.Show("La fecha de publicación no puede ser menor a la fecha actual.");
                return;
            }

            try
            {
                Conexion.Open();
                OracleCommand comando = new OracleCommand("CREAR_SUBASTA", Conexion);
                comando.CommandType = System.Data.CommandType.StoredProcedure;

                comando.Parameters.Add("nuevo_idsubasta", OracleType.Number).Value = 0;
                comando.Parameters.Add("nuevo_nombresubasta", OracleType.VarChar).Value = txtNombreSubasta.Text;
                comando.Parameters.Add("nuevo_idpedido", OracleType.Number).Value = cbIdPedido.Text;
                comando.Parameters.Add("nuevo_fechapublicacion", OracleType.DateTime).Value = dtFechaPublicacion.SelectedDate.Value;
                comando.Parameters.Add("nuevo_fechatermino", OracleType.DateTime).Value = dtFechaTermino.SelectedDate.Value;
                comando.Parameters.Add("nuevo_idrepresentante", OracleType.Number).Value = cbIdRepresentante.Text;
                comando.ExecuteNonQuery();
                MessageBox.Show("Subasta Ingresada Correctamente.");
            }
            catch (Exception r)
            {
                MessageBox.Show(r.Message);
                MessageBox.Show("la Subasta no fue ingresada Correctamente.");
            }
            Conexion.Close();
        }



        private void BtnEliminarSubasta_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("¿ Desea Continuar con la Eliminacion ?.", "¡Atencion!", System.Windows.MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (messageBoxResult == MessageBoxResult.No)
            {
                return;
            }

            try
            {
                Conexion.Open();
                OracleCommand comando = new OracleCommand("ELIMINAR_SUBASTA", Conexion);
                comando.CommandType = System.Data.CommandType.StoredProcedure;
                comando.Parameters.Add("ID_SUBASTA", OracleType.Number).Value = int.Parse(lblIdSubasta.Content.ToString());
                comando.ExecuteNonQuery();
                MessageBox.Show("Subasta Eliminada Correctamente.");
            }

            catch (Exception r)
            {
                MessageBox.Show(r.Message);
                MessageBox.Show("La Subasta no fue eliminada.");
            }
            Conexion.Close();
        }

        private void BtnActualizarSubasta_Click(object sender, RoutedEventArgs e)
        {
            if (txtNombreSubasta.Text.Trim() == "")
            {
                return;
                MessageBox.Show("Especifique Nombre de Subasta.");
            }

            if (cbIdPedido.SelectedIndex == null)
            {
                MessageBox.Show("Especifique ID Pedido.");
                return;
            }

            if (cbIdRepresentante.SelectedIndex == null)
            {
                MessageBox.Show("Especifique ID Representante.");
                return;
            }

            if (dtFechaPublicacion.SelectedDate == null)
            {
                MessageBox.Show("Especifique Una Fecha de Publicación.");
                return;
            }

            if (dtFechaTermino.SelectedDate == null)
            {
                MessageBox.Show("Especifique Una Fecha de Termino.");
                return;
            }

            if (dtFechaPublicacion.SelectedDate == dtFechaTermino.SelectedDate)

            {
                MessageBox.Show("La fecha de publicación y termino no pueden ser el mismo dia.");
                return;
            }


            if (dtFechaPublicacion.SelectedDate < DateTime.Now.Date)
            {
                MessageBox.Show("La fecha de publicación no puede ser menor a la fecha actual.");
                return;
            }


            try
            {
                Conexion.Open();
                OracleCommand comando = new OracleCommand("ACTUALIZAR_SUBASTA", Conexion);
                comando.CommandType = System.Data.CommandType.StoredProcedure;
                comando.Parameters.Add("nuevo_idsubasta", OracleType.Number).Value = int.Parse(lblIdSubasta.Content.ToString());
                comando.Parameters.Add("nuevo_nombresubasta", OracleType.VarChar).Value = txtNombreSubasta.Text;
                comando.Parameters.Add("nuevo_idpedido", OracleType.Number).Value = int.Parse(cbIdPedido.Text);
                comando.Parameters.Add("nuevo_fechapublicacion", OracleType.DateTime).Value = dtFechaPublicacion.SelectedDate.Value;
                comando.Parameters.Add("nuevo_fechatermino", OracleType.DateTime).Value = dtFechaTermino.SelectedDate.Value;
                comando.Parameters.Add("nuevo_idrepresentante", OracleType.Number).Value = int.Parse(cbIdRepresentante.Text);
                MessageBox.Show("Subasta Modificada Correctamente.");
                comando.ExecuteNonQuery();
                LimpiarTxtSubasta();
            }
            catch (Exception r)
            {
                MessageBox.Show(r.Message);
            }
            Conexion.Close();
        }

        private void BtnRefrescarSubasta_Click(object sender, RoutedEventArgs e)
        {
            gridSubasta.ItemsSource = sub.MostrarSubasta().DefaultView;
            gridSubasta.Items.Refresh();
        }

        private void BtnSeleccionarSubasta_Click(object sender, RoutedEventArgs e)
        {
            DataRowView s = gridSubasta.SelectedItem as DataRowView;
            if (s == null)
            {
                MessageBox.Show("No se seleccionaron lineas.");
            }
            else
            {
                lblIdSubasta.Content = s[0];
                cbIdRepresentante.Text = s[5].ToString();
                cbIdPedido.Text = s[2].ToString();
                txtNombreSubasta.Text = s[1].ToString();
                dtFechaPublicacion.Text = s[3].ToString();
                dtFechaTermino.Text = s[4].ToString();
            }
        }

        public void LimpiarTxtSubasta()
        {
            lblIdSubasta.Content = "0";
            cbIdPedido.SelectedIndex = 0;
            cbIdRepresentante.SelectedIndex = 0;
            txtNombreSubasta.Text = "";
            dtFechaPublicacion.SelectedDate = null;
            dtFechaTermino.SelectedDate = null;
        }

        private void BtnLimpiarSubasta_Click(object sender, RoutedEventArgs e)
        {
            LimpiarTxtSubasta();
        }
        #endregion

        #region CRUD EMPRESAS - Cristián Esquivel
        // Método de Agregar Empresas
        private void btnAgregarEmpresa_Click(object sender, RoutedEventArgs e)
        {
            // Validaciones
            if (txtRutEmpresa.Text.Trim() == "")
            {
                MessageBox.Show("Especifique Rut");
                return;
            }
            if (!MaipoGrandeNegocio.Validation.ValidaRut(txtRutEmpresa.Text))
            {
                MessageBox.Show("Rut Invalido");
                return;
            }
            if (txtTipoEmpresa.Text.Trim() == "")
            {
                MessageBox.Show("Especifique Tipo de Empresa");
                return;
            }
            if (txtDireccionEmpresa.Text.Trim() == "")
            {
                MessageBox.Show("Especifique Dirección");
                return;
            }
            if (txtCiudadEmpresa.Text.Trim() == "")
            {
                MessageBox.Show("Especifique Ciudad");
                return;
            }
            if (txtPaisEmpresa.Text.Trim() == "")
            {
                MessageBox.Show("Especifique País");
                return;
            }
            if (txtFono1Empresa.Text.Trim() == "")
            {
                MessageBox.Show("Especifique Fono 1");
                return;
            }
            if (txtDireccionEmpresa.Text.Trim() == "")
            {
                MessageBox.Show("Especifique Dirección");
                return;
            }
            if (!MaipoGrandeNegocio.Validation.IsValidEmail(txtEmailEmpresa.Text))
            {
                MessageBox.Show("Formato de Email Incorrecto");
                return;
            }
            try
            {
                // Ingresa la empresa
                CN_Empresa.Instance.InsertarEmpresa(
                    0,
                    txtRutEmpresa.Text,
                    txtTipoEmpresa.Text,
                    txtNombreComercialEmpresa.Text,
                    txtRazonSocialEmpresa.Text,
                    "",
                    txtDireccionEmpresa.Text,
                    txtCiudadEmpresa.Text,
                    txtPaisEmpresa.Text,
                    txtFono1Empresa.Text,
                    txtFono2Empresa.Text,
                    txtEmailEmpresa.Text,
                    txtWebEmpresa.Text);
                MessageBox.Show("Empresa Ingresada Correctamente");
                limpiarTxtEmpresa();
                RefrescarEmpresas();
                cargarcmbidempresa(cmbIdEmpresaUsu);
            }
            catch (Exception r)
            {
                MessageBox.Show(r.Message);
            }


        }
        // Método para modificar la empresa
        private void btnModificarEmpresa_Click(object sender, RoutedEventArgs e)
        {
            // Validaciones y mensajes de confirmación
            MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("¿Confirmar Cambios?", "¿Desea confirmar las modificaciones realizadas?", System.Windows.MessageBoxButton.YesNo, MessageBoxImage.Information);
            if (messageBoxResult == MessageBoxResult.No)
            {
                return;
            }

            if (txtRutEmpresa.Text.Trim() == "")
            {
                MessageBox.Show("Especifique Rut");
                return;
            }
            if (!MaipoGrandeNegocio.Validation.ValidaRut(txtRutEmpresa.Text))
            {
                MessageBox.Show("Rut Inválido");
                return;
            }
            if (txtTipoEmpresa.Text.Trim() == "")
            {
                MessageBox.Show("Especifique Tipo de Empresa");
                return;
            }
            if (txtDireccionEmpresa.Text.Trim() == "")
            {
                MessageBox.Show("Especifique Dirección");
                return;
            }
            if (txtCiudadEmpresa.Text.Trim() == "")
            {
                MessageBox.Show("Especifique Ciudad");
                return;
            }
            if (txtPaisEmpresa.Text.Trim() == "")
            {
                MessageBox.Show("Especifique País");
                return;
            }
            if (txtFono1Empresa.Text.Trim() == "")
            {
                MessageBox.Show("Especifique Fono 1");
                return;
            }
            if (txtDireccionEmpresa.Text.Trim() == "")
            {
                MessageBox.Show("Especifique Dirección");
                return;
            }
            if (!MaipoGrandeNegocio.Validation.IsValidEmail(txtEmailEmpresa.Text))
            {
                MessageBox.Show("Formato de Email Incorrecto");
                return;
            }
            try
            {
                if (txtFono2Empresa.Text.Trim() == "")
                {
                    txtFono2Empresa.Text = "0";
                }
                // Modifica la empresa
                CN_Empresa.Instance.ModificarEmpresa(
                    int.Parse(lblIDEmpresa.Content.ToString()),
                    txtRutEmpresa.Text,
                    txtTipoEmpresa.Text,
                    txtNombreComercialEmpresa.Text,
                    txtRazonSocialEmpresa.Text,
                    "",
                    txtDireccionEmpresa.Text,
                    txtCiudadEmpresa.Text,
                    txtPaisEmpresa.Text,
                    txtFono1Empresa.Text,
                    txtFono2Empresa.Text,
                    txtEmailEmpresa.Text,
                    txtWebEmpresa.Text);
                MessageBox.Show("Empresa Modificada Correctamente");
                // Habilita los botones necesarios
                btnAgregarEmpresa.IsEnabled = true;
                btnModificarEmpresa.IsEnabled = false;
                btnEliminarEmpresa.IsEnabled = false;
                limpiarTxtEmpresa();
                RefrescarEmpresas();
                cargarcmbidempresa(cmbIdEmpresaUsu);//para que me actualice el cmb de usuarios
                gridusuariosUsu.ItemsSource = CN_Usuario.Instance.MostrarUsuario().DefaultView;// actualizar tabla usuarios
            }
            catch (Exception r)
            {
                MessageBox.Show(r.Message);
            }
        }

        // Método para eliminar una empresa
        private void btnEliminarEmpresa_Click(object sender, RoutedEventArgs e)
        {
            // Mensaje de confirmación
            MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Usted está por eliminar una empresa, debe asegurarse de que esta no contenga ningun representante asignado, sabiendo esto ¿Desea continuar con la eliminación?", "ATENCIÓN!!", System.Windows.MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (messageBoxResult == MessageBoxResult.No)
            {
                return;
            }
            try
            {
                // Verifica si existen datos que dependen de la empresa, lo cual eso implicaría que no se puede eliminar
                if (!CN_Empresa.Instance.EliminarEmpresa(
                    int.Parse(lblIDEmpresa.Content.ToString())))
                {
                    MessageBox.Show("Aún existen datos que dependen de esta empresa, por lo cual no puede eliminarse.", "ERROR", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                else
                {
                    MessageBox.Show("Empresa Eliminada Correctamente");
                }

                btnAgregarEmpresa.IsEnabled = true;
                btnModificarEmpresa.IsEnabled = false;
                btnEliminarEmpresa.IsEnabled = false;
                limpiarTxtEmpresa();
                RefrescarEmpresas();
                cargarcmbidempresa(cmbIdEmpresaUsu);
            }
            catch (Exception r)
            {
                MessageBox.Show(r.Message);
            }
        }

        // Refresca la tabla
        private void btnRefrescarEmpresas_Click(object sender, RoutedEventArgs e)
        {
            RefrescarEmpresas();
        }

        // Método que permite extraer los datos de una linea seleccionada de la tabla para poder ya sea modificar o eliminar una empresa
        private void btnSeleccionarEmpresa_Click(object sender, RoutedEventArgs e)
        {
            DataRowView d = gridEmpresa.SelectedItem as DataRowView;
            if (d == null)
            {
                MessageBox.Show("No se seleccionaron lineas");
            }
            else
            {
                // Se extraen los datos de la linea a los textbox correspondientes
                lblIDEmpresa.Content = d[0];
                txtRutEmpresa.Text = d[1].ToString();
                txtTipoEmpresa.Text = d[2].ToString();
                txtNombreComercialEmpresa.Text = d[3].ToString();
                txtRazonSocialEmpresa.Text = d[4].ToString();
                txtDireccionEmpresa.Text = d[5].ToString();
                txtCiudadEmpresa.Text = d[6].ToString();
                txtPaisEmpresa.Text = d[7].ToString();
                txtFono1Empresa.Text = d[8].ToString();
                txtFono2Empresa.Text = d[9].ToString();
                txtEmailEmpresa.Text = d[10].ToString();
                txtWebEmpresa.Text = d[11].ToString();
                // Habilita los botones necesarios
                btnAgregarEmpresa.IsEnabled = false;
                btnModificarEmpresa.IsEnabled = true;
                btnEliminarEmpresa.IsEnabled = true;
                btnDeseleccionarEmpresa.IsEnabled = true;
            }
        }

        // Limpia los textos y botones para refrescar la pantalla
        public void limpiarTxtEmpresa()
        {
            lblIDEmpresa.Content = "0";
            txtRutEmpresa.Text = "";
            txtTipoEmpresa.Text = "";
            txtNombreComercialEmpresa.Text = "";
            txtRazonSocialEmpresa.Text = "";
            txtDireccionEmpresa.Text = "";
            txtCiudadEmpresa.Text = "";
            txtPaisEmpresa.Text = "";
            txtFono1Empresa.Text = "";
            txtFono2Empresa.Text = "";
            txtEmailEmpresa.Text = "";
            txtWebEmpresa.Text = "";
            // Habilita los botones necesarios
            btnAgregarEmpresa.IsEnabled = true;
            btnModificarEmpresa.IsEnabled = false;
            btnEliminarEmpresa.IsEnabled = false;
            btnDeseleccionarEmpresa.IsEnabled = false;
        }

        // Refresca la tabla de empresas
        void RefrescarEmpresas()
        {
            gridEmpresa.ItemsSource = CN_Empresa.Instance.MostrarEmpresa().DefaultView;
            gridEmpresa.Items.Refresh();
        }

        // Método que sirve para deseleccionar la empresa que se haya seleccionado
        private void btnDeseleccionarEmpresa_Click(object sender, RoutedEventArgs e)
        {
            limpiarTxtEmpresa();
            btnDeseleccionarEmpresa.IsEnabled = false;
        }
        #endregion

        #region PEDIDOS - Cristián Esquivel
        // Método que permite abrir una pestaña con el detalle del pedido seleccionado
        private void btnVerDetallePedido_Click(object sender, RoutedEventArgs e)
        {
            DataRowView d = gridPedidos.SelectedItem as DataRowView;
            if (d == null) // Verifica si se seleccionó o no una linea
            {
                MessageBox.Show("No se ha seleccionado pedido");
                return;
            }
            p = new VerDetallePedido(this, d);
            p.Show();
            this.Hide();
        }
        // Método que permite refrescar el listado de pedidos
        private void btnRefrescarPedidos_Click(object sender, RoutedEventArgs e)
        {
            gridPedidos.ItemsSource = CN_Pedido.Instance.MostrarPedido().DefaultView;
            gridPedidos.Items.Refresh();
        }
        #endregion

        #region VALIDACIONES
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void ValidacionRut(object sender, TextCompositionEventArgs e)//validacion que solo deja ingresar numeros, - , k y K
        {
            //autor Juan Román
            int ascci = Convert.ToInt32(Convert.ToChar(e.Text));

            if (ascci == 75 || ascci == 107 || ascci == 45 || ascci >= 48 && ascci <= 57)

                e.Handled = false;

            else e.Handled = true;
        }

        private void validacionLetras(object sender, TextCompositionEventArgs e)//Validacion que solo deja ingresar letras mayusculas y minusculas
        {//autor Juan Román
            int ascci = Convert.ToInt32(Convert.ToChar(e.Text));

            if (ascci >= 65 && ascci <= 90 || ascci >= 97 && ascci <= 122)

                e.Handled = false;

            else e.Handled = true;

        }

        private void validacionFonos(object sender, TextCompositionEventArgs e)//validacion que solo deja ingresar + y numeros
        {//autor Juan Román
            int ascci = Convert.ToInt32(Convert.ToChar(e.Text));

            if (ascci >= 48 && ascci <= 57 || ascci == 43)

                e.Handled = false;

            else e.Handled = true;

        }

        #endregion

        #region CRUD UNIDAD DE MEDICIÓN - Cristián Esquivel
        // Método de agregar unidad de medición
        private void btnAgregarUnidad_Click(object sender, RoutedEventArgs e)
        {
            // Validaciones
            if (txtDescripcionUnidad.Text.Trim() == "")
            {
                MessageBox.Show("Especifique descripción");
                return;
            }
            try
            {
                // Ingresa la unidad de medición
                CN_Unidad_Medicion.Instance.InsertarUnidadMedicion(
                    0,
                    txtDescripcionUnidad.Text);
                MessageBox.Show("Unidad de Medición Ingresada Correctamente");
                limpiarTxtUnidad();
                RefrescarUnidad();
                cargarcmbidempresa(cmbIdEmpresaUsu);
            }
            catch (Exception r)
            {
                MessageBox.Show(r.Message);
            }
        }

        // Método para modificar una unidad de medición
        private void btnModificarUnidad_Click(object sender, RoutedEventArgs e)
        {
            // Validaciones
            if (txtDescripcionUnidad.Text.Trim() == "")
            {
                MessageBox.Show("Especifique descripción");
                return;
            }
            try
            {
                // Modifica la unidad de medición
                CN_Unidad_Medicion.Instance.ModificarUnidadMedicion(
                    int.Parse(lblIDUnidad.Content.ToString()),
                    txtDescripcionUnidad.Text);
                MessageBox.Show("Unidad de Medición Modificada Correctamente");
                limpiarTxtUnidad();
                RefrescarUnidad();
                btnAgregarUnidad.IsEnabled = true;
                btnModificarUnidad.IsEnabled = false;
                btnEliminarUnidad.IsEnabled = false;
            }
            catch (Exception r)
            {
                MessageBox.Show(r.Message);
            }
        }

        // Método para eliminar una unidad de medición
        private void btnEliminarUnidad_Click(object sender, RoutedEventArgs e)
        {
            // Mensaje de confirmación
            MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Usted está por eliminar una unidad de medición, debe asegurarse de que esta no se haya utilizado en ningun pedido o lote asignado, sabiendo esto ¿Desea continuar con la eliminación?", "ATENCIÓN!!", System.Windows.MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (messageBoxResult == MessageBoxResult.No)
            {
                return;
            }
            try
            {
                // Verifica si existen datos que dependen de la unidad de medición, lo cual eso implicaría que no se puede eliminar
                if (!CN_Unidad_Medicion.Instance.EliminarUnidadMedicion(
                    int.Parse(lblIDUnidad.Content.ToString())))
                {
                    MessageBox.Show("Aún existen datos que dependen de esta unidad de medición, por lo cual no puede eliminarse.", "ERROR", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                else
                {
                    MessageBox.Show("Unidad de medición eliminada correctamente");
                }

                btnAgregarUnidad.IsEnabled = true;
                btnModificarUnidad.IsEnabled = false;
                btnEliminarUnidad.IsEnabled = false;
                limpiarTxtUnidad();
                RefrescarUnidad();
            }
            catch (Exception r)
            {
                MessageBox.Show(r.Message);
            }
        }

        public void limpiarTxtUnidad()
        {
            lblIDUnidad.Content = "0";
            txtDescripcionUnidad.Text = "";

            // Habilita los botones necesarios
            btnAgregarUnidad.IsEnabled = true;
            btnModificarUnidad.IsEnabled = false;
            btnEliminarUnidad.IsEnabled = false;
            btnDeseleccionarUnidad.IsEnabled = false;
        }

        // Refresca la tabla de unidad de medición
        void RefrescarUnidad()
        {
            gridUnidades.ItemsSource = CN_Unidad_Medicion.Instance.MostrarUnidadMedicion().DefaultView;
            gridUnidades.Items.Refresh();
        }

        // Método que sirve para deseleccionar la unidad de medición que se haya seleccionado
        private void btnDeseleccionarUnidad_Click(object sender, RoutedEventArgs e)
        {
            limpiarTxtUnidad();
            btnDeseleccionarUnidad.IsEnabled = false;
        }

        private void btnRefrescarUnidad_Click(object sender, RoutedEventArgs e)
        {
            RefrescarUnidad();
        }

        private void btnSeleccionarUnidad_Click(object sender, RoutedEventArgs e)
        {
            DataRowView d = gridUnidades.SelectedItem as DataRowView;
            if (d == null)
            {
                MessageBox.Show("No se seleccionaron lineas");
            }
            else
            {
                // Se extraen los datos de la linea a los textbox correspondientes
                lblIDUnidad.Content = d[0];
                txtDescripcionUnidad.Text = d[1].ToString();
                // Habilita los botones necesarios
                btnAgregarUnidad.IsEnabled = false;
                btnModificarUnidad.IsEnabled = true;
                btnEliminarUnidad.IsEnabled = true;
                btnDeseleccionarUnidad.IsEnabled = true;
            }
        }

        #endregion

        #region Dashboards



        //PieChart
        public Func<ChartPoint, String> PointLabel { get; set; }

        internal void RefreshData(double badPct)
        {
            //myPiechart.Series[0].Values[0] = badPct;
            //myPiechart.Series[1].Values[0] = 100.0 - badPct;
        }

        private void PieChart_DataClick(Object sender, ChartPoint chartPoint)
        {
            var chart = (LiveCharts.Wpf.PieChart)chartPoint.ChartView;
            foreach (PieSeries pieSeries in chart.Series)
                pieSeries.PushOut = 0;

            var seleccionarSeries = (PieSeries)chartPoint.SeriesView;
            seleccionarSeries.PushOut = 8;
        }

        #region Dashboard Administrador

        public void IniciarDashAdmin()
        {
            //Por siacaso
            CD_DashBoards controlador = new CD_DashBoards();
            CN_Dashboards con = new CN_Dashboards();

            //Cargar gráficos
            //this.Cartesian();
            this.DA_pie_variedades_frutas();
            this.DA_pie_variedades_frutas_x_productor();

            //Cargar tarjetas de pedidos
            DA_lbl_Pedidos_Progreso.Content = controlador.TotalPedidosEnProceso();
            DA_lbl_Pedidos_Completados.Content = controlador.TotalCompletados();
            DA_lbl_Pedidos_Anulados.Content = controlador.TotalAnulados();

            //Cargar grillas de tops representantes
            dg_top_clientes.ItemsSource = con.MostrarTopClientes().DefaultView;
            dg_top_clientes.Items.Refresh();
            dg_top_productores.ItemsSource = con.MostrarTopProductores().DefaultView;
            dg_top_productores.Items.Refresh();
            dg_top_transportistas.ItemsSource = con.MostrarTopTransportistas().DefaultView;
            dg_top_transportistas.Items.Refresh();

            //Cargar tarjetas de cantidad de representantes
            DA_lbl_Cant_ClientesExt.Content = controlador.TotalRepresentantes(2);
            DA_lbl_Cant_ClientesInt.Content = controlador.TotalRepresentantes(3);
            DA_lbl_Cant_Productores.Content = controlador.TotalRepresentantes(4);
            DA_lbl_Cant_Transportistas.Content = controlador.TotalRepresentantes(5);
        }


        public void DA_pie_variedades_frutas()
        {
            CD_DashBoards conex = new CD_DashBoards();
            CN_Dashboards con = new CN_Dashboards();

            PointLabel = chartPoint => string.Format("{0}{1:P}", chartPoint.Y, chartPoint.Participation);

            foreach (string[] variedad in con.ConteoVariedadesFrutas())
            {
                DA_pie_variedadesFrutas.Series.Add(new PieSeries { Title = variedad[0], StrokeThickness = 0, Values = new ChartValues<double> { Convert.ToDouble(variedad[1]) } });
            }
            DataContext = this;
        }
        public void DA_pie_variedades_frutas_x_productor()
        {
            CD_DashBoards conex = new CD_DashBoards();
            CN_Dashboards con = new CN_Dashboards();

            PointLabel = chartPoint => string.Format("{0}{1:P}", chartPoint.Y, chartPoint.Participation);

            foreach (string[] variedad in conex.ContarVariedadesFrutasXProductor())
            {
                DA_pie_variedadesFrutas_x_Productor.Series.Add(new PieSeries { Title = variedad[0], StrokeThickness = 0, Values = new ChartValues<double> { Convert.ToDouble(variedad[1]) } });
            }
            DataContext = this;
        }

        //Cartesian
        public void Cartesian()
        {
            SeriesCollection = new SeriesCollection
            {
                new LineSeries
                {
                    Title = "Series 1",
                    Values = new ChartValues<double> { 4, 6, 5, 2 ,4 }
                },
                new LineSeries
                {
                    Title = "Series 2",
                    Values = new ChartValues<double> { 6, 7, 3, 4 ,6 },
                    PointGeometry = null
                },
                new LineSeries
                {
                    Title = "Series 3",
                    Values = new ChartValues<double> { 4,2,7,2,7 },
                    PointGeometry = DefaultGeometries.Square,
                    PointGeometrySize = 15
                }
            };

            Labels = new[] { "Jan", "Feb", "Mar", "Apr", "May" };
            YFormatter = value => value.ToString("C");

            //modifying the series collection will animate and update the chart
            SeriesCollection.Add(new LineSeries
            {
                Title = "Series 4",
                Values = new ChartValues<double> { 5, 3, 2, 4 },
                LineSmoothness = 0, //0: straight lines, 1: really smooth lines
                PointGeometry = Geometry.Parse("m 25 70.36218 20 -28 -20 22 -8 -6 z"),
                PointGeometrySize = 50,
                PointForeground = Brushes.Gray
            });

            //modifying any series values will also animate and update the chart
            SeriesCollection[3].Values.Add(5d);

            DataContext = this;
        }

        public SeriesCollection SeriesCollection { get; set; }
        public string[] Labels { get; set; }
        public Func<double, string> YFormatter { get; set; }


        #endregion Dashboard Administrador

        #region DashBoard Consultor
        internal class PerdidaViewModel
        {
            public List<Perdida> Perdida { get; private set; }

            public PerdidaViewModel(Perdida perdida)
            {
                Perdida = new List<Perdida>();
                Perdida.Add(perdida);
            }
        }

        protected int Calcular()
        {

            return 4;
        }


        internal class Perdida
        {
            public string Titulo { get; private set; }
            public int Porcentaje { get; private set; }

            public Perdida()
            {
                Titulo = "Ventas Perdidas";
                Porcentaje = CalcularPorcentagem();
            }



            private int CalcularPorcentagem()
            {
                return 97; //Calculo da porcentagem de consumo
            }
        }
        #endregion DashBoard Consultor

        #endregion Dashboards

        #region REPORTES Consultor - Maximiliano González

        

        private void BtnMostrarInforme_Click(object sender, RoutedEventArgs e)
        {

            if (combolistaReportes.SelectedIndex == 0 )
            {
                MessageBox.Show("Seleccione informe", "Error Dato Faltante", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (combolistaReportes.SelectedIndex == 1)
            {
                ReportViewerDemo.Reset();
                lblOpcReport.Visibility = Visibility.Visible;
                String cal = "";
                switch (comboEstadoPedido.SelectedIndex)
                {
                    
                    case 1: cal = "1"; break;
                    case 2: cal = "2"; break;
                    case 3: cal = "3"; break;
                    case 4: cal = "4"; break;
                    case 5: cal = "5"; break;
                    case 6: cal = "6"; break;
                    case 7: cal = "7"; break;
                }
               
                ds = new ReportDataSource("DataSet1", rp.MostrarLotesByCalidad(cal));
                ReportViewerDemo.LocalReport.DataSources.Add(ds);

                ReportViewerDemo.LocalReport.ReportEmbeddedResource = "MaipoGrandeVista.Reportes.ReporteLotesCalidad.rdlc";
                ReportViewerDemo.RefreshReport();

            }
            if (combolistaReportes.SelectedIndex == 2)
            {
                
                
                ReportViewerDemo.Reset();
                String txt = "";
                switch (comboEstadoPedido.SelectedIndex)
                {
                    
                    case 1: txt= "Requiere Validación"; break; 
                    case 2: txt = "Requiere Negociación"; break;
                    case 3: txt = "Subastado"; break;
                    case 4: txt = "En Recorrido"; break;
                    case 5: txt = "Entregado"; break;
                    case 6: txt = "Completado"; break;
                    case 7: txt = "Anulado"; break;
                }
                               
                ds = new ReportDataSource("DataSet2", rp.MostrarPedidoByEstado(txt));
                ReportViewerDemo.LocalReport.DataSources.Add(ds);

                ReportViewerDemo.LocalReport.ReportEmbeddedResource = "MaipoGrandeVista.Reportes.ReportePedidosPorEstado.rdlc";
                ReportViewerDemo.RefreshReport();                
                
            }
            if (combolistaReportes.SelectedIndex == 3)
            {
                
                
                ReportViewerDemo.Reset();
                ds = new ReportDataSource("DataSet3", rp.MostrarOfertaTransporte());
                ReportViewerDemo.LocalReport.DataSources.Add(ds);

                ReportViewerDemo.LocalReport.ReportEmbeddedResource = "MaipoGrandeVista.Reportes.ReporteOfertasTransporte.rdlc";
                ReportViewerDemo.RefreshReport();
            }
            if (combolistaReportes.SelectedIndex == 4)
            {

                ReportViewerDemo.Reset();
                String txt = "";
                switch (comboSubastaEstado.SelectedIndex)
                {
                    case 1: txt = "Requiere Negociación"; break;
                    case 2: txt = "Requiere Validación"; break;
                    case 3: txt = "Subastado"; break;
                }
                ds = new ReportDataSource("DataSet4", rp.MostrarSubastaByEstado(txt));
                ReportViewerDemo.LocalReport.DataSources.Add(ds);

                ReportViewerDemo.LocalReport.ReportEmbeddedResource = "MaipoGrandeVista.Reportes.ReporteSubastas.rdlc";
                ReportViewerDemo.RefreshReport();
            }

        }


        private void CombolistaReportes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {           
            if (combolistaReportes.SelectedIndex == 2)
            { comboEstadoPedido.Visibility = Visibility.Visible; comboLoteCalidad.Visibility = Visibility.Hidden;  comboSubastaEstado.Visibility = Visibility.Hidden; lblOpcReport.Visibility = Visibility.Visible;  }
            if (combolistaReportes.SelectedIndex == 1 )
            { comboLoteCalidad.Visibility = Visibility.Visible; comboEstadoPedido.Visibility = Visibility.Hidden; comboSubastaEstado.Visibility = Visibility.Hidden; lblOpcReport.Visibility = Visibility.Visible;  }
            if (combolistaReportes.SelectedIndex == 3)
            { comboLoteCalidad.Visibility = Visibility.Hidden; comboEstadoPedido.Visibility = Visibility.Hidden; comboSubastaEstado.Visibility = Visibility.Hidden; lblOpcReport.Visibility = Visibility.Hidden;  }
            if (combolistaReportes.SelectedIndex == 4)
            { comboSubastaEstado.Visibility = Visibility.Visible; comboLoteCalidad.Visibility = Visibility.Hidden; comboEstadoPedido.Visibility = Visibility.Hidden; lblOpcReport.Visibility = Visibility.Visible; }
        }


        #endregion


    }
}
