﻿using System;
using System.Collections.Generic;
using System.Data.OracleClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MaipoGrandeDatos;
using MaipoGrandeNegocio;

namespace MaipoGrandeVista
{
    /// <summary>
    /// Lógica de interacción para Login.xaml
    /// </summary>
    public partial class Login : Window
    {

        OracleConnection Conexion = new OracleConnection(ConexionBD.conex);

        public Login()
        {
            InitializeComponent();
            lblMessage.Visibility = Visibility.Collapsed;
            btnLostPass.Visibility = Visibility.Collapsed;
        }

        private void BtnLogin_Click(object sender, RoutedEventArgs e)
        {
            Conexion.Open();
            string sentenciaLogin = "select rolRepresentante, nombre from t_representante where vigente = 1 and IDREPRESENTANTE = (select IDREPRESENTANTE from t_usuario where email = :email and contrasena = :pass and vigente = 1)";
            //OracleCommand comando = new OracleCommand("Select * from t_usuario where email = :email and contrasena = :pass",conexion);
            OracleCommand comando = new OracleCommand(sentenciaLogin, Conexion);

            comando.Parameters.AddWithValue(":email", txtMail.Text);
            comando.Parameters.AddWithValue(":pass", txtPass.Password.ToString());

            OracleDataReader lector = comando.ExecuteReader();

            if (lector.Read()) //Si puede leer, hay un dato; y si hay un dato, existe la persona.
            {
                //Debería devolver Administrador o Consultor
                Session.TipoUsuario = lector.GetOracleString(0).ToString();
                //Devuelve el primer y segundo nombre del usuario
                Session.NombreUsuario = lector.GetOracleString(1).ToString();

                if (Session.TipoUsuario.Equals("Administrador"))
                {
                    MainWindow ventanaPrincipal = new MainWindow();
                    Conexion.Close();
                    ventanaPrincipal.Show();
                    this.Hide();
                }
                else if (Session.TipoUsuario.Equals("Consultor"))
                {
                    MainWindow ventanaPrincipal = new MainWindow();
                    Conexion.Close();
                    ventanaPrincipal.Show();
                    this.Hide();
                }
                lblMessage.Visibility = Visibility.Visible;
                Conexion.Close();
            }
            else
            {
                lblMessage.Visibility = Visibility.Visible;
                Conexion.Close();
            }
        }

        private void TxtMail_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                UIElement focusedElement = Keyboard.FocusedElement as UIElement;
                if (focusedElement != null)
                {
                    //al presionar Enter pasaria al siguiente textbox,para ser rellenado por el usuario
                    focusedElement.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
                }
                e.Handled = true;
            }

        }

        private void TxtPass_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                BtnLogin_Click(sender, e);
                lblMessage.Content = "Correo o contraseña incorrectos";
                lblMessage.Visibility = Visibility.Visible;
            }
        }
        #region CerrarPrograma //____________________________________________________
        private void BtnCloseSesion_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// Cierra todo el programa.
        /// </summary>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Application.Current.Shutdown();
        }

        #endregion //_________________________________________________________________
    }
}
