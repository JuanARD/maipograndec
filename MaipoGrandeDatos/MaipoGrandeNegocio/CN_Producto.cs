﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MaipoGrandeDatos;

namespace MaipoGrandeNegocio
{
    public sealed class CN_Producto
    {
        //Singleton aplicado por Cristián Esquivel, Crud aplicado por Cristián Esquivel y Juan Román
        private readonly static CN_Producto _instance = new CN_Producto();

        private CN_Producto()
        {
        }

        public static CN_Producto Instance
        {
            get
            {
                return _instance;
            }
        }

        private CD_Producto objetoDeCD = new CD_Producto();
        public DataTable MostrarProduct()
        {
            DataTable tabla = new DataTable();
            tabla = objetoDeCD.Mostrar();

            tabla.Columns["idproducto"].ColumnName = "ID Producto";
            tabla.Columns["nombre"].ColumnName = "Nombre";
            tabla.Columns["tipo"].ColumnName = "Tipo";
            tabla.Columns.Remove("foto");
            return tabla;
        }

        public DataTable MostrarProductoById(int id) // Maximiliano González
        {
            DataTable tabla = new DataTable();
            tabla = objetoDeCD.MostrarProductoByID(id);
            return tabla;
        }

        public bool EliminarProducto(int id)
        {
            return objetoDeCD.Eliminar(id);
        }

    }
}