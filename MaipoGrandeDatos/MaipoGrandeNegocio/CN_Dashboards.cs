﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MaipoGrandeDatos;
using System.Collections;

namespace MaipoGrandeNegocio
{
    public class CN_Dashboards
    {
        public CN_Dashboards() { }

        private CD_DashBoards objetoDeCD = new CD_DashBoards();




        public DataTable MostrarTopClientes()
        {
            DataTable tabla = new DataTable();
            tabla = objetoDeCD.Top5Clientes();
            try
            {
                tabla.Columns["top_5_clientes"].ColumnName = "Clientes";
                tabla.Columns["cant_pedidos"].ColumnName = "Cantidad Pedidos";
            }
            catch (Exception) { }
            return tabla;
        }

        public DataTable MostrarTopProductores()
        {
            DataTable tabla = new DataTable();
            tabla = objetoDeCD.Top5Productores();
            try
            {
                tabla.Columns["top_5_productores"].ColumnName = "Productores";
                tabla.Columns["cant_pedidos"].ColumnName = "Cantidad Lotes";
            }
            catch (Exception) { }
            return tabla;
        }

        public DataTable MostrarTopTransportistas()
        {
            DataTable tabla = new DataTable();
            tabla = objetoDeCD.Top5Transportistas();
            try
            {
                tabla.Columns["top_5_transportistas"].ColumnName = "Transportistas";
                tabla.Columns["viajes_realizados"].ColumnName = "Viajes Concretados";
            }
            catch (Exception) { }
            return tabla;
        }

        public DataTable dasdasConteoVariedadesFrutas()
        {
            DataTable tabla = new DataTable();
            tabla = objetoDeCD.Top5Transportistas();
            try
            {
                tabla.Columns["fruta"].ColumnName = "Frutas";
                tabla.Columns["variedades"].ColumnName = "Variedades";
            }
            catch (Exception) { }
            return tabla;
        }

        public List<string[]> ConteoVariedadesFrutas()
        {
            CD_DashBoards con = new CD_DashBoards();
            return con.ContarVariedadesFrutas();
        }
    }
}
