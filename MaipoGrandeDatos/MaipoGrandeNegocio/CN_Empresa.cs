﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MaipoGrandeDatos;
namespace MaipoGrandeNegocio
{
    public sealed class CN_Empresa
    {
        //Singleton aplicado por Cristián Esquivel, Crud aplicado por Cristián Esquivel y Juan Román
        private readonly static CN_Empresa _instance = new CN_Empresa();

        private CN_Empresa()
        {
        }

        public static CN_Empresa Instance
        {
            get
            {
                return _instance;
            }
        }

        private CD_Empresa objetoDeCD = new CD_Empresa();

        public DataTable MostrarEmpresa()
        {
            DataTable tabla = new DataTable();
            tabla = objetoDeCD.Mostrar();
            tabla.Columns["idempresa"].ColumnName = "ID";
            tabla.Columns["rutempresa"].ColumnName = "Rut Empresa";
            tabla.Columns["tipoempresa"].ColumnName = "Tipo Empresa";
            tabla.Columns["nombrecomercial"].ColumnName = "Nombre Comercial";
            tabla.Columns["razonsocial"].ColumnName = "Razón Social";
            tabla.Columns.Remove("logo");
            tabla.Columns["direccion"].ColumnName = "Dirección";
            tabla.Columns["ciudad"].ColumnName = "Ciudad";
            tabla.Columns["pais"].ColumnName = "País";
            tabla.Columns["fono1"].ColumnName = "Fono 1";
            tabla.Columns["fono2"].ColumnName = "Fono 2";
            tabla.Columns["email"].ColumnName = "EMail";
            tabla.Columns["web"].ColumnName = "Web";
            return tabla;
        }

        public DataTable MostrarEmpresaPorID(int id)
        {
            DataTable tabla = new DataTable();
            tabla = objetoDeCD.MostrarPorID(id);
            tabla.Columns["idempresa"].ColumnName = "ID";
            tabla.Columns["rutempresa"].ColumnName = "Rut Empresa";
            tabla.Columns["tipoempresa"].ColumnName = "Tipo Empresa";
            tabla.Columns["nombrecomercial"].ColumnName = "Nombre Comercial";
            tabla.Columns["razonsocial"].ColumnName = "Razón Social";
            tabla.Columns.Remove("logo");
            tabla.Columns["direccion"].ColumnName = "Dirección";
            tabla.Columns["ciudad"].ColumnName = "Ciudad";
            tabla.Columns["pais"].ColumnName = "País";
            tabla.Columns["fono1"].ColumnName = "Fono 1";
            tabla.Columns["fono2"].ColumnName = "Fono 2";
            tabla.Columns["email"].ColumnName = "EMail";
            tabla.Columns["web"].ColumnName = "Web";
            return tabla;
        }


        public void InsertarEmpresa(int id_Empresa, string rut_empresa, string tipo_empresa, string nombre_comercial, string razon_social, string logo, string direccion, string ciudad, string pais, string fono1, string fono2, string email, string web)
        {
            objetoDeCD.Insertar(id_Empresa, rut_empresa, tipo_empresa, nombre_comercial, razon_social, logo, direccion, ciudad, pais, fono1, fono2, email, web);
        }

        public void ModificarEmpresa(int id_Empresa, string rut_empresa, string tipo_empresa, string nombre_comercial, string razon_social, string logo, string direccion, string ciudad, string pais, string fono1, string fono2, string email, string web)
        {
            objetoDeCD.Modificar(id_Empresa, rut_empresa, tipo_empresa, nombre_comercial, razon_social, logo, direccion, ciudad, pais, fono1, fono2, email, web);
        }

        public bool EliminarEmpresa(int id)
        {
            return objetoDeCD.Eliminar(id);
        }
    }
}
