﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MaipoGrandeDatos;

namespace MaipoGrandeNegocio
{
    public class CN_Lote_Producto
    {
        //Crud aplicado por Juan Román
        private CD_Lote_Producto objetoDeCD = new CD_Lote_Producto();

        public DataTable MostrarLoteProducto()
        {
            DataTable tabla = new DataTable();
            tabla = objetoDeCD.Mostrar();
            return tabla;
        }

        public DataTable MostrarLoteProductoById(int id) // Maximiliano González
        {
            DataTable tabla = new DataTable();
            tabla = objetoDeCD.MostrarLoteProductoById(id);
            return tabla;
        }

        public void InsertarLoteProducto(int id_lote_producto, int id_producto, int id_empresa, int id_unidad, int cantidad_productos, int precioUnidad, int calidad, DateTime fecha_vencimiento, string condiciones_almacenaje, string descripcion, char vigente, char ventaAlExtranjero)
        {
            objetoDeCD.Insertar(id_lote_producto, id_producto, id_empresa, id_unidad, cantidad_productos, precioUnidad, calidad, fecha_vencimiento, condiciones_almacenaje, descripcion, vigente, ventaAlExtranjero);
        }

        public void ModificarLoteProducto(int id_lote_producto, int id_producto, int id_empresa, int id_unidad, int cantidad_productos, int precioUnidad, int calidad, DateTime fecha_vencimiento, string condiciones_almacenaje, string descripcion, char vigente, char ventaAlExtranjero)
        {
            objetoDeCD.Modificar(id_lote_producto, id_producto, id_empresa, id_unidad, cantidad_productos, precioUnidad, calidad, fecha_vencimiento, condiciones_almacenaje, descripcion, vigente, ventaAlExtranjero);
        }

        public void EliminarLoteProducto(int id)
        {
            objetoDeCD.Eliminar(id);
        }
    }
}
