﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MaipoGrandeDatos;

namespace MaipoGrandeNegocio
{
    public class CN_Lote_Ofrecido
    {
        //Crud aplicado por Juan Román
        private CD_Lote_Ofrecido objetoDeCD = new CD_Lote_Ofrecido();

        public DataTable MostrarLoteOfrecido()
        {
            DataTable tabla = new DataTable();
            tabla = objetoDeCD.Mostrar();
            return tabla;
        }

        public DataTable MostrarLotesByIdPedido(int id) // Maximiliano González
        {
            DataTable tabla = new DataTable();
            tabla = objetoDeCD.MostrarLotesByIdPedido(id);

            tabla.Columns["idloteofrecido"].ColumnName = "ID";
            tabla.Columns["idproductopedido"].ColumnName = "ID Pedido";
            tabla.Columns["idproducto"].ColumnName = "ID Producto";
            tabla.Columns["enbodega"].ColumnName = "Bodega";
            tabla.Columns["cantidad"].ColumnName = "Cantidad";
            tabla.Columns["cantidadsaldo"].ColumnName = "Saldo";
            tabla.Columns["vigente"].ColumnName = "Vigente";
            tabla.Columns["idrepresentante"].ColumnName = "ID Representante";
            return tabla;
        }

        public void ValidarLoteProductoPedido(int id)//Maximiliano González
        {
            objetoDeCD.ValidarLoteProductoPedido(id);
        }

        public void InsertarLoteOfrecido(int id_lote_ofrecido, int id_producto_pedido, int id_lote_producto, string comentario, char en_bodega, int cantidad_saldo, int id_representantem, char vigente)
        {
            objetoDeCD.Insertar(id_lote_ofrecido, id_producto_pedido, id_lote_producto, comentario, en_bodega, cantidad_saldo, id_representantem, vigente);
        }

        public void ModificarLoteOfrecido(int id_lote_ofrecido, int id_producto_pedido, int id_lote_producto, string comentario, char en_bodega, int cantidad_saldo, int id_representantem, char vigente)
        {
            objetoDeCD.Modificar(id_lote_ofrecido, id_producto_pedido, id_lote_producto, comentario, en_bodega, cantidad_saldo, id_representantem, vigente);
        }

        public void EliminarLoteOfrecido(int id)
        {
            objetoDeCD.Eliminar(id);
        }
    }
}
