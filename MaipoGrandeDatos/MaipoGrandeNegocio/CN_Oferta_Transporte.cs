﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MaipoGrandeDatos;

namespace MaipoGrandeNegocio
{
    public sealed class CN_Oferta_Transporte
    {
        //Singleton aplicado por Cristián Esquivel, Módulo por Juan Román
        private readonly static CN_Oferta_Transporte _instance = new CN_Oferta_Transporte();

        private CN_Oferta_Transporte()
        {
        }

        public static CN_Oferta_Transporte Instance
        {
            get
            {
                return _instance;
            }
        }

        private CD_Oferta_Transporte objetoDeCD = new CD_Oferta_Transporte();
        public DataTable MostrarOfertatra()
        {
            DataTable tabla = new DataTable();
            tabla = objetoDeCD.Mostrar();

            tabla.Columns["idofertatransporte"].ColumnName = "ID Oferta";
            tabla.Columns["idsubasta"].ColumnName = "ID Subasta";
            tabla.Columns["nombresubasta"].ColumnName = "Nombre Subasta";
            tabla.Columns["idvehiculo"].ColumnName = "ID Vehículo";
            tabla.Columns["idrepresentante"].ColumnName = "Id Representante";
            tabla.Columns["nombre"].ColumnName = "Nombre";
            tabla.Columns["appaterno"].ColumnName = "Apellido Paterno";
            tabla.Columns["vigente"].ColumnName = "Vigente";
            return tabla;
        }

        public DataTable MostrarSubastaEnOferta(int id)//Maximiliano González
        {
            DataTable tabla = new DataTable();
            tabla = objetoDeCD.MostrarSubastaEnOferta(id);
            tabla.Columns["idofertatransporte"].ColumnName = "ID Oferta";
            tabla.Columns["idsubasta"].ColumnName = "ID Subasta";
            tabla.Columns["idvehiculo"].ColumnName = "ID Vehiculo";
            tabla.Columns["costotransporte"].ColumnName = "Costo Transporte";



            return tabla;
        }

        public void ValidarOfertaPedido(int id_oferta)//Maximiliano González
        {
            objetoDeCD.ModificarEstadoPedidoSubastaOferta(id_oferta);
        }

    }
}
