﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MaipoGrandeDatos;

namespace MaipoGrandeNegocio
{
    public sealed class CN_Unidad_Medicion
    {
        //Singleton aplicado por Cristián Esquivel, Crud aplicado por Cristián Esquivel y Juan Román
        private readonly static CN_Unidad_Medicion _instance = new CN_Unidad_Medicion();

        private CN_Unidad_Medicion()
        {
        }

        public static CN_Unidad_Medicion Instance
        {
            get
            {
                return _instance;
            }
        }

        private CD_Unidad_Medicion objetoDeCD = new CD_Unidad_Medicion();

        public DataTable MostrarUnidadMedicion()
        {
            DataTable tabla = new DataTable();
            tabla = objetoDeCD.Mostrar();
            tabla.Columns["idunidad"].ColumnName = "ID";
            tabla.Columns["descripcion"].ColumnName = "Descripción";
            return tabla;
        }

        public void InsertarUnidadMedicion(int id_unidad, string descripcion)
        {
            objetoDeCD.Insertar(id_unidad, descripcion);
        }

        public void ModificarUnidadMedicion(int id_unidad, string descripcion)
        {
            objetoDeCD.Modificar(id_unidad, descripcion);
        }

        public bool EliminarUnidadMedicion(int id)
        {
            return objetoDeCD.Eliminar(id);
        }
    }
}
