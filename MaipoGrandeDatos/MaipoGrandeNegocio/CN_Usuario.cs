﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MaipoGrandeDatos;

namespace MaipoGrandeNegocio
{
    public sealed class CN_Usuario
    {
        //Singleton aplicado por Cristián Esquivel, Módulo aplicado por Juan Román
        private readonly static CN_Usuario _instance = new CN_Usuario();

        private CN_Usuario()
        {
        }

        public static CN_Usuario Instance
        {
            get
            {
                return _instance;
            }
        }

        private CD_Usuario objetoDeCD = new CD_Usuario();

        public DataTable MostrarUsuario()
        {
            DataTable tabla = new DataTable();
            tabla = objetoDeCD.Mostrar();

            tabla.Columns["idusuario"].ColumnName = "ID Usuario";
            tabla.Columns["idrepresentante"].ColumnName = "ID Representante";
            tabla.Columns["rutrepresentante"].ColumnName = "Rut";
            tabla.Columns["nombre"].ColumnName = "Nombre";
            tabla.Columns["appaterno"].ColumnName = "Apellido Paterno";
            tabla.Columns["apmaterno"].ColumnName = "Apellido Materno";
            tabla.Columns["direccion"].ColumnName = "Dirección";
            tabla.Columns["fechanacimiento"].ColumnName = "Fecha de Nacimiento";
            tabla.Columns["fono1"].ColumnName = "Telefono 01";
            tabla.Columns["fono2"].ColumnName = "Telefono 02";
            tabla.Columns["sexo"].ColumnName = "Sexo";
            tabla.Columns["rolrepresentante"].ColumnName = "Rol";
            tabla.Columns["email"].ColumnName = "Email";
            tabla.Columns["contrasena"].ColumnName = "Contraseña";
            tabla.Columns["idempresa"].ColumnName = "ID Empresa";
            tabla.Columns["nombrecomercial"].ColumnName = "Nombre Comercial";
            tabla.Columns["vigente"].ColumnName = "¿Sigue Vigente?";
            return tabla;
        }
    }
}
