﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MaipoGrandeDatos;

namespace MaipoGrandeNegocio
{
    public class CN_Subasta
    {
        //Crud aplicado por Juan Román
        private CD_Subasta objetoDeCD = new CD_Subasta();

        public DataTable MostrarSubasta()
        {
            DataTable tabla = new DataTable();
            tabla = objetoDeCD.Mostrar();
            return tabla;
        }

        public DataTable MostrarSubastaAdespacho() //Maximiliano González
        {
            DataTable tabla = new DataTable();
            tabla = objetoDeCD.MostrarAofertar();

            try
            {
                tabla.Columns["idsubasta"].ColumnName = "ID Subasta";
                tabla.Columns["nombresubasta"].ColumnName = "Cliente";
                tabla.Columns["idpedido"].ColumnName = "ID Pedido";
                tabla.Columns["fechapublicacion"].ColumnName = "Fecha Publicación";
                tabla.Columns["fechatermino"].ColumnName = "Fecha Termino";
                tabla.Columns["idrepresentante"].ColumnName = "ID Representante";
            }
            catch (Exception)
            {
            }

            return tabla;
        }

        public void InsertarSubasta(int id_subasta, string nombre_subasta, int id_pedido, DateTime fecha_publicacion, DateTime fecha_termino, int id_representante)
        {
            objetoDeCD.Insertar(id_subasta, nombre_subasta, id_pedido, fecha_publicacion, fecha_termino, id_representante);
        }

        public void ModificarSubasta(int id_subasta, string nombre_subasta, int id_pedido, DateTime fecha_publicacion, DateTime fecha_termino, int id_representante)
        {
            objetoDeCD.Modificar(id_subasta, nombre_subasta, id_pedido, fecha_publicacion, fecha_termino, id_representante);
        }

        public void EliminarSubasta(int id)
        {
            objetoDeCD.Eliminar(id);
        }
    }
}
