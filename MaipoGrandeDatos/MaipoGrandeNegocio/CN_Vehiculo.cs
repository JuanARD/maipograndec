﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MaipoGrandeDatos;

namespace MaipoGrandeNegocio
{
    public sealed class CN_Vehiculo
    {
        //Singleton aplicado por Cristián Esquivel, Módulo aplicado por Juan Román
        private readonly static CN_Vehiculo _instance = new CN_Vehiculo();

        private CN_Vehiculo()
        {
        }

        public static CN_Vehiculo Instance
        {
            get
            {
                return _instance;
            }
        }

        private CD_Vehiculo objetoDeCD = new CD_Vehiculo();
        public DataTable MostrarVehiculos()
        {
            DataTable tabla = new DataTable();
            tabla = objetoDeCD.Mostrar();

            tabla.Columns["idempresa"].ColumnName = "ID Empresa";
            tabla.Columns["nombrecomercial"].ColumnName = "Nombre Comercial";
            tabla.Columns["idvehiculo"].ColumnName = "ID Vehiculo";
            tabla.Columns["clase"].ColumnName = "Clase";
            tabla.Columns["cmalto"].ColumnName = "Cm Alto";
            tabla.Columns["cmancho"].ColumnName = "Cm Ancho";
            tabla.Columns["cmlargo"].ColumnName = "Cm Largo";
            tabla.Columns["frigorifico"].ColumnName = "Frigorifico";
            tabla.Columns["patente"].ColumnName = "Patente";
            return tabla;
        }

        public DataTable MostrarVehiculoById(int id) // Maximiliano González
        {
            DataTable tabla = new DataTable();
            tabla = objetoDeCD.MostrarVehiculoById(id);
            return tabla;
        }

        public bool EliminarVehiculo(int id)
        {
            return objetoDeCD.Eliminar(id);
        }
    }
}
