﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MaipoGrandeDatos;

namespace MaipoGrandeNegocio
{
    public sealed class CN_Representante
    {
        //Singleton aplicado por Cristián Esquivel, Crud aplicado por Juan Román
        private readonly static CN_Representante _instance = new CN_Representante();

        private CN_Representante()
        {
        }

        public static CN_Representante Instance
        {
            get
            {
                return _instance;
            }
        }

        private CD_Representante objetoDeCD = new CD_Representante();

        public DataTable MostrarRepresentante()
        {
            DataTable tabla = new DataTable();
            tabla = objetoDeCD.Mostrar();
            tabla.Columns.Remove("IDREPRESENTANTE");
            tabla.Columns["IDEMPRESA"].ColumnName = "ID Empresa";
            tabla.Columns["RUTREPRESENTANTE"].ColumnName = "Rut";
            tabla.Columns["ROLREPRESENTANTE"].ColumnName = "Rol";
            tabla.Columns["NOMBRE"].ColumnName = "Nombre";
            tabla.Columns["APPATERNO"].ColumnName = "Apellido Paterno";
            tabla.Columns["APMATERNO"].ColumnName = "Apellido Materno";
            tabla.Columns["SEXO"].ColumnName = "Sexo";
            tabla.Columns["FECHANACIMIENTO"].ColumnName = "Fecha de Nacimiento";
            tabla.Columns["FONO1"].ColumnName = "Telefono 01";
            tabla.Columns["FONO2"].ColumnName = "Telefono 02";
            tabla.Columns["DIRECCION"].ColumnName = "Dirección";
            tabla.Columns["VIGENTE"].ColumnName = "¿Sigue Vigente?";
            return tabla;
        }

        public DataTable MostrarRepresentantePorID(int id)
        {
            DataTable tabla = new DataTable();
            tabla = objetoDeCD.MostrarPorID(id);
            tabla.Columns["idrepresentante"].ColumnName = "ID";
            tabla.Columns["idempresa"].ColumnName = "ID Empresa";
            tabla.Columns["rutrepresentante"].ColumnName = "Rut";
            tabla.Columns["rolrepresentante"].ColumnName = "Rol";
            tabla.Columns["nombre"].ColumnName = "Nombre";
            tabla.Columns["appaterno"].ColumnName = "Apellido Paterno";
            tabla.Columns["apmaterno"].ColumnName = "Apellido Materno";
            tabla.Columns["sexo"].ColumnName = "Sexo";
            tabla.Columns["fechanacimiento"].ColumnName = "Fecha de Nacimiento";
            tabla.Columns["fono1"].ColumnName = "Telefono 01";
            tabla.Columns["fono2"].ColumnName = "Telefono 02";
            tabla.Columns["DIRECCION"].ColumnName = "Dirección";
            tabla.Columns["vigente"].ColumnName = "¿Sigue Vigente?";
            return tabla;
        }

        public DataTable MostrarRepresentanteEmpresaID(int id) // Maximiliano González
        {
            DataTable tabla = new DataTable();
            tabla = objetoDeCD.MostrarRepresentanteEmpresaID(id);
            return tabla;
        }

        public void InsertarRepresentante(int id_representante, int id_empresa, string rut_representante, string rol_representante, string nombre, string ap_paterno, string ap_materno, char sexo, DateTime fecha_nacimiento, string fono1, string fono2, string direccion, int rut_empresa, char vigente)
        {
            objetoDeCD.Insertar(id_representante, id_empresa, rut_representante, rol_representante, nombre, ap_paterno, ap_materno, sexo, fecha_nacimiento, fono1, fono2, rut_empresa, vigente);
        }

        public void ModificarRepresentante(int id_representante, int id_empresa, string rut_representante, string rol_representante, string nombre, string ap_paterno, string ap_materno, char sexo, DateTime fecha_nacimiento, string fono1, string fono2, string direccion, int rut_empresa, char vigente)
        {
            objetoDeCD.Modificar(id_representante, id_empresa, rut_representante, rol_representante, nombre, ap_paterno, ap_materno, sexo, fecha_nacimiento, fono1, fono2, rut_empresa, vigente);
        }

        public bool EliminarRepresentante(int id)
        {
            return objetoDeCD.Eliminar(id);
        }

        public List<int> ListarIdRepresentante()
        {

            List<int> ListarIdRepresentante = new List<int>();
            DataTable tabla = new DataTable();
            tabla = objetoDeCD.ListarIdRepresentante();

            ListarIdRepresentante = tabla.AsEnumerable().Select(it => int.Parse(it[0].ToString())).ToList();


            return ListarIdRepresentante;
        }
    }
}