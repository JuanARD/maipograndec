﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MaipoGrandeDatos;

namespace MaipoGrandeNegocio
{
    public sealed class CN_Producto_Pedido
    {
        //Singleton aplicado por Cristián Esquivel, Crud aplicado por Cristián Esquivel y Juan Román
        private readonly static CN_Producto_Pedido _instance = new CN_Producto_Pedido();

        private CN_Producto_Pedido()
        {
        }

        public static CN_Producto_Pedido Instance
        {
            get
            {
                return _instance;
            }
        }
        private CD_Producto_Pedido objetoDeCD = new CD_Producto_Pedido();

        public DataTable MostrarProductoPedido()
        {
            DataTable tabla = new DataTable();
            tabla = objetoDeCD.Mostrar();
            return tabla;
        }

        public DataTable MostrarProductosEnPedido(int id)
        {
            DataTable tabla = new DataTable();
            tabla = objetoDeCD.MostrarProductosEnPedido(id);
            try
            {
                tabla.Columns["nombre"].ColumnName = "Nombre Producto";
                tabla.Columns["tipo"].ColumnName = "Tipo";
                tabla.Columns["descripcion"].ColumnName = "Unidad de Medición";
                tabla.Columns["cantidad"].ColumnName = "Cantidad por Unidad";
            }
            catch (Exception)
            {
            }
            return tabla;
        }

        public void InsertarProductoPedido(int id_producto_pedido, int id_pedido, int cantidad, int id_unidad, int id_producto)
        {
            objetoDeCD.Insertar(id_producto_pedido, id_pedido, cantidad, id_unidad, id_producto);
        }

        public void ModificarProductoPedido(int id_producto_pedido, int id_pedido, int cantidad, int id_unidad, int id_producto)
        {
            objetoDeCD.Modificar(id_producto_pedido, id_pedido, cantidad, id_unidad, id_producto);
        }

        public void EliminarProductoPedido(int id)
        {
            objetoDeCD.Eliminar(id);
        }
    }
}
