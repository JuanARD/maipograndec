﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MaipoGrandeDatos;
namespace MaipoGrandeNegocio
{
    public class CN_Reportes
    {   //Métodos de Reportes aplicado por Maximiliano González
        private CD_Reportes objetoDeCD = new CD_Reportes();


        public DataTable MostrarLotesByCalidad(String cal)//1
        {
            DataTable tabla = new DataTable();
            tabla = objetoDeCD.MostrarLotesByCalidad(cal);
            return tabla;
        }


        public DataTable MostrarPedidoByEstado(String estado) //2
        {
            DataTable tabla = new DataTable();
            tabla = objetoDeCD.MostrarPedidoByEstado(estado);
            return tabla;
        }

        public DataTable MostrarOfertaTransporte() //3
        {
            DataTable tabla = new DataTable();
            tabla = objetoDeCD.MostrarOfertaTransporte();
            return tabla;
        }

        public DataTable MostrarSubastaByEstado(string est) //4
        {
            DataTable tabla = new DataTable();
            tabla = objetoDeCD.MostrarSubastaByEstado(est);
            return tabla;
        }

    }
}
