﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MaipoGrandeDatos;


namespace MaipoGrandeNegocio
{
    public class CN_Contrato
    {
        //Crud aplicado por Juan Román
        private CD_Contrato objetoDeCD = new CD_Contrato();

        public DataTable MostrarContrato()
        {
            DataTable tabla = new DataTable();
            tabla = objetoDeCD.Mostrar();
            return tabla;
        }

        public void InsertarContrato(int id_contrato, int id_empresa, DateTime fecha_inico, DateTime fecha_termino, string ruta_documento, char vigente)
        {
            objetoDeCD.Insertar(id_contrato, id_empresa, fecha_inico, fecha_termino, ruta_documento, vigente);
        }

        public void ModificarContrato(int id_contrato, int id_empresa, DateTime fecha_inico, DateTime fecha_termino, string ruta_documento, char vigente)
        {
            objetoDeCD.Modificar(id_contrato, id_empresa, fecha_inico, fecha_termino, ruta_documento, vigente);
        }

        public void EliminarContrato(int id)
        {
            objetoDeCD.Eliminar(id);
        }
    }
}
