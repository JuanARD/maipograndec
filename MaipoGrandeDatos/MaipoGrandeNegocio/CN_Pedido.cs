﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MaipoGrandeDatos;

namespace MaipoGrandeNegocio
{
    public sealed class CN_Pedido
    {
        //Singleton aplicado por Cristián Esquivel, Crud aplicado por Cristián Esquivel y Juan Román
        private readonly static CN_Pedido _instance = new CN_Pedido();

        private CN_Pedido()
        {
        }

        public static CN_Pedido Instance
        {
            get
            {
                return _instance;
            }
        }
        private CD_Pedido objetoDeCD = new CD_Pedido();

        public DataTable MostrarPedido()
        {
            DataTable tabla = new DataTable();
            tabla = objetoDeCD.Mostrar();
            try
            {
                tabla.Columns["idpedido"].ColumnName = "ID Pedido";
                tabla.Columns["idrepresentante"].ColumnName = "ID Representante";
                tabla.Columns["rutacontrato"].ColumnName = "Ruta Contrato";
                tabla.Columns["fechainiciopedido"].ColumnName = "Fecha Inicio";
                tabla.Columns["fechaentregaestipulada"].ColumnName = "Fecha Entrega Estipulada";
                tabla.Columns["fechaentregareal"].ColumnName = "Fecha Entrega Real";
                tabla.Columns["observacioninicial"].ColumnName = "Observación Inicial";
                tabla.Columns["estadoPedido"].ColumnName = "Estado";
                tabla.Columns["calidadentrega"].ColumnName = "Calidad de Entrega";
                tabla.Columns["observacionentrega"].ColumnName = "Observación de Entrega";
                tabla.Columns["devuelto"].ColumnName = "¿Se ha devuelto?";
                tabla.Columns["vigente"].ColumnName = "¿Sigue Vigente?";
                tabla.Columns["direccion"].ColumnName = "Direccion";
                tabla.Columns["fono"].ColumnName = "Fono";

            }
            catch (Exception)
            {
            }

            return tabla;
        }

        public DataTable MostrarPedidoReqNegociacion() // Maximiliano González
        {
            DataTable tabla = new DataTable();
            tabla = objetoDeCD.MostrarPedidoReqNegociacion();
            try
            {
                tabla.Columns["idpedido"].ColumnName = "ID Pedido";
                tabla.Columns["nombre"].ColumnName = "Nombre";
                tabla.Columns["appaterno"].ColumnName = "Apellido";
                tabla.Columns["fechainiciopedido"].ColumnName = "Fecha Inicio";
                tabla.Columns["fechaentregaestipulada"].ColumnName = "Fecha Entrega";
                tabla.Columns["estadopedido"].ColumnName = "Estado";
                tabla.Columns["calidadentrega"].ColumnName = "Calidad";
                tabla.Columns["direccion"].ColumnName = "Destino";
                tabla.Columns["fono"].ColumnName = "Fono";

            }
            catch (Exception) { }
            return tabla;
        }

        public void InsertarPedido(int id_pedido, int id_representante, string ruta_contrato, DateTime fecha_inicio_pedido, DateTime fecha_entrega_estipulada,
            DateTime fecha_entrega_real, string observacion_inicial, string estado_pedido, string calidadEntrega, string observacion_entrega, char devuelto, char vigente, string direccion, string fono)
        {
            objetoDeCD.Insertar(id_pedido, id_representante, ruta_contrato, fecha_inicio_pedido, fecha_entrega_estipulada, fecha_entrega_real, observacion_inicial, estado_pedido, calidadEntrega, observacion_entrega, devuelto, vigente, direccion, fono);
        }

        public void ModificarPedido(int id_pedido, int id_representante, string ruta_contrato, DateTime fecha_inicio_pedido, DateTime fecha_entrega_estipulada,
            string fecha_entrega_real, string observacion_inicial, string estado_pedido, string calidadEntrega, string observacion_entrega, char devuelto, char vigente, string direccion, string fono)
        {
            objetoDeCD.Modificar(id_pedido, id_representante, ruta_contrato, fecha_inicio_pedido, fecha_entrega_estipulada, fecha_entrega_real, observacion_inicial, estado_pedido, calidadEntrega, observacion_entrega, devuelto, vigente, direccion, fono);
        }

        public void EliminarPedido(int id)
        {
            objetoDeCD.Eliminar(id);
        }

        public DataTable MostrarPedidoById(int id) // Maximiliano González
        {
            DataTable tabla = new DataTable();
            tabla = objetoDeCD.MostrarPedidoById(id);
            return tabla;
        }
    }
}