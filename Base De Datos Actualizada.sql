--------------------------------------------------------
-- Archivo creado  - viernes-noviembre-15-2019   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Sequence SEQ_CONTRATO
--------------------------------------------------------

   CREATE SEQUENCE  "BDMAIPOGRANDE"."SEQ_CONTRATO"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  ORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEQ_COSTOS_VIAJE
--------------------------------------------------------

   CREATE SEQUENCE  "BDMAIPOGRANDE"."SEQ_COSTOS_VIAJE"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEQ_EMPRESA
--------------------------------------------------------

   CREATE SEQUENCE  "BDMAIPOGRANDE"."SEQ_EMPRESA"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 10 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEQ_LOTE_OFRECIDO
--------------------------------------------------------

   CREATE SEQUENCE  "BDMAIPOGRANDE"."SEQ_LOTE_OFRECIDO"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEQ_LOTE_PRODUCTO
--------------------------------------------------------

   CREATE SEQUENCE  "BDMAIPOGRANDE"."SEQ_LOTE_PRODUCTO"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 2 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEQ_OFERTA_TRANSPORTE
--------------------------------------------------------

   CREATE SEQUENCE  "BDMAIPOGRANDE"."SEQ_OFERTA_TRANSPORTE"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 2 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEQ_PEDIDO
--------------------------------------------------------

   CREATE SEQUENCE  "BDMAIPOGRANDE"."SEQ_PEDIDO"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 36 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEQ_PRODUCTO
--------------------------------------------------------

   CREATE SEQUENCE  "BDMAIPOGRANDE"."SEQ_PRODUCTO"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 5 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEQ_PRODUCTO_PEDIDO
--------------------------------------------------------

   CREATE SEQUENCE  "BDMAIPOGRANDE"."SEQ_PRODUCTO_PEDIDO"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 37 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEQ_REPRESENTANTE
--------------------------------------------------------

   CREATE SEQUENCE  "BDMAIPOGRANDE"."SEQ_REPRESENTANTE"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 14 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEQ_SUBASTA
--------------------------------------------------------

   CREATE SEQUENCE  "BDMAIPOGRANDE"."SEQ_SUBASTA"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 5 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEQ_UNIDAD_MEDICION
--------------------------------------------------------

   CREATE SEQUENCE  "BDMAIPOGRANDE"."SEQ_UNIDAD_MEDICION"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 3 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEQ_USUARIO
--------------------------------------------------------

   CREATE SEQUENCE  "BDMAIPOGRANDE"."SEQ_USUARIO"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 3 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEQ_VEHICULO
--------------------------------------------------------

   CREATE SEQUENCE  "BDMAIPOGRANDE"."SEQ_VEHICULO"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 4 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEQ_VIAJE
--------------------------------------------------------

   CREATE SEQUENCE  "BDMAIPOGRANDE"."SEQ_VIAJE"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Table T_CONTRATO
--------------------------------------------------------

  CREATE TABLE "BDMAIPOGRANDE"."T_CONTRATO" 
   (	"IDCONTRATO" NUMBER(*,0), 
	"IDEMPRESA" NUMBER(*,0), 
	"FECHAINICIO" DATE, 
	"FECHATERMINO" DATE, 
	"RUTADOCUMENTO" VARCHAR2(100 BYTE), 
	"VIGENTE" CHAR(1 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table T_COSTOS_VIAJE
--------------------------------------------------------

  CREATE TABLE "BDMAIPOGRANDE"."T_COSTOS_VIAJE" 
   (	"IDCOSTOVIAJE" NUMBER(*,0), 
	"IDVIAJE" NUMBER(*,0), 
	"TIPOCOSTO" VARCHAR2(20 BYTE), 
	"OBSERVACIONES" VARCHAR2(500 BYTE), 
	"VALOR" FLOAT(3)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table T_EMPRESA
--------------------------------------------------------

  CREATE TABLE "BDMAIPOGRANDE"."T_EMPRESA" 
   (	"IDEMPRESA" NUMBER(*,0), 
	"RUTEMPRESA" VARCHAR2(15 BYTE), 
	"TIPOEMPRESA" VARCHAR2(30 BYTE), 
	"NOMBRECOMERCIAL" VARCHAR2(50 BYTE), 
	"RAZONSOCIAL" VARCHAR2(50 BYTE), 
	"LOGO" BLOB, 
	"DIRECCION" VARCHAR2(60 BYTE), 
	"CIUDAD" VARCHAR2(20 BYTE), 
	"PAIS" VARCHAR2(20 BYTE), 
	"FONO1" VARCHAR2(15 BYTE), 
	"FONO2" VARCHAR2(15 BYTE), 
	"EMAIL" VARCHAR2(60 BYTE), 
	"WEB" VARCHAR2(60 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" 
 LOB ("LOGO") STORE AS BASICFILE (
  TABLESPACE "USERS" ENABLE STORAGE IN ROW CHUNK 8192 RETENTION 
  NOCACHE LOGGING 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)) ;
--------------------------------------------------------
--  DDL for Table T_LOTE_OFRECIDO
--------------------------------------------------------

  CREATE TABLE "BDMAIPOGRANDE"."T_LOTE_OFRECIDO" 
   (	"IDLOTEOFRECIDO" NUMBER(*,0), 
	"IDPRODUCTOPEDIDO" NUMBER(*,0), 
	"IDLOTEPRODUCTO" NUMBER(*,0), 
	"COMENTARIO" VARCHAR2(300 BYTE), 
	"ENBODEGA" CHAR(1 BYTE), 
	"CANTIDADSALDO" NUMBER(*,0), 
	"IDREPRESENTANTE" NUMBER(*,0), 
	"VIGENTE" CHAR(1 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table T_LOTE_PRODUCTO
--------------------------------------------------------

  CREATE TABLE "BDMAIPOGRANDE"."T_LOTE_PRODUCTO" 
   (	"IDLOTEPRODUCTO" NUMBER(*,0), 
	"IDPRODUCTO" NUMBER(*,0), 
	"IDEMPRESA" NUMBER(*,0), 
	"IDUNIDAD" NUMBER(*,0), 
	"CANTIDADPRODUCTOS" NUMBER(*,0), 
	"PRECIOUNIDAD" FLOAT(3), 
	"CALIDAD" NUMBER(*,0), 
	"FECHAVENCIMIENTO" DATE, 
	"CONDICIONESALMACENAJE" VARCHAR2(300 BYTE), 
	"DESCRIPCION" CLOB, 
	"VIGENTE" CHAR(1 BYTE), 
	"VENTAALEXTRANJERO" CHAR(1 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" 
 LOB ("DESCRIPCION") STORE AS BASICFILE (
  TABLESPACE "USERS" ENABLE STORAGE IN ROW CHUNK 8192 RETENTION 
  NOCACHE LOGGING 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)) ;
--------------------------------------------------------
--  DDL for Table T_OFERTA_TRANSPORTE
--------------------------------------------------------

  CREATE TABLE "BDMAIPOGRANDE"."T_OFERTA_TRANSPORTE" 
   (	"IDOFERTATRANSPORTE" NUMBER(*,0), 
	"IDSUBASTA" NUMBER(*,0), 
	"IDVEHICULO" NUMBER(*,0), 
	"IDREPRESENTANTE" NUMBER(*,0), 
	"COSTOTRANSPORTE" NUMBER(*,0), 
	"VIGENTE" CHAR(1 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table T_PEDIDO
--------------------------------------------------------

  CREATE TABLE "BDMAIPOGRANDE"."T_PEDIDO" 
   (	"IDPEDIDO" NUMBER(*,0), 
	"IDREPRESENTANTE" NUMBER(*,0), 
	"RUTACONTRATO" VARCHAR2(100 BYTE), 
	"FECHAINICIOPEDIDO" DATE, 
	"FECHAENTREGAESTIPULADA" DATE, 
	"FECHAENTREGAREAL" DATE, 
	"OBSERVACIONINICIAL" CLOB, 
	"ESTADOPEDIDO" VARCHAR2(30 BYTE), 
	"CALIDADENTREGA" VARCHAR2(15 BYTE), 
	"OBSERVACIONENTREGA" CLOB, 
	"DEVUELTO" CHAR(1 BYTE), 
	"VIGENTE" CHAR(1 BYTE), 
	"DIRECCION" VARCHAR2(60 BYTE), 
	"FONO" VARCHAR2(15 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" 
 LOB ("OBSERVACIONINICIAL") STORE AS BASICFILE (
  TABLESPACE "USERS" ENABLE STORAGE IN ROW CHUNK 8192 RETENTION 
  NOCACHE LOGGING 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)) 
 LOB ("OBSERVACIONENTREGA") STORE AS BASICFILE (
  TABLESPACE "USERS" ENABLE STORAGE IN ROW CHUNK 8192 RETENTION 
  NOCACHE LOGGING 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)) ;
--------------------------------------------------------
--  DDL for Table T_PRODUCTO
--------------------------------------------------------

  CREATE TABLE "BDMAIPOGRANDE"."T_PRODUCTO" 
   (	"IDPRODUCTO" NUMBER(*,0), 
	"NOMBRE" VARCHAR2(20 BYTE), 
	"TIPO" VARCHAR2(30 BYTE), 
	"FOTO" BLOB
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" 
 LOB ("FOTO") STORE AS BASICFILE (
  TABLESPACE "USERS" ENABLE STORAGE IN ROW CHUNK 8192 RETENTION 
  NOCACHE LOGGING 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)) ;
--------------------------------------------------------
--  DDL for Table T_PRODUCTO_PEDIDO
--------------------------------------------------------

  CREATE TABLE "BDMAIPOGRANDE"."T_PRODUCTO_PEDIDO" 
   (	"IDPRODUCTOPEDIDO" NUMBER(*,0), 
	"IDPEDIDO" NUMBER(*,0), 
	"CANTIDAD" NUMBER(*,0), 
	"IDUNIDAD" NUMBER(*,0), 
	"IDPRODUCTO" NUMBER(*,0)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table T_REPRESENTANTE
--------------------------------------------------------

  CREATE TABLE "BDMAIPOGRANDE"."T_REPRESENTANTE" 
   (	"IDREPRESENTANTE" NUMBER(*,0), 
	"IDEMPRESA" NUMBER(*,0), 
	"RUTREPRESENTANTE" VARCHAR2(12 BYTE), 
	"ROLREPRESENTANTE" VARCHAR2(20 BYTE), 
	"NOMBRE" VARCHAR2(60 BYTE), 
	"APPATERNO" VARCHAR2(30 BYTE), 
	"APMATERNO" VARCHAR2(30 BYTE), 
	"SEXO" CHAR(1 BYTE), 
	"FECHANACIMIENTO" DATE, 
	"FONO1" VARCHAR2(15 BYTE), 
	"FONO2" VARCHAR2(15 BYTE), 
	"VIGENTE" CHAR(1 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table T_SUBASTA
--------------------------------------------------------

  CREATE TABLE "BDMAIPOGRANDE"."T_SUBASTA" 
   (	"IDSUBASTA" NUMBER(*,0), 
	"NOMBRESUBASTA" VARCHAR2(60 BYTE), 
	"IDPEDIDO" NUMBER(*,0), 
	"FECHAPUBLICACION" DATE, 
	"FECHATERMINO" DATE, 
	"IDREPRESENTANTE" NUMBER(*,0)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table T_UNIDAD_MEDICION
--------------------------------------------------------

  CREATE TABLE "BDMAIPOGRANDE"."T_UNIDAD_MEDICION" 
   (	"IDUNIDAD" NUMBER(*,0), 
	"DESCRIPCION" VARCHAR2(20 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table T_USUARIO
--------------------------------------------------------

  CREATE TABLE "BDMAIPOGRANDE"."T_USUARIO" 
   (	"IDUSUARIO" NUMBER(*,0), 
	"EMAIL" VARCHAR2(60 BYTE), 
	"CONTRASENA" VARCHAR2(100 BYTE), 
	"IDREPRESENTANTE" NUMBER(*,0), 
	"VIGENTE" CHAR(1 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table T_VEHICULO
--------------------------------------------------------

  CREATE TABLE "BDMAIPOGRANDE"."T_VEHICULO" 
   (	"IDVEHICULO" NUMBER(*,0), 
	"IDEMPRESA" NUMBER(*,0), 
	"PATENTE" VARCHAR2(6 BYTE), 
	"CLASE" VARCHAR2(2 BYTE), 
	"CAPACIDADKG" NUMBER(*,0), 
	"CMANCHO" NUMBER(*,0), 
	"CMALTO" NUMBER(*,0), 
	"CMLARGO" NUMBER(*,0), 
	"FRIGORIFICO" CHAR(1 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table T_VIAJE
--------------------------------------------------------

  CREATE TABLE "BDMAIPOGRANDE"."T_VIAJE" 
   (	"IDVIAJE" NUMBER(*,0), 
	"IDOFERTATRANSPORTE" NUMBER(*,0), 
	"IDREPRESENTANTE" NUMBER(*,0), 
	"FECHASALIDA" DATE, 
	"FECHALLEGADA" DATE, 
	"CIUDADACTUAL" VARCHAR2(30 BYTE), 
	"ESTADOVIAJE" VARCHAR2(20 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for View V_VIAJE
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "BDMAIPOGRANDE"."V_VIAJE" ("IDSUBASTA", "IDVIAJE", "NOMBRESUBASTA", "TRANSPORTISTA", "V_FECHASALIDA", "V_FECHALLEGADA", "V_CIUDADACTUAL", "V_ESTADOVIAJE", "COSTOTOTAL") AS 
  select  
        sub.IDSUBASTA,
        cos.IDVIAJE, 
        sub.nombresubasta as "NOMBRESUBASTA" ,
        emp.nombrecomercial as "TRANSPORTISTA" ,
        via.fechasalida as "V_FECHASALIDA",
        via.fechallegada as "V_FECHALLEGADA",
        via.ciudadactual as "V_CIUDADACTUAL",
        via.estadoviaje as "V_ESTADOVIAJE",

        TO_CHAR(sum(cos.VALOR+ofer.costotransporte), '$999999,99') as "COSTOTOTAL"
from t_costos_viaje cos
left join t_viaje via               on cos.idviaje = via.idviaje
left join t_oferta_transporte ofer  on via.idofertatransporte = ofer.idofertatransporte
left join t_subasta sub             on ofer.idsubasta = sub.idsubasta
left join t_vehiculo veh            on ofer.idvehiculo = veh.idvehiculo
left join t_empresa emp             on veh.idempresa = emp.idempresa


group by sub.idsubasta,cos.idviaje, sub.nombresubasta, emp.nombrecomercial, via.fechasalida, via.fechallegada, via.ciudadactual, via.estadoviaje

order by idsubasta asc
;
--------------------------------------------------------
--  DDL for Index T_PRODUCTO_PEDIDO_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "BDMAIPOGRANDE"."T_PRODUCTO_PEDIDO_PK" ON "BDMAIPOGRANDE"."T_PRODUCTO_PEDIDO" ("IDPRODUCTOPEDIDO") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index T_VEHICULO_SUBASTA_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "BDMAIPOGRANDE"."T_VEHICULO_SUBASTA_PK" ON "BDMAIPOGRANDE"."T_OFERTA_TRANSPORTE" ("IDOFERTATRANSPORTE") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index T_VEHICULO_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "BDMAIPOGRANDE"."T_VEHICULO_PK" ON "BDMAIPOGRANDE"."T_VEHICULO" ("IDVEHICULO") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index PRODUCTO_PRODUCTOR_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "BDMAIPOGRANDE"."PRODUCTO_PRODUCTOR_PK" ON "BDMAIPOGRANDE"."T_LOTE_OFRECIDO" ("IDLOTEOFRECIDO") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index TABLE_REPRESENTANTE_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "BDMAIPOGRANDE"."TABLE_REPRESENTANTE_PK" ON "BDMAIPOGRANDE"."T_REPRESENTANTE" ("IDREPRESENTANTE") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index T_USUARIO_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "BDMAIPOGRANDE"."T_USUARIO_PK" ON "BDMAIPOGRANDE"."T_USUARIO" ("IDUSUARIO") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index T_CONTRATO_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "BDMAIPOGRANDE"."T_CONTRATO_PK" ON "BDMAIPOGRANDE"."T_CONTRATO" ("IDCONTRATO") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index T_EMPRESA_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "BDMAIPOGRANDE"."T_EMPRESA_PK" ON "BDMAIPOGRANDE"."T_EMPRESA" ("IDEMPRESA") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index T_LOTE_PRODUCTO_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "BDMAIPOGRANDE"."T_LOTE_PRODUCTO_PK" ON "BDMAIPOGRANDE"."T_LOTE_PRODUCTO" ("IDLOTEPRODUCTO") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index T_VIAJE_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "BDMAIPOGRANDE"."T_VIAJE_PK" ON "BDMAIPOGRANDE"."T_VIAJE" ("IDVIAJE") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index T_PEDIDO_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "BDMAIPOGRANDE"."T_PEDIDO_PK" ON "BDMAIPOGRANDE"."T_PEDIDO" ("IDPEDIDO") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index T_UNIDAD_VENTA_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "BDMAIPOGRANDE"."T_UNIDAD_VENTA_PK" ON "BDMAIPOGRANDE"."T_UNIDAD_MEDICION" ("IDUNIDAD") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index T_SUBASTA_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "BDMAIPOGRANDE"."T_SUBASTA_PK" ON "BDMAIPOGRANDE"."T_SUBASTA" ("IDSUBASTA") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index PRODUCTO_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "BDMAIPOGRANDE"."PRODUCTO_PK" ON "BDMAIPOGRANDE"."T_PRODUCTO" ("IDPRODUCTO") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index T_USUARIO__IDX
--------------------------------------------------------

  CREATE UNIQUE INDEX "BDMAIPOGRANDE"."T_USUARIO__IDX" ON "BDMAIPOGRANDE"."T_USUARIO" ("IDREPRESENTANTE") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index T_COSTOS_VIAJE_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "BDMAIPOGRANDE"."T_COSTOS_VIAJE_PK" ON "BDMAIPOGRANDE"."T_COSTOS_VIAJE" ("IDCOSTOVIAJE") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Procedure ACTUALIZAR_CONTRATO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."ACTUALIZAR_CONTRATO" 
(
    nuevo_idcontrato in number,
    nuevo_idempresa in number,
    nuevo_fechainicio in date,
    nuevo_fechatermino in date,
    nuevo_rutadocumento in varchar2,
    nuevo_vigente in char    
)
AS
    VAR_CONTRATO number;
BEGIN
    VAR_CONTRATO:=0;
    SELECT COUNT(*) INTO VAR_CONTRATO FROM T_CONTRATO WHERE IDCONTRATO = nuevo_idcontrato;
    IF VAR_CONTRATO = 0 THEN
        UPDATE T_CONTRATO SET 
        idempresa = nuevo_idempresa, 
        fechainicio = nuevo_fechainicio,
        fechatermino = nuevo_fechatermino, 
        rutadocumento = nuevo_rutadocumento, 
        vigente = nuevo_vigente
        WHERE IDCONTRATO = nuevo_idcontrato;
    ELSE
        DBMS_OUTPUT.PUT_LINE('IMPOSIBLE CREAR USUARIO');
    END IF;
---AUTOR: MAXIMILIANO GONZ�LEZ-
END;

/
--------------------------------------------------------
--  DDL for Procedure ACTUALIZARCONTRATO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."ACTUALIZARCONTRATO" 
(
idcon in number, 
idem in number,
fechai in date,
fechat in date, 
rutdoc in varchar2,
vige in char
)
as
 vidc number:=idcon;
 vide number:=idem;
 vfi date:=fechai;
 vft date := fechat;
 vrd varchar2(50):=rutdoc;
 vvi char(1):=vige;
begin 
update T_CONTRATO set IDEMPRESA = vide , FECHAINICIO = vfi , FECHATERMINO = vft, RUTADOCUMENTO = vrd, VIGENTE=vvi  where IDCONTRATO = vidc;

exception 
when NO_DATA_FOUND then null;
when others then raise;
 End actualizarContrato;

/
--------------------------------------------------------
--  DDL for Procedure ACTUALIZAR_COSTOS_VIAJE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."ACTUALIZAR_COSTOS_VIAJE" 
(
    NUEVO_IDCOSTOVIAJE IN NUMBER,
    NUEVO_IDVIAJE IN NUMBER,
    NUEVO_TIPOCOSTO IN VARCHAR,
    NUEVO_OBSERVACIONES IN VARCHAR,
    NUEVO_VALOR IN NUMBER
)
AS VAR_COSTOS_VIAJE NUMBER;
BEGIN
    VAR_COSTOS_VIAJE:=0;
    SELECT COUNT(*) INTO VAR_COSTOS_VIAJE FROM T_COSTOS_VIAJE WHERE IDCOSTOVIAJE = NUEVO_IDCOSTOVIAJE;
    IF VAR_COSTOS_VIAJE = 0 THEN
       UPDATE T_COSTOS_VIAJE SET
      IDVIAJE = NUEVO_IDVIAJE, 
      TIPOCOSTO = NUEVO_TIPOCOSTO, 
      OBSERVACIONES = NUEVO_OBSERVACIONES, 
      VALOR = NUEVO_VALOR
       WHERE IDCOSTOVIAJE = NUEVO_IDCOSTOVIAJE;
    DBMS_OUTPUT.PUT_LINE('EL COSTO DE VIAJE HA SIDO ACTUALIZADO EXITOSAMENTE.');
        ELSE
        DBMS_OUTPUT.PUT_LINE('NO ES POSIBLE ACTUALIZAR EL COSTO DE VIAJE.');
        END IF;
    END ACTUALIZAR_COSTOS_VIAJE;

/
--------------------------------------------------------
--  DDL for Procedure ACTUALIZAR_EMPRESA
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."ACTUALIZAR_EMPRESA" 
(
    nuevo_idempresa IN NUMBER,
    nuevo_rutempresa IN varchar2,
    nuevo_tipoempresa IN VARCHAR2,
    nuevo_nombrecomercial IN VARCHAR2,
    nuevo_razonsocial IN VARCHAR2,
    nuevo_logo IN BLOB,
    nuevo_direccion IN VARCHAR2,
    nuevo_ciudad IN VARCHAR2,
    nuevo_pais IN VARCHAR2,
    nuevo_fono1 IN VARCHAR2,
    nuevo_fono2 IN VARCHAR2,
    nuevo_email IN VARCHAR2,
    nuevo_web IN VARCHAR2
)
AS
 RUTEMPRESA varchar2(15):= nuevo_rutempresa;
    TIPOEMPRESA varchar2(50):= nuevo_tipoempresa;
    NOMBRECOMERCIAL varchar2(30):= nuevo_nombrecomercial;
    RAZONSOCIAL varchar2(30):= nuevo_razonsocial;
    LOGO blob:= nuevo_logo;
    DIRECCION varchar2(60):= nuevo_direccion;
    CIUDAD varchar2(20):= nuevo_ciudad;
    PAIS varchar2(20):= nuevo_pais;
    FONO1 VARCHAR2(15) := nuevo_fono1;
    FONO2 VARCHAR2(15) := nuevo_fono2;
    EMAIL varchar2(60):= nuevo_email;
    WEB varchar2(60):= nuevo_web;
BEGIN
 
        UPDATE t_EMPRESA SET
 RUTEMPRESA = nuevo_rutempresa,
    TIPOEMPRESA = nuevo_tipoempresa,
    NOMBRECOMERCIAL = nuevo_nombrecomercial,
    RAZONSOCIAL = nuevo_razonsocial,
    LOGO = nuevo_logo,
    DIRECCION = nuevo_direccion,
    CIUDAD = nuevo_ciudad,
    PAIS = nuevo_pais,
    FONO1 = nuevo_fono1,
    FONO2 = nuevo_fono2,
    EMAIL = nuevo_email,
    WEB = nuevo_web
where IDEMPRESA = NUEVO_IDEMPRESA;
Exception
when NO_DATA_FOUND then
null;
when others then
 raise;
END ACTUALIZAR_EMPRESA;

/
--------------------------------------------------------
--  DDL for Procedure ACTUALIZAR_LOTE_OFRECIDO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."ACTUALIZAR_LOTE_OFRECIDO" 
(
    nuevo_id_lote_ofrecido in number,
    nuevo_id_producto_pedido in number,
    nuevo_id_lote_producto in number,
    nuevo_comentario in varchar2,
    nuevo_en_bodega in char,
    nuevo_cantidad_saldo in number,
    nuevo_id_representante in number,
    nuevo_vigente in char
)
AS
    IDPRODUCTOPEDIDO number:=nuevo_id_producto_pedido;
    IDLOTEPRODUCTO number:=nuevo_id_lote_producto;
    COMENTARIO varchar2(300):=nuevo_comentario;
    ENBODEGA char:=nuevo_en_bodega;
    CANTIDADSALDO number:=nuevo_cantidad_saldo;
    IDREPRESENTANTE number:=nuevo_id_representante;
    VIGENTE char:=nuevo_vigente;
BEGIN
        UPDATE t_lote_ofrecido SET
    
    IDPRODUCTOPEDIDO =nuevo_id_producto_pedido,
    IDLOTEPRODUCTO =nuevo_id_lote_producto,
    COMENTARIO =nuevo_comentario,
    ENBODEGA =nuevo_en_bodega,
    CANTIDADSALDO =nuevo_cantidad_saldo,
    IDREPRESENTANTE =nuevo_id_representante,
    VIGENTE =nuevo_vigente
WHERE IDLOTEOFRECIDO =nuevo_id_lote_ofrecido;
Exception
when NO_DATA_FOUND then
null;
when others then
 raise;
END ACTUALIZAR_LOTE_OFRECIDO;

/
--------------------------------------------------------
--  DDL for Procedure ACTUALIZAR_LOTE_PRODUCTO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."ACTUALIZAR_LOTE_PRODUCTO" 
(
    nuevo_idloteProducto in number,
    nuevo_idproducto in number,
    nuevo_idempresa in number,
    nuevo_idunidad in number,
    nuevo_cantidadproductos in number,
    nuevo_precioUnidad in float,
    nuevo_calidad in number,
    nuevo_fechavencimiento in date,
    nuevo_condicionesalmacenaje in varchar2,
    nuevo_descripcion in clob,
    nuevo_vigente in char,
    nuevo_ventaAlExtranjero in char
)
AS
    IDPRODUCTO number:=nuevo_idproducto;
    IDEMPRESA number:=nuevo_idempresa;
    IDUNIDAD number:=nuevo_idunidad;
    CANTIDADPRODUCTOS number:=nuevo_cantidadproductos;
    PRECIOUNIDAD float:=nuevo_precioUnidad;
    CALIDAD number:=nuevo_calidad;
    FECHAVENCIMIENTO date:=nuevo_fechavencimiento;
    CONDICIONESALMACENAJE varchar2(300):=nuevo_condicionesalmacenaje;
    DESCRIPCION clob:=nuevo_descripcion;
    VIGENTE char:=nuevo_vigente;
    VENTAALEXTRANJERO char:=nuevo_ventaAlExtranjero;
BEGIN
        UPDATE t_lote_producto SET
    
    IDPRODUCTO =nuevo_idproducto,
    IDEMPRESA =nuevo_idempresa,
    IDUNIDAD =nuevo_idunidad,
    CANTIDADPRODUCTOS =nuevo_cantidadproductos,
    PRECIOUNIDAD =nuevo_precioUnidad,
    CALIDAD =nuevo_calidad,
    FECHAVENCIMIENTO =nuevo_fechavencimiento,
    CONDICIONESALMACENAJE =nuevo_condicionesalmacenaje,
    DESCRIPCION =nuevo_descripcion,
    VIGENTE =nuevo_vigente,
    VENTAALEXTRANJERO =nuevo_ventaAlExtranjero
WHERE IDLOTEPRODUCTO =nuevo_idloteProducto;
Exception
when NO_DATA_FOUND then
null;
when others then
 raise;
END ACTUALIZAR_LOTE_PRODUCTO;

/
--------------------------------------------------------
--  DDL for Procedure ACTUALIZAR_OFERTA_TRANSPORTE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."ACTUALIZAR_OFERTA_TRANSPORTE" 
(
nuevo_id_oferta_transporte in number,
nuevo_id_subasta in number,
nuevo_id_vehiculo in number,
nuevo_id_representante in number,
nuevo_costo_transporte in number,
nuevo_vigente in char
)
IS
var_oferta_transporte number;
BEGIN
var_oferta_transporte:=0;
SELECT COUNT(*) INTO var_oferta_transporte FROM t_oferta_transporte WHERE idofertatransporte = nuevo_id_oferta_transporte;
 IF var_oferta_transporte = 0 THEN
UPDATE t_oferta_transporte SET
idofertatransporte = nuevo_id_oferta_transporte,
idsubasta = nuevo_id_subasta,
idvehiculo = nuevo_id_vehiculo,
idrepresentante = nuevo_id_representante, 
costotransporte = nuevo_costo_transporte,
vigente = nuevo_vigente
WHERE idofertatransporte = nuevo_id_oferta_transporte;
    ELSE
        DBMS_OUTPUT.PUT_LINE('IMPOSIBLE CREAR OFERTA TRANSPORTE');
    END IF;
---AUTOR: URIEL OLIVARES-
END;

/
--------------------------------------------------------
--  DDL for Procedure ACTUALIZAR_PEDIDO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."ACTUALIZAR_PEDIDO" 
(
    nuevo_idPedido in number,
    nuevo_idRepresentante in number,
    nuevo_rutaContrato in varchar2,
    nuevo_fechaInicioPedido in date,
    nuevo_fechaEntregaEstipulada in date,
    nuevo_fechaEntregaReal in date,
    nuevo_observacionInicial in clob,
    nuevo_estadoPedido in varchar2,
    nuevo_calidadEntrega in varchar2,
    nuevo_observacionEntrega in clob,
    nuevo_devuelto in char,
    nuevo_vigente in char,
    nuevo_direccion in varchar2,
    nuevo_fono in varchar2
)
AS
    IDREPRESENTANTE	NUMBER:=nuevo_idRepresentante;
    RUTACONTRATO	VARCHAR2(100):=nuevo_rutaContrato;
    FECHAINICIOPEDIDO	DATE:=nuevo_fechaInicioPedido;
    FECHAENTREGAESTIPULADA	DATE:=nuevo_fechaEntregaEstipulada;
    FECHAENTREGAREAL	DATE:=nuevo_fechaEntregaReal;
    OBSERVACIONINICIAL	CLOB:=nuevo_observacionInicial;
    ESTADOPEDIDO	VARCHAR2(30):=nuevo_estadoPedido;
    CALIDADENTREGA	VARCHAR2(15):=nuevo_calidadEntrega;
    OBSERVACIONENTREGA	CLOB:=nuevo_observacionEntrega;
    DEVUELTO	CHAR:=nuevo_devuelto;
    VIGENTE	CHAR:=nuevo_vigente;
    DIRECCION	VARCHAR2(60):=nuevo_direccion;
    FONO	VARCHAR2(15):=nuevo_fono;
BEGIN
        UPDATE t_pedido SET
    
    IDREPRESENTANTE	=nuevo_idRepresentante,
    RUTACONTRATO	=nuevo_rutaContrato,
    FECHAINICIOPEDIDO	=nuevo_fechaInicioPedido,
    FECHAENTREGAESTIPULADA	=nuevo_fechaEntregaEstipulada,
    FECHAENTREGAREAL	=nuevo_fechaEntregaReal,
    OBSERVACIONINICIAL	=nuevo_observacionInicial,
    ESTADOPEDIDO	=nuevo_estadoPedido,
    CALIDADENTREGA	=nuevo_calidadEntrega,
    OBSERVACIONENTREGA	=nuevo_observacionEntrega,
    DEVUELTO	=nuevo_devuelto,
    VIGENTE	=nuevo_vigente,
    DIRECCION	=nuevo_direccion,
    FONO	=nuevo_fono
WHERE IDPEDIDO =nuevo_idPedido;
Exception
when NO_DATA_FOUND then
null;
when others then
 raise;
END ACTUALIZAR_PEDIDO;

/
--------------------------------------------------------
--  DDL for Procedure ACTUALIZAR_PRODUCTO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."ACTUALIZAR_PRODUCTO" (
    new_idProducto IN NUMBER,
    new_nombre IN VARCHAR2,
    new_tipo IN VARCHAR2,
    new_foto in BLOB
)
IS
VAR_PRODUCTO NUMBER;
BEGIN
VAR_PRODUCTO:=0;
SELECT COUNT(*) INTO VAR_PRODUCTO FROM T_PRODUCTO WHERE idProducto = new_idProducto;
    IF VAR_PRODUCTO = 0 THEN
    UPDATE t_producto SET
    idproducto = new_idProducto,
    nombre = new_nombre,
    tipo = new_tipo,
    foto = new_foto
    WHERE idproducto = new_idProducto;
     ELSE
        DBMS_OUTPUT.PUT_LINE('NO SE LOGRADO MODIFICAR PRODUCTO');
    END IF;
-- ricardo
END ACTUALIZAR_PRODUCTO;

/
--------------------------------------------------------
--  DDL for Procedure ACTUALIZAR_PRODUCTO_PEDIDO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."ACTUALIZAR_PRODUCTO_PEDIDO" 
(
    NUEVO_IDPRODUCTOPEDIDO in number,
    NUEVO_IDPEDIDO in number,
    NUEVO_CANTIDAD in number,
    NUEVO_IDUNIDAD in number,
    NUEVO_IDPRODUCTO in number
)
AS
    VAR_PRODUCTO_PEDIDO NUMBER;
BEGIN
    VAR_PRODUCTO_PEDIDO:=0;
    SELECT COUNT(*) INTO VAR_PRODUCTO_PEDIDO  FROM T_PRODUCTO_PEDIDO WHERE IDPRODUCTOPEDIDO = NUEVO_IDPRODUCTOPEDIDO;
    IF VAR_PRODUCTO_PEDIDO = 0 THEN
        UPDATE  T_PRODUCTO_PEDIDO SET
IDPRODUCTOPEDIDO = NUEVO_IDPRODUCTOPEDIDO, 
IDPEDIDO = NUEVO_IDPEDIDO, 
CANTIDAD = NUEVO_CANTIDAD, 
IDUNIDAD = NUEVO_IDUNIDAD, 
IDPRODUCTO = NUEVO_IDPRODUCTO
WHERE IDPRODUCTOPEDIDO = NUEVO_IDPRODUCTOPEDIDO;
ELSE
DBMS_OUTPUT.PUT_LINE('NO ES POSIBLE ACTUALIZAR EL PRODUCTO PEDIDO.');
END IF;
END ACTUALIZAR_PRODUCTO_PEDIDO;

/
--------------------------------------------------------
--  DDL for Procedure ACTUALIZAR_REPRESENTANTE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."ACTUALIZAR_REPRESENTANTE" 
(
    nuevo_idrepresentante IN NUMBER,
    nuevo_idempresa IN NUMBER,
    nuevo_rutrepresentante IN VARCHAR2,
    nuevo_rolrepresentante IN VARCHAR2,
    nuevo_nombre IN VARCHAR2,
    nuevo_appaterno IN VARCHAR2,
    nuevo_apmaterno IN VARCHAR2,
    nuevo_sexo IN CHAR,
    nuevo_fechanacimiento IN DATE,
    nuevo_fono1 IN VARCHAR2,
    nuevo_fono2 IN VARCHAR2,
    nuevo_vigente IN CHAR
)
AS
idempresa number := nuevo_idempresa;
rutrepresentante VARCHAR2(9) := nuevo_rutrepresentante;
rolrepresentante VARCHAR2(20) := nuevo_rolrepresentante;
nombre VARCHAR2(60) := nuevo_nombre;
appaterno VARCHAR2(30) := nuevo_appaterno;
apmaterno VARCHAR2(30) := nuevo_apmaterno;
sexo char := nuevo_sexo;
fechanacimiento DATE := nuevo_fechanacimiento;
fono1 VARCHAR2(15) := nuevo_fono1;
fono2 VARCHAR2(15) := nuevo_fono2;
vigente char := nuevo_vigente;
BEGIN
 
        UPDATE T_REPRESENTANTE SET
idempresa = nuevo_idempresa,
rutrepresentante = nuevo_rutrepresentante,
rolrepresentante = nuevo_rolrepresentante,
nombre = nuevo_nombre,
appaterno = nuevo_appaterno,
apmaterno = nuevo_apmaterno,
sexo = nuevo_sexo,
fechanacimiento = nuevo_fechanacimiento,
fono1 = nuevo_fono1,
fono2 = nuevo_fono2,
vigente = nuevo_vigente
where idrepresentante = nuevo_idrepresentante;
Exception
when NO_DATA_FOUND then
null;
when others then
 raise;
END ACTUALIZAR_REPRESENTANTE;

/
--------------------------------------------------------
--  DDL for Procedure ACTUALIZAR_SUBASTA
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."ACTUALIZAR_SUBASTA" 
(
    nuevo_idsubasta in number,
    nuevo_nombresubasta in varchar2,
    nuevo_idpedido in number,
    nuevo_fechapublicacion in date,
    nuevo_fechatermino in date,
    nuevo_idrepresentante number
)
AS
    IDSUBASTA NUMBER := NUEVO_IDSUBASTA;
    NOMBRESUBASTA VARCHAR2(60):= NUEVO_NOMBRESUBASTA;
    IDPEDIDO NUMBER:= NUEVO_IDPEDIDO;
    FECHAPUBLICACION DATE := NUEVO_FECHAPUBLICACION;
    FECHATERMINO DATE := NUEVO_FECHATERMINO;
    IDREPRESENTANTE NUMBER:= NUEVO_IDREPRESENTANTE;
BEGIN
    UPDATE T_SUBASTA SET
    NOMBRESUBASTA = NUEVO_NOMBRESUBASTA,
    IDPEDIDO = NUEVO_IDPEDIDO,
    FECHAPUBLICACION = NUEVO_FECHAPUBLICACION,
    FECHATERMINO = NUEVO_FECHATERMINO,
    IDREPRESENTANTE = NUEVO_IDREPRESENTANTE
WHERE IDSUBASTA = NUEVO_IDSUBASTA;
EXCEPTION
WHEN NO_DATA_FOUND THEN
NULL;
WHEN OTHERS THEN
RAISE;
END ACTUALIZAR_SUBASTA;

/
--------------------------------------------------------
--  DDL for Procedure ACTUALIZAR_UNIDAD_MEDICION
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."ACTUALIZAR_UNIDAD_MEDICION" 
(
    nuevo_idUnidad in number,
    nuevo_descripcion in varchar2
)
AS
    VAR_UNIDAD number;
BEGIN
    VAR_UNIDAD:=0;
    SELECT COUNT(*) INTO VAR_UNIDAD FROM T_UNIDAD_MEDICION WHERE idUnidad = nuevo_idUnidad;
    IF VAR_UNIDAD = 0 THEN
        UPDATE T_UNIDAD_MEDICION SET 
        descripcion = nuevo_descripcion 
        WHERE idUnidad = nuevo_idUnidad;
    ELSE
        DBMS_OUTPUT.PUT_LINE('IMPOSIBLE CREAR USUARIO');
    END IF;
---AUTOR: CRISTI�N ESQUIVEL--
END ACTUALIZAR_UNIDAD_MEDICION;

/
--------------------------------------------------------
--  DDL for Procedure ACTUALIZAR_USUARIO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."ACTUALIZAR_USUARIO" 
(
    nuevo_idusuario in number,
    nuevo_email in varchar2,
    nueva_contrasena in varchar2,
    nuevo_idrepresentante in number,
    nuevo_vigente in char
)
AS
EMAIL varchar2(60):= nuevo_email;
CONTRASENA varchar2(100):= nueva_contrasena;
IDREPRESENTANTE number := nuevo_idrepresentante;
VIGENTE char := nuevo_vigente;
BEGIN
 
        UPDATE t_usuario SET
EMAIL = nuevo_email,
CONTRASENA = nueva_contrasena,
IDREPRESENTANTE = nuevo_idrepresentante,
VIGENTE = nuevo_vigente 
WHERE IDUSUARIO = nuevo_idusuario; 
Exception
when NO_DATA_FOUND then
null;
when others then
 raise;
END ACTUALIZAR_USUARIO;

/
--------------------------------------------------------
--  DDL for Procedure ACTUALIZAR_VEHICULO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."ACTUALIZAR_VEHICULO" 
(
    nuevo_idvehiculo in number,
    nuevo_idempresa in number,
    nuevo_patente in varchar2,
    nuevo_clase in varchar2,
    nuevo_capacidadkg in number,
    nuevo_cmancho in number,
    nuevo_cmalto in number,
    nuevo_cmlargo in number,
    nuevo_frigorifico in char
)
AS
    VAR_VEHICULO number;
BEGIN
    VAR_VEHICULO:=0;
    SELECT COUNT(*) INTO VAR_VEHICULO FROM T_VEHICULO WHERE IDVEHICULO = nuevo_idvehiculo;
    IF VAR_VEHICULO != 0 THEN
    UPDATE T_VEHICULO SET
    IDVEHICULO = nuevo_idvehiculo,
    IDEMPRESA = nuevo_idempresa,
    PATENTE = nuevo_patente,
    CLASE = nuevo_clase,
    CAPACIDADKG = nuevo_capacidadkg,
    CMANCHO = nuevo_cmancho,
    CMALTO = nuevo_cmalto,
    CMLARGO = nuevo_cmlargo,
    FRIGORIFICO = nuevo_frigorifico;
    ELSE
        DBMS_OUTPUT.PUT_LINE('IMPOSIBLE ACTUALIZAR VEHICULO');
    END IF;
---AUTOR: MAXIMILIANO GONZ�LEZ-
END ACTUALIZAR_VEHICULO;

/
--------------------------------------------------------
--  DDL for Procedure ACTUALIZAR_VIAJE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."ACTUALIZAR_VIAJE" 
(
    NUEVO_IDVIAJE IN NUMBER,
    NUEVO_IDOFERTATRANSPORTE IN NUMBER,
    NUEVO_IDREPRESENTANTE IN NUMBER,
    NUEVO_FECHASALIDA IN DATE,
    NUEVO_FECHALLEGADA IN DATE,
    NUEVO_CIUDADACTUAL VARCHAR2,
    NUEVO_ESTADOVIAJE VARCHAR2
)
IS
VAR_VIAJE number;
BEGIN
VAR_VIAJE:=0;
SELECT COUNT(*) INTO VAR_VIAJE FROM T_VIAJE WHERE IDVIAJE = NUEVO_IDVIAJE;
   if VAR_VIAJE = 0 THEN
    UPDATE t_VIAJE SET 
    IDOFERTATRANSPORTE = NUEVO_IDOFERTATRANSPORTE,
    IDREPRESENTANTE = NUEVO_IDREPRESENTANTE,
    FECHASALIDA = NUEVO_FECHASALIDA,
    FECHALLEGADA = NUEVO_FECHALLEGADA,
    CIUDADACTUAL = NUEVO_CIUDADACTUAL,
    ESTADOVIAJE = NUEVO_ESTADOVIAJE
    WHERE IDVIAJE = NUEVO_IDVIAJE;
     ELSE
        DBMS_OUTPUT.PUT_LINE('NO SE LOGRADO MODIFICAR VIAJE');
    END IF;
-- Autor: Juan Roman
END ACTUALIZAR_viaje;

/
--------------------------------------------------------
--  DDL for Procedure CREAR_CONTRATO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."CREAR_CONTRATO" 
(
    nuevo_idcontrato in number,
    nuevo_idempresa in number,
    nuevo_fechainicio in date,
    nuevo_fechatermino in date,
    nuevo_rutadocumento in varchar2,
    nuevo_vigente in char    
)
AS
    VAR_CONTRATO number;
BEGIN
    VAR_CONTRATO:=0;
    SELECT COUNT(*) INTO VAR_CONTRATO FROM T_CONTRATO WHERE IDCONTRATO = nuevo_idcontrato;
    IF VAR_CONTRATO = 0 THEN
        INSERT INTO T_CONTRATO VALUES(SEQ_CONTRATO.nextval, nuevo_idempresa, nuevo_fechainicio,
        nuevo_fechatermino, nuevo_rutadocumento, nuevo_vigente);
    ELSE
        DBMS_OUTPUT.PUT_LINE('IMPOSIBLE CREAR USUARIO');
    END IF;
---AUTOR: MAXIMILIANO GONZ�LEZ-
END CREAR_CONTRATO;

/
--------------------------------------------------------
--  DDL for Procedure CREAR_COSTOS_VIAJE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."CREAR_COSTOS_VIAJE" 
(
    NUEVO_IDCOSTOVIAJE IN NUMBER,
    NUEVO_IDVIAJE IN NUMBER,
    NUEVO_TIPOCOSTO IN VARCHAR,
    NUEVO_OBSERVACIONES IN VARCHAR,
    NUEVO_VALOR IN NUMBER
)
AS VAR_COSTOS_VIAJE NUMBER;
BEGIN
    VAR_COSTOS_VIAJE:=0;
    SELECT COUNT(*) INTO VAR_COSTOS_VIAJE FROM T_COSTOS_VIAJE WHERE IDCOSTOVIAJE = NUEVO_IDCOSTOVIAJE;
    IF VAR_COSTOS_VIAJE = 0 THEN
    INSERT INTO T_COSTOS_VIAJE VALUES(SEQ_COSTOS_VIAJE.nextval, NUEVO_IDVIAJE, NUEVO_TIPOCOSTO, 
    NUEVO_OBSERVACIONES, NUEVO_VALOR);
    DBMS_OUTPUT.PUT_LINE('EL COSTO DE VIAJE HA SIDO CREADO EXITOSAMENTE.');
        ELSE
        DBMS_OUTPUT.PUT_LINE('NO ES POSIBLE CREAR EL COSTO DE VIAJE.');
        END IF;
    END CREAR_COSTOS_VIAJE;

/
--------------------------------------------------------
--  DDL for Procedure CREAR_EMPRESA
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."CREAR_EMPRESA" (
    nuevo_idempresa IN NUMBER,
    nuevo_rutempresa IN VARCHAR2,
    nuevo_tipoempresa IN VARCHAR2,
    nuevo_nombrecomercial IN VARCHAR2,
    nuevo_razonsocial IN VARCHAR2,
    nuevo_logo IN BLOB,
    nuevo_direccion IN VARCHAR2,
    nuevo_ciudad IN VARCHAR2,
    nuevo_pais IN VARCHAR2,
    nuevo_fono1 IN VARCHAR2,
    nuevo_fono2 IN VARCHAR2,
    nuevo_email IN VARCHAR2,
    nuevo_web IN VARCHAR2
    )
IS
id_nuevaempresa number;
BEGIN
    id_nuevaempresa :=0;
    SELECT COUNT(*) INTO id_nuevaempresa FROM T_EMPRESA WHERE IDEMPRESA = nuevo_idempresa;

    IF id_nuevaempresa = 0 THEN
        INSERT INTO T_EMPRESA VALUES(SEQ_EMPRESA.nextval,nuevo_rutempresa,nuevo_tipoempresa,nuevo_nombrecomercial,
        nuevo_razonsocial,nuevo_logo, nuevo_direccion,nuevo_ciudad,nuevo_pais,nuevo_fono1,nuevo_fono2,nuevo_email,
        nuevo_web);
    END IF;
     ---MAXIMILIANO GONZ�LEZ--
END;

/
--------------------------------------------------------
--  DDL for Procedure CREAR_LOTE_OFRECIDO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."CREAR_LOTE_OFRECIDO" 
(
nuevo_id_lote_ofrecido in number,
nuevo_id_producto_pedido in number,
nuevo_id_lote_producto in number,
nuevo_comentario in varchar2,
nuevo_en_bodega in char,
nuevo_cantidad_saldo in number,
nuevo_id_representante in number,
nuevo_vigente in char
)
AS
    VAR_LOTE_OFRECIDO number;
BEGIN
    VAR_LOTE_OFRECIDO:=0;
    SELECT COUNT(*) INTO VAR_LOTE_OFRECIDO FROM T_LOTE_OFRECIDO WHERE idloteofrecido = nuevo_id_lote_ofrecido;
    IF VAR_LOTE_OFRECIDO = 0 THEN
        INSERT INTO T_LOTE_OFRECIDO VALUES(SEQ_LOTE_OFRECIDO.nextval,nuevo_id_producto_pedido,nuevo_id_lote_producto,nuevo_comentario,nuevo_en_bodega,nuevo_cantidad_saldo,nuevo_id_representante,nuevo_vigente);
    ELSE
        DBMS_OUTPUT.PUT_LINE('IMPOSIBLE CREAR LOTE OFRECIDO');
    END IF;
---AUTOR: JUAN ROMAN
END CREAR_LOTE_OFRECIDO;

/
--------------------------------------------------------
--  DDL for Procedure CREAR_LOTE_PRODUCTO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."CREAR_LOTE_PRODUCTO" 
(
    nuevo_idloteProducto in number,
    nuevo_idproducto in number,
    nuevo_idempresa in number,
    nuevo_idunidad in number,
    nuevo_cantidadproductos in number,
    nuevo_precioUnidad in float,
    nuevo_calidad in number,
    nuevo_fechavencimiento in date,
    nuevo_condicionesalmacenaje in varchar2,
    nuevo_descripcion in clob,
    nuevo_vigente in char,
    nuevo_ventaAlExtranjero in char
)
AS
    VAR_LOTE_PRODUCTO number;
BEGIN
    VAR_LOTE_PRODUCTO:=0;
    SELECT COUNT(*) INTO VAR_LOTE_PRODUCTO FROM T_LOTE_PRODUCTO WHERE IDLOTEPRODUCTO = nuevo_idloteProducto;
    IF VAR_LOTE_PRODUCTO = 0 THEN
        INSERT INTO T_LOTE_PRODUCTO VALUES(SEQ_LOTE_PRODUCTO.nextval,nuevo_idproducto,nuevo_idempresa,
        nuevo_idunidad,nuevo_cantidadproductos,nuevo_precioUnidad,nuevo_calidad,nuevo_fechavencimiento,
        nuevo_condicionesalmacenaje, nuevo_descripcion, nuevo_vigente, nuevo_ventaalextranjero);
    ELSE
        DBMS_OUTPUT.PUT_LINE('IMPOSIBLE CREAR LOTE PRODUCTO');
    END IF;
---AUTOR: JUAN ROMAN-
END CREAR_LOTE_PRODUCTO;

/
--------------------------------------------------------
--  DDL for Procedure CREAR_OFERTA_TRANSPORTE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."CREAR_OFERTA_TRANSPORTE" 
(
nuevo_id_oferta_transporte in number,
nuevo_id_subasta in number,
nuevo_id_vehiculo in number,
nuevo_id_representante in number,
nuevo_costo_transporte in number,
nuevo_vigente in char
)
is
--declaracionvariable--
var_oferta_transporte number;
BEGIN
 var_oferta_transporte:=0;
 SELECT COUNT(*) INTO var_oferta_transporte FROM t_oferta_transporte WHERE idofertatransporte = nuevo_id_oferta_transporte;

 IF var_oferta_transporte =0 THEN 
    INSERT INTO t_oferta_transporte VALUES(SEQ_OFERTA_TRANSPORTE.nextval ,nuevo_id_subasta,
                nuevo_id_vehiculo, nuevo_id_representante, nuevo_costo_transporte,nuevo_vigente);
 ELSE
        DBMS_OUTPUT.PUT_LINE('IMPOSIBLE CREAR OFERTA TRANSPORTE');
 END IF;
 ---URIEL OLIVARES--
END;

/
--------------------------------------------------------
--  DDL for Procedure CREAR_PEDIDO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."CREAR_PEDIDO" 
(
    nuevo_idPedido in number,
    nuevo_idRepresentante in number,
    nuevo_rutaContrato in varchar2,
    nuevo_fechaInicioPedido in date,
    nuevo_fechaEntregaEstipulada in date,
    nuevo_fechaEntregaReal in date,
    nuevo_observacionInicial in clob,
    nuevo_estadoPedido in varchar2,
    nuevo_calidadEntrega in varchar2,
    nuevo_observacionEntrega in clob,
    nuevo_devuelto in char,
    nuevo_vigente in char,
    nuevo_direccion in varchar2,
    nuevo_fono in varchar2
)
AS
    VAR_PEDIDO number;
BEGIN
    VAR_PEDIDO :=0;
    SELECT COUNT(*) INTO VAR_PEDIDO  FROM T_PEDIDO WHERE IDPEDIDO = nuevo_idPedido;
    IF VAR_PEDIDO = 0 THEN
        INSERT INTO T_PEDIDO VALUES(
	SEQ_PEDIDO.nextval,
	nuevo_idRepresentante,
	nuevo_rutaContrato,
	nuevo_fechaInicioPedido,
	nuevo_fechaEntregaEstipulada,
	nuevo_fechaEntregaReal,
	nuevo_observacionInicial,
	nuevo_estadoPedido,
    nuevo_calidadEntrega,
	nuevo_observacionEntrega,
	nuevo_devuelto,
	nuevo_vigente,
    nuevo_direccion,
    nuevo_fono);
    ELSE
        DBMS_OUTPUT.PUT_LINE('IMPOSIBLE CREAR PEDIDO');
    END IF;
---AUTOR: CRISTI�N ESQUIVEL-
END CREAR_PEDIDO;

/
--------------------------------------------------------
--  DDL for Procedure CREAR_PRODUCTO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."CREAR_PRODUCTO" (
    new_idProducto IN NUMBER,
    new_nombre IN VARCHAR2,
    new_tipo IN VARCHAR2,
    new_foto in BLOB
    )
IS
nuevoProducto number;
BEGIN
    nuevoProducto :=0;
    SELECT COUNT(*) INTO nuevoProducto FROM t_producto WHERE idproducto = new_idProducto;

    IF nuevoProducto = 0 THEN
        INSERT INTO t_producto VALUES(SEQ_PRODUCTO.nextval,new_nombre,new_tipo,new_foto );
    END IF;
     --Ricardo--
END;

/
--------------------------------------------------------
--  DDL for Procedure CREAR_PRODUCTO_PEDIDO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."CREAR_PRODUCTO_PEDIDO" 
(
    NUEVO_IDPRODUCTOPEDIDO in number,
    NUEVO_IDPEDIDO in number,
    NUEVO_CANTIDAD in number,
    NUEVO_IDUNIDAD in number,
    NUEVO_IDPRODUCTO in number
)
AS VAR_PRODUCTO_PEDIDO NUMBER;
BEGIN
    VAR_PRODUCTO_PEDIDO:=0;
    SELECT COUNT(*) INTO VAR_PRODUCTO_PEDIDO FROM T_PRODUCTO_PEDIDO WHERE IDPRODUCTOPEDIDO = NUEVO_IDPRODUCTOPEDIDO;
    IF VAR_PRODUCTO_PEDIDO = 0 THEN
        INSERT INTO T_PRODUCTO_PEDIDO VALUES(SEQ_PRODUCTO_PEDIDO.nextval, NUEVO_IDPEDIDO, NUEVO_CANTIDAD, NUEVO_IDUNIDAD, NUEVO_IDPRODUCTO);
        DBMS_OUTPUT.PUT_LINE('EL PRODUCTO PEDIDO HA SIDO CREADO EXITOSAMENTE.');
        ELSE
        DBMS_OUTPUT.PUT_LINE('NO ES POSIBLE CREAR EL PRODUCTO PEDIDO.');
        END IF;
    END CREAR_PRODUCTO_PEDIDO;

/
--------------------------------------------------------
--  DDL for Procedure CREAR_REPRESENTANTE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."CREAR_REPRESENTANTE" 
(
    nuevo_idrepresentante IN NUMBER,
    nuevo_idempresa IN NUMBER,
    nuevo_rutrepresentante IN VARCHAR2,
    nuevo_rolrepresentante IN VARCHAR2,
    nuevo_nombre IN VARCHAR2,
    nuevo_appaterno IN VARCHAR2,
    nuevo_apmaterno IN VARCHAR2,
    nuevo_sexo IN CHAR,
    nuevo_fechanacimiento IN DATE,
    nuevo_fono1 IN VARCHAR2,
    nuevo_fono2 IN VARCHAR2,
    nuevo_vigente IN CHAR
)
IS
    id_rep number;
BEGIN
    id_rep:=0;
    SELECT COUNT(*) INTO id_rep FROM T_REPRESENTANTE WHERE IDREPRESENTANTE = nuevo_idrepresentante;

    IF id_rep = 0 THEN

        INSERT INTO T_REPRESENTANTE VALUES(SEQ_REPRESENTANTE.nextval,nuevo_idempresa,
        nuevo_rutrepresentante,nuevo_rolrepresentante,nuevo_nombre,nuevo_appaterno,
        nuevo_apmaterno,nuevo_sexo,nuevo_fechanacimiento,
        nuevo_fono1,nuevo_fono2,nuevo_vigente);
    END IF;
     ---Autor: MAXIMILIANO GONZ�LEZ--
END;

/
--------------------------------------------------------
--  DDL for Procedure CREAR_SUBASTA
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."CREAR_SUBASTA" 
(
    nuevo_idsubasta in number,
    nuevo_nombresubasta in varchar2,
    nuevo_idpedido in number,
    nuevo_fechapublicacion in date,
    nuevo_fechatermino in date,
    nuevo_idrepresentante number
)
AS
    VAR_SUBASTA number;
BEGIN
    VAR_SUBASTA:=0;
    SELECT COUNT(*) INTO VAR_SUBASTA FROM T_SUBASTA WHERE IDSUBASTA = nuevo_idsubasta;
    IF VAR_SUBASTA = 0 THEN
        INSERT INTO T_SUBASTA VALUES (SEQ_SUBASTA.nextval,nuevo_nombresubasta,nuevo_idpedido,
    nuevo_fechapublicacion,nuevo_fechatermino,nuevo_idrepresentante);
    ELSE
        DBMS_OUTPUT.PUT_LINE('IMPOSIBLE CREAR SUBASTA');
    END IF;
---AUTOR: JUAN ROMAN-
END CREAR_SUBASTA;

/
--------------------------------------------------------
--  DDL for Procedure CREAR_UNIDAD_MEDICION
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."CREAR_UNIDAD_MEDICION" 
(
    nuevo_idUnidad in number,
    nuevo_descripcion in varchar2
)
AS
    VAR_UNIDAD number;
BEGIN
    VAR_UNIDAD :=0;
    SELECT COUNT(*) INTO VAR_UNIDAD  FROM T_UNIDAD_MEDICION WHERE idUnidad = nuevo_idUnidad;
    IF VAR_UNIDAD = 0 THEN
        INSERT INTO T_UNIDAD_MEDICION VALUES(
	SEQ_UNIDAD_MEDICION.nextval,
        nuevo_descripcion);
    ELSE
        DBMS_OUTPUT.PUT_LINE('IMPOSIBLE CREAR UNIDAD DE MEDICI�N');
    END IF;
---AUTOR: CRISTI�N ESQUIVEL--
END CREAR_UNIDAD_MEDICION;

/
--------------------------------------------------------
--  DDL for Procedure CREAR_USUARIO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."CREAR_USUARIO" 
(
nuevo_usuario in number,
nuevo_email in varchar2,
nueva_contrasena in varchar2,
nuevo_idrepresentante in number,
nuevo_vigente in char
)
is
--declaracionvariable--
id_user number;
BEGIN
 id_user:=0;
 SELECT COUNT(*) INTO id_user FROM t_usuario WHERE idusuario = nuevo_usuario;

 IF id_user =0 THEN 
    INSERT INTO T_USUARIO VALUES(SEQ_USUARIO.nextval ,nuevo_email, nueva_contrasena ,nuevo_idrepresentante,
    nuevo_vigente);
 END IF;
 ---MAXIMILIANO GONZ�LEZ--
END;

/
--------------------------------------------------------
--  DDL for Procedure CREAR_VEHICULO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."CREAR_VEHICULO" 
(
    nuevo_idvehiculo in number,
    nuevo_idempresa in number,
    nuevo_patente in varchar2,
    nuevo_clase in varchar2,
    nuevo_capacidadkg in number,
    nuevo_cmancho in number,
    nuevo_cmalto in number,
    nuevo_cmlargo in number,
    nuevo_frigorifico in char
)
AS
    VAR_VEHICULO number;
BEGIN
    VAR_VEHICULO:=0;
    SELECT COUNT(*) INTO VAR_VEHICULO FROM T_VEHICULO WHERE IDVEHICULO = nuevo_idvehiculo;
    IF VAR_VEHICULO = 0 THEN
        INSERT INTO T_VEHICULO VALUES(SEQ_VEHICULO.nextval, nuevo_idempresa, nuevo_patente,
        nuevo_clase, nuevo_capacidadkg, nuevo_cmancho, nuevo_cmalto,nuevo_cmlargo,nuevo_frigorifico);
    ELSE
        DBMS_OUTPUT.PUT_LINE('IMPOSIBLE CREAR VEHICULO');
    END IF;
---AUTOR: MAXIMILIANO GONZ�LEZ-
END CREAR_VEHICULO;

/
--------------------------------------------------------
--  DDL for Procedure CREAR_VIAJE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."CREAR_VIAJE" (
    NUEVO_IDVIAJE IN NUMBER,
    NUEVO_IDOFERTATRANSPORTE IN NUMBER,
    NUEVO_IDREPRESENTANTE IN NUMBER,
    NUEVO_FECHASALIDA IN DATE,
    NUEVO_FECHALLEGADA IN DATE,
    NUEVO_CIUDADACTUAL VARCHAR2,
    NUEVO_ESTADOVIAJE VARCHAR2
    )
IS
VAR_VIAJE number;
BEGIN
    VAR_VIAJE :=0;
    SELECT COUNT(*) INTO VAR_VIAJE FROM T_VIAJE WHERE IDVIAJE = NUEVO_IDVIAJE;

    IF VAR_VIAJE = 0 THEN
        INSERT INTO T_VIAJE VALUES(SEQ_VIAJE.nextval,NUEVO_IDOFERTATRANSPORTE,NUEVO_IDREPRESENTANTE,
        NUEVO_FECHASALIDA,NUEVO_FECHALLEGADA,NUEVO_CIUDADACTUAL,NUEVO_ESTADOVIAJE);
    END IF;
---Autor: Juan Roman---
END CREAR_VIAJE;

/
--------------------------------------------------------
--  DDL for Procedure ELIMINAR_CONTRATO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."ELIMINAR_CONTRATO" (ID_CONTRATO NUMBER)
AS
BORRADO NUMBER;
BEGIN 
    DELETE FROM T_CONTRATO WHERE IDCONTRATO = ID_CONTRATO;
    BORRADO := SQL%ROWCOUNT;
    IF BORRADO = 0 THEN
        DBMS_OUTPUT.PUT_LINE('ID CONTRATO: ' || ID_CONTRATO || ' NO ENCONTRADO');
    ELSE        
        DBMS_OUTPUT.PUT_LINE('SE HA BORRADO EL CONTRATO ID: ' || ID_CONTRATO);
    END IF;
-- AUTOR: MAXIMILIANO GONZ�LEZ --
END ELIMINAR_CONTRATO;

/
--------------------------------------------------------
--  DDL for Procedure ELIMINARCONTRATO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."ELIMINARCONTRATO" (idcon in number)
as
vidcon number:=idcon;
begin 
delete from T_CONTRATO where idcontrato = vidcon;
end;

/
--------------------------------------------------------
--  DDL for Procedure ELIMINAR_COSTOS_VIAJE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."ELIMINAR_COSTOS_VIAJE" (ID_COSTOVIAJE NUMBER)
AS
BORRADO NUMBER;
BEGIN
    DELETE FROM T_COSTOS_VIAJE WHERE IDCOSTOVIAJE = ID_COSTOVIAJE;
    BORRADO := SQL%ROWCOUNT;
    IF BORRADO = 0 THEN
    DBMS_OUTPUT.PUT_LINE('ID COSTOVIAJE: '|| ID_COSTOVIAJE || 'NO ENCONTRADO');
    ELSE
    DBMS_OUTPUT.PUT_LINE('SE HA BORRADO EL COSTO VIAJE CON ID: ' || ID_COSTOVIAJE);
    END IF;
END ELIMINAR_COSTOS_VIAJE;

/
--------------------------------------------------------
--  DDL for Procedure ELIMINAR_EMPRESA
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."ELIMINAR_EMPRESA" (ID_EMPRESA NUMBER)
AS
BORRADO NUMBER;
BEGIN 
    DELETE FROM T_EMPRESA WHERE IDEMPRESA = ID_EMPRESA;
    BORRADO := SQL%ROWCOUNT;
    IF BORRADO = 0 THEN
        DBMS_OUTPUT.PUT_LINE('EMPRESA' || ID_EMPRESA || 'NO ENCONTRADO');
    ELSE        
        DBMS_OUTPUT.PUT_LINE('SE HA BORRADO LA EMPRESA' || ID_EMPRESA);
    END IF;
END ELIMINAR_EMPRESA;

/
--------------------------------------------------------
--  DDL for Procedure ELIMINAR_LOTE_OFRECIDO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."ELIMINAR_LOTE_OFRECIDO" (id_lote_ofrecido NUMBER)
AS
BORRADO NUMBER;
BEGIN 
    DELETE FROM t_lote_ofrecido WHERE IDLOTEOFRECIDO = id_lote_ofrecido;
    BORRADO := SQL%ROWCOUNT;
    IF BORRADO = 0 THEN
        DBMS_OUTPUT.PUT_LINE('ID LOTE OFRECIDO: ' || id_lote_ofrecido || ' NO ENCONTRADO');
    ELSE        
        DBMS_OUTPUT.PUT_LINE('SE HA BORRADO EL LOTE OFRECIDO ID: ' || id_lote_ofrecido);
    END IF;
-- AUTOR: URIEL OLVIARES --
END ELIMINAR_LOTE_OFRECIDO;

/
--------------------------------------------------------
--  DDL for Procedure ELIMINAR_LOTE_PRODUCTO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."ELIMINAR_LOTE_PRODUCTO" (ID_LOTE_PRODUCTO NUMBER)
AS
BORRADO NUMBER;
BEGIN 
    DELETE FROM T_LOTE_PRODUCTO WHERE IDLOTEPRODUCTO = ID_LOTE_PRODUCTO;
    BORRADO := SQL%ROWCOUNT;
    IF BORRADO = 0 THEN
        DBMS_OUTPUT.PUT_LINE('ID LOTE_PRODUCTO: ' || ID_LOTE_PRODUCTO || ' NO ENCONTRADO');
    ELSE        
        DBMS_OUTPUT.PUT_LINE('SE HA BORRADO EL LOTE_PRODUCTO ID: ' || ID_LOTE_PRODUCTO);
    END IF;
---AUTOR: JUAN ROMAN-
END ELIMINAR_LOTE_PRODUCTO;

/
--------------------------------------------------------
--  DDL for Procedure ELIMINAR_OFERTA_TRANSPORTE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."ELIMINAR_OFERTA_TRANSPORTE" (ID_OFERTA_TRANSPORTE NUMBER)
AS
BORRADO NUMBER;
BEGIN 
    DELETE FROM T_OFERTA_TRANSPORTE WHERE IDOFERTATRANSPORTE = ID_OFERTA_TRANSPORTE;
    BORRADO := SQL%ROWCOUNT;
    IF BORRADO = 0 THEN
        DBMS_OUTPUT.PUT_LINE('ID OFERTA TRANSPORTE: '||ID_OFERTA_TRANSPORTE||' NO ENCONTRADO');
    ELSE
        DBMS_OUTPUT.PUT_LINE('SE HA BORRADO LA OFERTA TRANSPORTE ID: ' || ID_OFERTA_TRANSPORTE);
    END IF;
-- AUTOR: URIEL OLVIARES --
END ELIMINAR_OFERTA_TRANSPORTE;

/
--------------------------------------------------------
--  DDL for Procedure ELIMINAR_PEDIDO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."ELIMINAR_PEDIDO" (ID_PEDIDO NUMBER)
AS
BORRADO NUMBER;
BEGIN 
    DELETE FROM T_PEDIDO WHERE idPedido = ID_PEDIDO;
    BORRADO := SQL%ROWCOUNT;
    IF BORRADO = 0 THEN
        DBMS_OUTPUT.PUT_LINE('EL ID DEL PEDIDO: ' || ID_PEDIDO || ' NO ENCONTRADO');
    ELSE        
        DBMS_OUTPUT.PUT_LINE('SE HA ELIMINADO PEDIDO CON ID: ' || ID_PEDIDO);
    END IF;
-- AUTOR: CRISTI�N ESQUIVEL --
END ELIMINAR_PEDIDO;

/
--------------------------------------------------------
--  DDL for Procedure ELIMINAR_PRODUCTO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."ELIMINAR_PRODUCTO" (id_Producto NUMBER)
AS
borrar NUMBER;
BEGIN 
    DELETE FROM t_producto WHERE idproducto = id_producto;
    borrar := SQL%ROWCOUNT;
    IF borrar = 0 THEN
        DBMS_OUTPUT.PUT_LINE('ID producto: '||  id_producto || ' NO ENCONTRADO');
    ELSE
        DBMS_OUTPUT.PUT_LINE('SE HA BORRADO EL producto : '||  id_producto);
    END IF;

END ELIMINAR_producto;

/
--------------------------------------------------------
--  DDL for Procedure ELIMINAR_PRODUCTO_PEDIDO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."ELIMINAR_PRODUCTO_PEDIDO" (ID_PRODUCTOPEDIDO NUMBER)
AS
BORRADO NUMBER;
BEGIN
    DELETE FROM T_PRODUCTO_PEDIDO WHERE IDPRODUCTOPEDIDO = ID_PRODUCTOPEDIDO;
    BORRADO := SQL%ROWCOUNT;
    IF BORRADO = 0 THEN
    DBMS_OUTPUT.PUT_LINE('ID PRODUCTO PEDIDO: '|| ID_PRODUCTOPEDIDO || 'NO ENCONTRADO');
    ELSE
    DBMS_OUTPUT.PUT_LINE('SE HA BORRADO EL PRODUCTO PEDIDO CON ID: ' || ID_PRODUCTOPEDIDO);
    END IF;
END ELIMINAR_PRODUCTO_PEDIDO;

/
--------------------------------------------------------
--  DDL for Procedure ELIMINAR_REPRESENTANTE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."ELIMINAR_REPRESENTANTE" (ID_REPRESENTANTE NUMBER)
AS
BORRADO NUMBER;
BEGIN 
    DELETE FROM T_REPRESENTANTE WHERE IDREPRESENTANTE = ID_REPRESENTANTE;
    BORRADO := SQL%ROWCOUNT;
    IF BORRADO = 0 THEN
        DBMS_OUTPUT.PUT_LINE('ID REPRESENTANTE: ' || ID_REPRESENTANTE || ' NO ENCONTRADO');
    ELSE        
        DBMS_OUTPUT.PUT_LINE('SE HA BORRADO EL REPRESENTANTE ID: ' || ID_REPRESENTANTE);
    END IF;
-- AUTOR: MAXIMILIANO GONZ�LEZ --
END ELIMINAR_REPRESENTANTE;

/
--------------------------------------------------------
--  DDL for Procedure ELIMINAR_SUBASTA
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."ELIMINAR_SUBASTA" (ID_SUBASTA NUMBER)
AS
BORRADO NUMBER;
BEGIN 
    DELETE FROM T_SUBASTA WHERE IDSUBASTA = ID_SUBASTA;
    BORRADO := SQL%ROWCOUNT;
    IF BORRADO = 0 THEN
        DBMS_OUTPUT.PUT_LINE('ID SUBASTA: ' || ID_SUBASTA || ' NO ENCONTRADO');
    ELSE        
        DBMS_OUTPUT.PUT_LINE('SE HA BORRADO LA SUBASTA ID: ' || ID_SUBASTA);
    END IF;
---AUTOR: JUAN ROMAN-
END ELIMINAR_SUBASTA;

/
--------------------------------------------------------
--  DDL for Procedure ELIMINAR_UNIDAD_MEDICION
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."ELIMINAR_UNIDAD_MEDICION" (ID_UNIDAD NUMBER)
AS
BORRADO NUMBER;
BEGIN 
    DELETE FROM T_UNIDAD_MEDICION WHERE idUnidad = ID_UNIDAD;
    BORRADO := SQL%ROWCOUNT;
    IF BORRADO = 0 THEN
        DBMS_OUTPUT.PUT_LINE('EL ID DE LA UNIDAD DE MEDICI�N: ' || ID_UNIDAD || ' NO ENCONTRADO');
    ELSE        
        DBMS_OUTPUT.PUT_LINE('SE HA ELIMINADO UNIDAD DE MEDICI�N CON ID: ' || ID_UNIDAD);
    END IF;
-- AUTOR: CRISTI�N ESQUIVEL --
END ELIMINAR_UNIDAD_MEDICION;

/
--------------------------------------------------------
--  DDL for Procedure ELIMINAR_USUARIO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."ELIMINAR_USUARIO" (ID_REPRESENTANTE NUMBER)
AS
BORRADO NUMBER;
BEGIN 
    DELETE FROM T_USUARIO WHERE IDREPRESENTANTE = ID_REPRESENTANTE;
    BORRADO := SQL%ROWCOUNT;
    IF BORRADO = 0 THEN
        DBMS_OUTPUT.PUT_LINE('ID EMPRESA: ' || ID_REPRESENTANTE || ' NO ENCONTRADO');
    ELSE        
        DBMS_OUTPUT.PUT_LINE('SE HA BORRADO LA EMPRESA ID: ' || ID_REPRESENTANTE);
    END IF;
-- AUTOR: MAXIMILIANO GONZ�LEZ --
END ELIMINAR_USUARIO;

/
--------------------------------------------------------
--  DDL for Procedure ELIMINAR_VEHICULO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."ELIMINAR_VEHICULO" (ID_VEHICULO NUMBER)
AS
BORRADO NUMBER;
BEGIN 
    DELETE FROM T_VEHICULO WHERE IDVEHICULO = ID_VEHICULO;
    BORRADO := SQL%ROWCOUNT;
    IF BORRADO = 0 THEN
        DBMS_OUTPUT.PUT_LINE('ID VEHICULO: ' || ID_VEHICULO || ' NO ENCONTRADO');
    ELSE        
        DBMS_OUTPUT.PUT_LINE('SE HA BORRADO EL VEHICULO ID: ' || ID_VEHICULO);
    END IF;
-- AUTOR: MAXIMILIANO GONZ�LEZ --
END ELIMINAR_VEHICULO;

/
--------------------------------------------------------
--  DDL for Procedure ELIMINAR_VIAJE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."ELIMINAR_VIAJE" (idofertatransporte NUMBER)
AS
borrar NUMBER;
BEGIN 
    DELETE FROM t_oferta_transporte WHERE idofertatransporte = idofertatransporte;
    borrar := SQL%ROWCOUNT;
    IF borrar = 0 THEN
        DBMS_OUTPUT.PUT_LINE('ID viaje: ' || idofertatransporte || ' NO ENCONTRADO');
    ELSE        
        DBMS_OUTPUT.PUT_LINE('SE HA BORRADO EL producto : ' || idofertatransporte);
    END IF;
-- ricardo --
END eliminar_viaje;

/
--------------------------------------------------------
--  DDL for Procedure INGRESARCONTRATO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."INGRESARCONTRATO" 
(
idcon in number ,
idem in NUMBER ,
fechini in DATE,
fechater in DATE,
rutdoc IN VARCHAR2,
vige in char
)
as
begin
insert into T_CONTRATO (IDCONTRATO,IDEMPRESA,FECHAINICIO,FECHATERMINO,RUTADOCUMENTO,VIGENTE) values (idcon,idem,fechini,fechater,rutdoc,vige);
end;

/
--------------------------------------------------------
--  DDL for Procedure LISTARCONTRATOS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."LISTARCONTRATOS" (reg out SYS_REFCURSOR)
AS
BEGIN
open reg for
SELECT * FROM t_contrato;
END;

/
--------------------------------------------------------
--  DDL for Procedure LISTARCOSTOSVIAJE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."LISTARCOSTOSVIAJE" (reg out SYS_REFCURSOR)
AS
BEGIN
open reg for
SELECT * FROM t_costos_viaje;
END;

/
--------------------------------------------------------
--  DDL for Procedure LISTAREMPRESAS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."LISTAREMPRESAS" (reg out SYS_REFCURSOR)
AS
BEGIN
open reg for
SELECT * FROM t_empresa;
END;

/
--------------------------------------------------------
--  DDL for Procedure LISTARLOTEOFRECIDO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."LISTARLOTEOFRECIDO" (reg out SYS_REFCURSOR)
AS
BEGIN
open reg for
SELECT * FROM t_lote_ofrecido;
END;

/
--------------------------------------------------------
--  DDL for Procedure LISTARLOTEPRODUCTO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."LISTARLOTEPRODUCTO" (reg out SYS_REFCURSOR)
AS
BEGIN
open reg for
SELECT * FROM t_lote_producto;
END;

/
--------------------------------------------------------
--  DDL for Procedure LISTAROFERTATRA
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."LISTAROFERTATRA" (reg out SYS_REFCURSOR)
AS
BEGIN
open reg for
select  ofer.idofertatransporte, ofer.idsubasta, sub.nombresubasta, 
ofer.idvehiculo, ofer.idrepresentante, 
rep.nombre, rep.appaterno, ofer.vigente
from bdmaipogrande.t_oferta_transporte ofer
join bdmaipogrande.t_representante rep on ofer.idrepresentante = rep.idrepresentante
left join bdmaipogrande.t_subasta sub on ofer.idsubasta = sub.idsubasta;
END;

/
--------------------------------------------------------
--  DDL for Procedure LISTAROFERTATRANSPORTE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."LISTAROFERTATRANSPORTE" (reg out SYS_REFCURSOR)
AS
BEGIN
open reg for
SELECT * FROM t_oferta_transporte;
END;

/
--------------------------------------------------------
--  DDL for Procedure LISTARPEDIDOS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."LISTARPEDIDOS" (reg out SYS_REFCURSOR)
AS
BEGIN
open reg for
SELECT * FROM t_pedido;
END;

/
--------------------------------------------------------
--  DDL for Procedure LISTARPRODUCTO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."LISTARPRODUCTO" (reg out SYS_REFCURSOR)
AS
BEGIN
open reg for
SELECT * FROM t_producto;
END;

/
--------------------------------------------------------
--  DDL for Procedure LISTARPRODUCTOPEDIDO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."LISTARPRODUCTOPEDIDO" (reg out SYS_REFCURSOR)
AS
BEGIN
open reg for
SELECT * FROM t_producto_pedido;
END;

/
--------------------------------------------------------
--  DDL for Procedure LISTARREPRESENTANTE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."LISTARREPRESENTANTE" (reg out SYS_REFCURSOR)
AS
BEGIN
open reg for
SELECT * FROM t_representante;
END;

/
--------------------------------------------------------
--  DDL for Procedure LISTARSUBASTA
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."LISTARSUBASTA" (reg out SYS_REFCURSOR)
AS
BEGIN
open reg for
SELECT * FROM t_subasta;
END;

/
--------------------------------------------------------
--  DDL for Procedure LISTARUNIDADMEDICION
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."LISTARUNIDADMEDICION" (reg out SYS_REFCURSOR)
AS
BEGIN
open reg for
SELECT * FROM t_unidad_medicion;
END;

/
--------------------------------------------------------
--  DDL for Procedure LISTARUSUARIO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."LISTARUSUARIO" (reg out SYS_REFCURSOR)
AS
BEGIN
open reg for
SELECT * FROM t_usuario;
END;

/
--------------------------------------------------------
--  DDL for Procedure LISTARUSUREP
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."LISTARUSUREP" (reg out SYS_REFCURSOR)
AS
BEGIN
open reg for
select  usu.idusuario, re.idrepresentante, 
        re.rutrepresentante, re.nombre, re.appaterno, re.apmaterno, re.sexo, re.fechanacimiento, re.fono1, re.fono2,
        emp.nombrecomercial, emp.idempresa, re.rolrepresentante,usu.email,usu.contrasena,
        usu.vigente
from bdmaipogrande.t_representante re
full outer join bdmaipogrande.t_usuario usu on re.idrepresentante = usu.idrepresentante
left join bdmaipogrande.t_empresa emp on re.idempresa = emp.idempresa;
END;

/
--------------------------------------------------------
--  DDL for Procedure LISTARVEHI
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."LISTARVEHI" (reg out SYS_REFCURSOR)
AS
BEGIN
open reg for
select  emp.idempresa, emp.nombrecomercial, vehi.idvehiculo, 
        vehi.clase, vehi.cmalto, vehi.cmancho, vehi.cmlargo, 
        vehi.frigorifico,vehi.patente
from bdmaipogrande.t_vehiculo vehi
join bdmaipogrande.t_empresa emp on vehi.idempresa = emp.idempresa;
END;

/
--------------------------------------------------------
--  DDL for Procedure LISTARVEHICULO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."LISTARVEHICULO" (reg out SYS_REFCURSOR)
AS
BEGIN
open reg for
SELECT * FROM t_vehiculo;
END;

/
--------------------------------------------------------
--  DDL for Procedure LISTARVIAJE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."LISTARVIAJE" (reg out SYS_REFCURSOR)
AS
BEGIN
open reg for
SELECT * FROM t_viaje;
END;

/
--------------------------------------------------------
--  DDL for Procedure VALIDARUSUARIO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."VALIDARUSUARIO" (
V_CORREO IN VARCHAR,
V_CLAVE IN VARCHAR,
P_EMAIL OUT t_usuario.email%TYPE,
P_CONTRASENA OUT t_usuario.contrasena%TYPE
)
AS
VAR_VALIDATE number;
BEGIN
VAR_VALIDATE:=0;
select count(*) INTO VAR_VALIDATE FROM T_USUARIO WHERE t_usuario.email =V_CORREO  and t_usuario.contrasena = V_CLAVE;

if var_validate=1 then
    select email,contrasena INTO P_EMAIL, P_CONTRASENA FROM T_USUARIO WHERE t_usuario.email =V_CORREO  and t_usuario.contrasena = V_CLAVE;
else
    DBMS_OUTPUT.PUT_LINE('Usuario no disponible');
    p_contrasena:='errornull';
    p_email:='errornull';
end if;

END;

/
--------------------------------------------------------
--  DDL for Procedure VERIFICARDEPENDENCIASEMPRESA
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "BDMAIPOGRANDE"."VERIFICARDEPENDENCIASEMPRESA" (
ID_EMPRESA IN NUMBER,
reg out SYS_REFCURSOR
)
AS
BEGIN
open reg for
select COUNT(c.idempresa)+COUNT(lp.idempresa)+COUNT(r.idempresa)+COUNT(v.idempresa) as "Cantidad" FROM t_empresa e 
full outer join t_contrato c on c.idempresa = e.idempresa
left join t_lote_producto lp on lp.idempresa = e.idempresa
left join t_representante r on r.idempresa = e.idempresa
left join t_vehiculo v on v.idempresa = e.idempresa
WHERE e.idempresa=id_empresa
group by e.idempresa
;
END;

/
--------------------------------------------------------
--  Constraints for Table T_EMPRESA
--------------------------------------------------------

  ALTER TABLE "BDMAIPOGRANDE"."T_EMPRESA" ADD CONSTRAINT "T_EMPRESA_PK" PRIMARY KEY ("IDEMPRESA")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
  ALTER TABLE "BDMAIPOGRANDE"."T_EMPRESA" MODIFY ("EMAIL" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_EMPRESA" MODIFY ("PAIS" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_EMPRESA" MODIFY ("CIUDAD" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_EMPRESA" MODIFY ("DIRECCION" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_EMPRESA" MODIFY ("TIPOEMPRESA" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_EMPRESA" MODIFY ("RUTEMPRESA" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_EMPRESA" MODIFY ("IDEMPRESA" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_EMPRESA" MODIFY ("FONO1" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table T_VIAJE
--------------------------------------------------------

  ALTER TABLE "BDMAIPOGRANDE"."T_VIAJE" ADD CONSTRAINT "T_VIAJE_PK" PRIMARY KEY ("IDVIAJE")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
  ALTER TABLE "BDMAIPOGRANDE"."T_VIAJE" MODIFY ("ESTADOVIAJE" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_VIAJE" MODIFY ("IDREPRESENTANTE" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_VIAJE" MODIFY ("IDOFERTATRANSPORTE" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_VIAJE" MODIFY ("IDVIAJE" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table T_PRODUCTO_PEDIDO
--------------------------------------------------------

  ALTER TABLE "BDMAIPOGRANDE"."T_PRODUCTO_PEDIDO" ADD CONSTRAINT "T_PRODUCTO_PEDIDO_PK" PRIMARY KEY ("IDPRODUCTOPEDIDO")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
  ALTER TABLE "BDMAIPOGRANDE"."T_PRODUCTO_PEDIDO" MODIFY ("IDPRODUCTO" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_PRODUCTO_PEDIDO" MODIFY ("IDUNIDAD" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_PRODUCTO_PEDIDO" MODIFY ("CANTIDAD" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_PRODUCTO_PEDIDO" MODIFY ("IDPEDIDO" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_PRODUCTO_PEDIDO" MODIFY ("IDPRODUCTOPEDIDO" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table T_COSTOS_VIAJE
--------------------------------------------------------

  ALTER TABLE "BDMAIPOGRANDE"."T_COSTOS_VIAJE" ADD CONSTRAINT "T_COSTOS_VIAJE_PK" PRIMARY KEY ("IDCOSTOVIAJE")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
  ALTER TABLE "BDMAIPOGRANDE"."T_COSTOS_VIAJE" MODIFY ("OBSERVACIONES" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_COSTOS_VIAJE" MODIFY ("IDVIAJE" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_COSTOS_VIAJE" MODIFY ("IDCOSTOVIAJE" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table T_LOTE_OFRECIDO
--------------------------------------------------------

  ALTER TABLE "BDMAIPOGRANDE"."T_LOTE_OFRECIDO" ADD CONSTRAINT "PRODUCTO_PRODUCTOR_PK" PRIMARY KEY ("IDLOTEOFRECIDO")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
  ALTER TABLE "BDMAIPOGRANDE"."T_LOTE_OFRECIDO" MODIFY ("VIGENTE" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_LOTE_OFRECIDO" MODIFY ("IDLOTEPRODUCTO" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_LOTE_OFRECIDO" MODIFY ("IDPRODUCTOPEDIDO" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_LOTE_OFRECIDO" MODIFY ("IDLOTEOFRECIDO" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table T_PEDIDO
--------------------------------------------------------

  ALTER TABLE "BDMAIPOGRANDE"."T_PEDIDO" ADD CONSTRAINT "T_PEDIDO_PK" PRIMARY KEY ("IDPEDIDO")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
  ALTER TABLE "BDMAIPOGRANDE"."T_PEDIDO" MODIFY ("FECHAENTREGAESTIPULADA" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_PEDIDO" MODIFY ("FECHAINICIOPEDIDO" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_PEDIDO" MODIFY ("IDREPRESENTANTE" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_PEDIDO" MODIFY ("IDPEDIDO" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table T_USUARIO
--------------------------------------------------------

  ALTER TABLE "BDMAIPOGRANDE"."T_USUARIO" ADD CONSTRAINT "T_USUARIO_PK" PRIMARY KEY ("IDUSUARIO")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
  ALTER TABLE "BDMAIPOGRANDE"."T_USUARIO" MODIFY ("VIGENTE" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_USUARIO" MODIFY ("IDREPRESENTANTE" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_USUARIO" MODIFY ("IDUSUARIO" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table T_SUBASTA
--------------------------------------------------------

  ALTER TABLE "BDMAIPOGRANDE"."T_SUBASTA" ADD CONSTRAINT "T_SUBASTA_PK" PRIMARY KEY ("IDSUBASTA")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
  ALTER TABLE "BDMAIPOGRANDE"."T_SUBASTA" MODIFY ("IDREPRESENTANTE" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_SUBASTA" MODIFY ("FECHATERMINO" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_SUBASTA" MODIFY ("FECHAPUBLICACION" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_SUBASTA" MODIFY ("IDPEDIDO" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_SUBASTA" MODIFY ("NOMBRESUBASTA" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_SUBASTA" MODIFY ("IDSUBASTA" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table T_OFERTA_TRANSPORTE
--------------------------------------------------------

  ALTER TABLE "BDMAIPOGRANDE"."T_OFERTA_TRANSPORTE" ADD CONSTRAINT "T_VEHICULO_SUBASTA_PK" PRIMARY KEY ("IDOFERTATRANSPORTE")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
  ALTER TABLE "BDMAIPOGRANDE"."T_OFERTA_TRANSPORTE" MODIFY ("VIGENTE" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_OFERTA_TRANSPORTE" MODIFY ("COSTOTRANSPORTE" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_OFERTA_TRANSPORTE" MODIFY ("IDREPRESENTANTE" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_OFERTA_TRANSPORTE" MODIFY ("IDVEHICULO" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_OFERTA_TRANSPORTE" MODIFY ("IDSUBASTA" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_OFERTA_TRANSPORTE" MODIFY ("IDOFERTATRANSPORTE" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table T_CONTRATO
--------------------------------------------------------

  ALTER TABLE "BDMAIPOGRANDE"."T_CONTRATO" ADD CONSTRAINT "T_CONTRATO_PK" PRIMARY KEY ("IDCONTRATO")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
  ALTER TABLE "BDMAIPOGRANDE"."T_CONTRATO" MODIFY ("VIGENTE" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_CONTRATO" MODIFY ("FECHAINICIO" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_CONTRATO" MODIFY ("IDEMPRESA" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_CONTRATO" MODIFY ("IDCONTRATO" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table T_REPRESENTANTE
--------------------------------------------------------

  ALTER TABLE "BDMAIPOGRANDE"."T_REPRESENTANTE" ADD CONSTRAINT "TABLE_REPRESENTANTE_PK" PRIMARY KEY ("IDREPRESENTANTE")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
  ALTER TABLE "BDMAIPOGRANDE"."T_REPRESENTANTE" MODIFY ("VIGENTE" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_REPRESENTANTE" MODIFY ("FONO1" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_REPRESENTANTE" MODIFY ("FECHANACIMIENTO" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_REPRESENTANTE" MODIFY ("SEXO" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_REPRESENTANTE" MODIFY ("APPATERNO" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_REPRESENTANTE" MODIFY ("NOMBRE" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_REPRESENTANTE" MODIFY ("ROLREPRESENTANTE" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_REPRESENTANTE" MODIFY ("RUTREPRESENTANTE" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_REPRESENTANTE" MODIFY ("IDEMPRESA" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_REPRESENTANTE" MODIFY ("IDREPRESENTANTE" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table T_VEHICULO
--------------------------------------------------------

  ALTER TABLE "BDMAIPOGRANDE"."T_VEHICULO" ADD CONSTRAINT "T_VEHICULO_PK" PRIMARY KEY ("IDVEHICULO")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
  ALTER TABLE "BDMAIPOGRANDE"."T_VEHICULO" MODIFY ("FRIGORIFICO" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_VEHICULO" MODIFY ("CMLARGO" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_VEHICULO" MODIFY ("CMALTO" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_VEHICULO" MODIFY ("CMANCHO" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_VEHICULO" MODIFY ("CAPACIDADKG" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_VEHICULO" MODIFY ("CLASE" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_VEHICULO" MODIFY ("PATENTE" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_VEHICULO" MODIFY ("IDEMPRESA" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_VEHICULO" MODIFY ("IDVEHICULO" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table T_PRODUCTO
--------------------------------------------------------

  ALTER TABLE "BDMAIPOGRANDE"."T_PRODUCTO" ADD CONSTRAINT "PRODUCTO_PK" PRIMARY KEY ("IDPRODUCTO")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
  ALTER TABLE "BDMAIPOGRANDE"."T_PRODUCTO" MODIFY ("IDPRODUCTO" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table T_LOTE_PRODUCTO
--------------------------------------------------------

  ALTER TABLE "BDMAIPOGRANDE"."T_LOTE_PRODUCTO" ADD CONSTRAINT "T_LOTE_PRODUCTO_PK" PRIMARY KEY ("IDLOTEPRODUCTO")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
  ALTER TABLE "BDMAIPOGRANDE"."T_LOTE_PRODUCTO" MODIFY ("VIGENTE" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_LOTE_PRODUCTO" MODIFY ("FECHAVENCIMIENTO" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_LOTE_PRODUCTO" MODIFY ("PRECIOUNIDAD" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_LOTE_PRODUCTO" MODIFY ("IDUNIDAD" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_LOTE_PRODUCTO" MODIFY ("IDEMPRESA" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_LOTE_PRODUCTO" MODIFY ("IDPRODUCTO" NOT NULL ENABLE);
  ALTER TABLE "BDMAIPOGRANDE"."T_LOTE_PRODUCTO" MODIFY ("IDLOTEPRODUCTO" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table T_UNIDAD_MEDICION
--------------------------------------------------------

  ALTER TABLE "BDMAIPOGRANDE"."T_UNIDAD_MEDICION" ADD CONSTRAINT "T_UNIDAD_VENTA_PK" PRIMARY KEY ("IDUNIDAD")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
  ALTER TABLE "BDMAIPOGRANDE"."T_UNIDAD_MEDICION" MODIFY ("IDUNIDAD" NOT NULL ENABLE);
--------------------------------------------------------
--  Ref Constraints for Table T_CONTRATO
--------------------------------------------------------

  ALTER TABLE "BDMAIPOGRANDE"."T_CONTRATO" ADD CONSTRAINT "T_CONTRATO_FK" FOREIGN KEY ("IDEMPRESA")
	  REFERENCES "BDMAIPOGRANDE"."T_EMPRESA" ("IDEMPRESA") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table T_COSTOS_VIAJE
--------------------------------------------------------

  ALTER TABLE "BDMAIPOGRANDE"."T_COSTOS_VIAJE" ADD CONSTRAINT "T_COSTOS_VIAJE_FK" FOREIGN KEY ("IDVIAJE")
	  REFERENCES "BDMAIPOGRANDE"."T_VIAJE" ("IDVIAJE") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table T_LOTE_OFRECIDO
--------------------------------------------------------

  ALTER TABLE "BDMAIPOGRANDE"."T_LOTE_OFRECIDO" ADD CONSTRAINT "T_LOTE_OFRECIDO_FK" FOREIGN KEY ("IDREPRESENTANTE")
	  REFERENCES "BDMAIPOGRANDE"."T_REPRESENTANTE" ("IDREPRESENTANTE") ENABLE;
  ALTER TABLE "BDMAIPOGRANDE"."T_LOTE_OFRECIDO" ADD CONSTRAINT "T_PRODUCTO_VENTA_FK" FOREIGN KEY ("IDLOTEPRODUCTO")
	  REFERENCES "BDMAIPOGRANDE"."T_LOTE_PRODUCTO" ("IDLOTEPRODUCTO") ENABLE;
  ALTER TABLE "BDMAIPOGRANDE"."T_LOTE_OFRECIDO" ADD CONSTRAINT "T_PRODUCTO_VENTA_FKV1" FOREIGN KEY ("IDPRODUCTOPEDIDO")
	  REFERENCES "BDMAIPOGRANDE"."T_PRODUCTO_PEDIDO" ("IDPRODUCTOPEDIDO") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table T_LOTE_PRODUCTO
--------------------------------------------------------

  ALTER TABLE "BDMAIPOGRANDE"."T_LOTE_PRODUCTO" ADD CONSTRAINT "T_LOTE_PRODUCTO_FK" FOREIGN KEY ("IDEMPRESA")
	  REFERENCES "BDMAIPOGRANDE"."T_EMPRESA" ("IDEMPRESA") ENABLE;
  ALTER TABLE "BDMAIPOGRANDE"."T_LOTE_PRODUCTO" ADD CONSTRAINT "T_LOTE_PRODUCTO_FKV1" FOREIGN KEY ("IDPRODUCTO")
	  REFERENCES "BDMAIPOGRANDE"."T_PRODUCTO" ("IDPRODUCTO") ENABLE;
  ALTER TABLE "BDMAIPOGRANDE"."T_LOTE_PRODUCTO" ADD CONSTRAINT "T_LOTE_PRODUCTO_FKV2" FOREIGN KEY ("IDUNIDAD")
	  REFERENCES "BDMAIPOGRANDE"."T_UNIDAD_MEDICION" ("IDUNIDAD") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table T_OFERTA_TRANSPORTE
--------------------------------------------------------

  ALTER TABLE "BDMAIPOGRANDE"."T_OFERTA_TRANSPORTE" ADD CONSTRAINT "T_OFERTA_TRANSPORTE_FK" FOREIGN KEY ("IDREPRESENTANTE")
	  REFERENCES "BDMAIPOGRANDE"."T_REPRESENTANTE" ("IDREPRESENTANTE") ENABLE;
  ALTER TABLE "BDMAIPOGRANDE"."T_OFERTA_TRANSPORTE" ADD CONSTRAINT "T_VEHICULO_FK" FOREIGN KEY ("IDVEHICULO")
	  REFERENCES "BDMAIPOGRANDE"."T_VEHICULO" ("IDVEHICULO") ENABLE;
  ALTER TABLE "BDMAIPOGRANDE"."T_OFERTA_TRANSPORTE" ADD CONSTRAINT "T_VEHICULO_FKV1" FOREIGN KEY ("IDSUBASTA")
	  REFERENCES "BDMAIPOGRANDE"."T_SUBASTA" ("IDSUBASTA") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table T_PEDIDO
--------------------------------------------------------

  ALTER TABLE "BDMAIPOGRANDE"."T_PEDIDO" ADD CONSTRAINT "T_ORDEN_PEDIDO_FK" FOREIGN KEY ("IDREPRESENTANTE")
	  REFERENCES "BDMAIPOGRANDE"."T_REPRESENTANTE" ("IDREPRESENTANTE") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table T_PRODUCTO_PEDIDO
--------------------------------------------------------

  ALTER TABLE "BDMAIPOGRANDE"."T_PRODUCTO_PEDIDO" ADD CONSTRAINT "T_PRODUCTO_PEDIDO_FK" FOREIGN KEY ("IDPEDIDO")
	  REFERENCES "BDMAIPOGRANDE"."T_PEDIDO" ("IDPEDIDO") ENABLE;
  ALTER TABLE "BDMAIPOGRANDE"."T_PRODUCTO_PEDIDO" ADD CONSTRAINT "T_PRODUCTO_PEDIDO_FKV1" FOREIGN KEY ("IDUNIDAD")
	  REFERENCES "BDMAIPOGRANDE"."T_UNIDAD_MEDICION" ("IDUNIDAD") ENABLE;
  ALTER TABLE "BDMAIPOGRANDE"."T_PRODUCTO_PEDIDO" ADD CONSTRAINT "T_PRODUCTO_PEDIDO_FKV2" FOREIGN KEY ("IDPRODUCTO")
	  REFERENCES "BDMAIPOGRANDE"."T_PRODUCTO" ("IDPRODUCTO") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table T_REPRESENTANTE
--------------------------------------------------------

  ALTER TABLE "BDMAIPOGRANDE"."T_REPRESENTANTE" ADD CONSTRAINT "T_REPRESENTANTE_FK" FOREIGN KEY ("IDEMPRESA")
	  REFERENCES "BDMAIPOGRANDE"."T_EMPRESA" ("IDEMPRESA") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table T_SUBASTA
--------------------------------------------------------

  ALTER TABLE "BDMAIPOGRANDE"."T_SUBASTA" ADD CONSTRAINT "T_SUBASTA_FK" FOREIGN KEY ("IDPEDIDO")
	  REFERENCES "BDMAIPOGRANDE"."T_PEDIDO" ("IDPEDIDO") ENABLE;
  ALTER TABLE "BDMAIPOGRANDE"."T_SUBASTA" ADD CONSTRAINT "T_SUBASTA_FKV1" FOREIGN KEY ("IDREPRESENTANTE")
	  REFERENCES "BDMAIPOGRANDE"."T_REPRESENTANTE" ("IDREPRESENTANTE") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table T_USUARIO
--------------------------------------------------------

  ALTER TABLE "BDMAIPOGRANDE"."T_USUARIO" ADD CONSTRAINT "T_USUARIO_T_REPRESENTANTE_FK" FOREIGN KEY ("IDREPRESENTANTE")
	  REFERENCES "BDMAIPOGRANDE"."T_REPRESENTANTE" ("IDREPRESENTANTE") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table T_VEHICULO
--------------------------------------------------------

  ALTER TABLE "BDMAIPOGRANDE"."T_VEHICULO" ADD CONSTRAINT "T_VEHICULO_T_EMPRESA_FK" FOREIGN KEY ("IDEMPRESA")
	  REFERENCES "BDMAIPOGRANDE"."T_EMPRESA" ("IDEMPRESA") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table T_VIAJE
--------------------------------------------------------

  ALTER TABLE "BDMAIPOGRANDE"."T_VIAJE" ADD CONSTRAINT "T_VIAJE_FKV1" FOREIGN KEY ("IDOFERTATRANSPORTE")
	  REFERENCES "BDMAIPOGRANDE"."T_OFERTA_TRANSPORTE" ("IDOFERTATRANSPORTE") ENABLE;
  ALTER TABLE "BDMAIPOGRANDE"."T_VIAJE" ADD CONSTRAINT "T_VIAJE_T_REPRESENTANTE_FK" FOREIGN KEY ("IDREPRESENTANTE")
	  REFERENCES "BDMAIPOGRANDE"."T_REPRESENTANTE" ("IDREPRESENTANTE") ENABLE;
